package soba.model;

public class FieldAccess {

	private String className;
	private String fieldName;
	private String fieldDesc;
	private boolean isStatic;
	private boolean isGet; // true==GET, false==PUT 

	private FieldAccess(String className, String fieldName, String fieldDesc, boolean isStatic, boolean isGet) {
		this.className = className;
		this.fieldName = fieldName;
		this.fieldDesc = fieldDesc;
		this.isStatic = isStatic;
		this.isGet = isGet;
	}
	
	public static FieldAccess createGetField(String className, String fieldName, String fieldDesc, boolean isStatic) {
		return new FieldAccess(className, fieldName, fieldDesc, isStatic, true);
	}

	public static FieldAccess createPutField(String className, String fieldName, String fieldDesc, boolean isStatic) {
		return new FieldAccess(className, fieldName, fieldDesc, isStatic, false);
	}
	
	public String getClassName() {
		return className;
	}

	public String getFieldName() {
		return fieldName;
	}
	
	public String getDescriptor() {
		return fieldDesc;
	}
	
	public boolean isStatic() {
		return isStatic;
	}
	
	public boolean isGet() {
		return isGet;
	}

	public boolean isPut() {
		return !isGet;
	}
	
	public String toString() {
		StringBuilder b = new StringBuilder();
		if (isGet) {
			b.append("GET");
		} else {
			b.append("PUT");
		}
		if (isStatic) {
			b.append("STATIC");
		} else {
			b.append("FIELD");
		}
		b.append(" ");
		b.append(className);
		b.append(".");
		b.append(fieldName);
		b.append(": ");
		b.append(fieldDesc);
		return b.toString();
	}
}
