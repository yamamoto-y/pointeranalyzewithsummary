package soba.model;

import java.util.List;

public interface IClassInfo {

	public static final String PACKAGE_SEPARATOR = "/";
	
	public String getPackageName();
	public String getClassName();
	public String getSuperClass();
	public List<String> getInterfaces();
	
	public int getMethodCount();
	public IMethodInfo getMethod(int index);	
	public IMethodInfo getMethodByName(String methodName, String methodDesc);
	
	public int getFieldCount();
	public IFieldInfo getField(int index);
	public IFieldInfo getFieldByName(String fieldName, String fieldDesc);
	
}
