package soba.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;



/**
 * DynamicBindingResolver is a class to resolve binding of methods and fields.
 * The instance methods use an internal cache to improve the performance.
 * You may directly use static methods to resolve dynamic binding.
 * @author ishio
 */
public class DynamicBindingResolver {

	/**
	 * Resolve dynamic binding of a method invocation.
	 * @param ch is a class hierarchy.
	 * @param invoke specifies a method to be invoked.
	 * @return an array of MethodInfo object representing methods
	 * that might be executed by the invocation.
	 * The return value is an empty array if no method declaration
	 * matched to the invocation.
	 */
	public static IMethodInfo[] resolveCall(ClassHierarchy ch, Invocation invoke) {
		if (invoke.isStaticOrSpecial()) {
			IMethodInfo m = resolveSpecialCall(ch, invoke.getClassName(), invoke.getMethodName(), invoke.getDescriptor());
			if (m == null) {
				return new IMethodInfo[0];
			} else {
				return new IMethodInfo[] {m};
			}
		} else {
			return resolveDynamicCall(ch, invoke.getClassName(), invoke.getMethodName(), invoke.getDescriptor());
		}
	}

	/**
	 * Resolve binding of a static/constructor call.
	 * @param ch is a class hierarchy.
	 * @param className specifies a class representing a receiver type.
	 * @param methodName specifies a method name.
	 * @param methodDesc specifies a method descriptor (without generics information).
	 * @return MethodInfo object representing the specified method.
	 * The return value may be null if the method is not found.
	 */
	public static IMethodInfo resolveSpecialCall(ClassHierarchy ch, String className, String methodName, String methodDesc) {
		IMethodInfo m = findDeclaration(ch, className, methodName, methodDesc);
		if (m != null) {
			return m;
		} else {
			return null;
		}
	}

	/**
	 * Resolve dynamic binding of a virtual method call.
	 * Please note that this method does not care about the detail of the method definition.
	 * If a static method or an undefined private method is specified, this method may return incorrect result.
	 * @param ch is a class hierarchy.
	 * @param className specifies a class representing a receiver type.
	 * @param methodName specifies a method name.
	 * @param methodDesc specifies a method descriptor (without generics information).
	 * @return MethodInfo object representing the specified method.
	 * The return value may be an emtpy if the method is not found.
	 */
	public static IMethodInfo[] resolveDynamicCall(ClassHierarchy ch, String className, String methodName, String methodDesc) {
		assert ch != null && className != null && methodName != null && methodDesc != null : "Parameters cannot be null.";
		String targetTypeName = className;

		// Find the declaration of the called method.
		IMethodInfo topDecl = findDeclaration(ch, className, methodName, methodDesc);
		if (topDecl == null) return new IMethodInfo[0];  // Not found

		List<IMethodInfo> result = new ArrayList<IMethodInfo>(16);
		if (topDecl.hasMethodBody()) result.add(topDecl);


		// We explicitly avoid array types, because arrays are not included in ClassHierarchy.
		if (!ch.isArrayType(targetTypeName)) {
			// Find all implementation of the same method signature in subclasses.
			Set<String> checkedClasses = new HashSet<String>();
			Stack<String> classes = new Stack<String>();
			classes.add(targetTypeName);
			while (!classes.empty()) {
				String currentClass = classes.pop();

				// skip the visited classes
				if (checkedClasses.contains(currentClass)) {
					continue;
				}
				checkedClasses.add(currentClass);

				IClassInfo currentClassInfo = ch.getClassInfo(currentClass);
				if (currentClassInfo != null) {
					IMethodInfo m = currentClassInfo.getMethodByName(methodName, methodDesc);
					if (m != null) {
						if (m.hasMethodBody() && (m != topDecl)) {
							// m overrides the target class.
							result.add(m);
						}
					}

					if ((m == null) || m.isOverridable()) {
						// the method may be overridden by subclasses.
						for (String c: ch.getSubtypes(currentClass)) {
							if (m != null && m.isPackagePrivate()) {
								// A package-private method can be overridden by only classes in the same package.
								if (ch.isSamePackage(c, currentClass)) {
									classes.push(c);
								}
							} else {
								// Other methods can be overridden by sub-types.
								classes.push(c);
							}
						}
					}
				} else {
					// Skip a class that is not included in the class hierarchy.
				}
			}

		}

		IMethodInfo[] resultArray = new IMethodInfo[result.size()];
		for (int i=0; i<result.size(); ++i) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}


	/**
	 * Find method declaration specified by typeName and signature.
	 * @param typeName is fully qualified domain name of a class/interface.
	 * @param signatureId identifies a method by its name and parameters.
	 * To obtain a method signature, use MethodUtil.getMethodSignature.
	 * @return MethodDecl object of the nearest ancestor (including the class specified by typeName)
	 * If no ancestor classes declare the method,
	 * MethodDecl comes from interfaces who declares the method.
	 * @throws ClassNotFoundException is thrown if typeName or its ancestors are not found in class hierarchy.
	 * @throws NoSuchMethodException is thrown if method is not found in ancestor classes and interfaces.
	 */
	private static IMethodInfo findDeclaration(ClassHierarchy classHierarchy, String className, String methodName, String methodDesc) {

		// Find the nearest ancestor class implements the method
		String currentClass = className;
		while (currentClass != null) {
			IClassInfo currentClassInfo = classHierarchy.getClassInfo(currentClass);
			if (currentClassInfo != null) {
				IMethodInfo m = currentClassInfo.getMethodByName(methodName, methodDesc);
				if (m != null) {
					return m;
				} else {
					currentClass = classHierarchy.getSuperClass(currentClass);
				}
			} else {
				if (classHierarchy.isArrayType(currentClass)) {
					currentClass = classHierarchy.getSuperClass(currentClass);
					continue;
				}
				return null;
			}
		}

		// Search all interfaces
		LinkedList<String> worklist = new LinkedList<String>();
		currentClass = className;
		while (currentClass != null) {
			worklist.addAll(classHierarchy.getSuperInterfaces(currentClass));
			currentClass = classHierarchy.getSuperClass(currentClass);
		}
		// Find a method declaration in the interfaces
		while (!worklist.isEmpty()) {
			String interfaceName = worklist.pollFirst();
			IClassInfo currentClassInfo = classHierarchy.getClassInfo(interfaceName);
			if (currentClassInfo != null) {
				IMethodInfo m = currentClassInfo.getMethodByName(methodName, methodDesc);
				if (m != null) {
					return m;
				} else {
					// An interface may extend another interface.
					worklist.addAll(classHierarchy.getSuperInterfaces(interfaceName));
				}
			} else {
				if (classHierarchy.isArrayType(interfaceName)) {
					// ignore array types
					continue;
				}
				// Skip an interfaceName that is not included in the class hierarchy.
				return null;
			}
		}

		// Method not found at all
		return null;
	}

	public static IFieldInfo resolveField(ClassHierarchy ch, FieldAccess access) {
		String owner = resolveFieldOwner(ch, access);
		return ch.getClassInfo(owner).getFieldByName(access.getFieldName(), access.getDescriptor());
	}

	/**
	 * Find a class which has a field to be accessed.
	 * @param ch specifies a class hierarchy.
	 * @param access specifies a field access instruction.
	 * @return the name of a class which has a specified field.
	 */
	public static String resolveFieldOwner(ClassHierarchy ch, FieldAccess access) {
		if (access.isStatic()) {
			return resolveStaticFieldOwner(ch, access.getClassName(), access.getFieldName(), access.getDescriptor());
		} else {
			return resolveInstanceFieldOwner(ch, access.getClassName(), access.getFieldName(), access.getDescriptor());
		}
	}

	/**
	 * Find a class which has an instance field to be accessed.
	 * @param ch specifies a class hierarchy.
	 * @param className specifies a class name in a field access instruction.
	 * The class may inherit a field from its parent.
	 * @param fieldName
	 * @param fieldDesc
	 * @return a class name that defines the field specified by the arguments.
	 * The method may return null if an owner is not found.
	 */
	public static String resolveInstanceFieldOwner(ClassHierarchy ch, String className, String fieldName, String fieldDesc) {
    	String current = className;
		while (current != null) {
			IClassInfo c = ch.getClassInfo(current);
			if (c != null) {
				if (c.getFieldByName(fieldName, fieldDesc) != null) {
					return current;
				} else {
					current = c.getSuperClass();
				}
			} else {
				// If a class is not registered, stop to resolve the owner.
				current = null;
			}
		}
		return null;
	}

	/**
	 * Find a class which has a static field to be accessed.
	 * @param ch specifies a class hierarchy.
	 * @param className
	 * @param fieldName
	 * @param fieldDesc
	 * @return a class name.
	 * @see JVM Specification Section 5.4.3.2.
	 * If two public fields are accessible from the specified className and fieldName,
	 * javac reports the ambiguous field reference as an error.
	 */
	public static String resolveStaticFieldOwner(ClassHierarchy ch, String className, String fieldName, String fieldDesc) {
		// Search the current class
		IClassInfo c = ch.getClassInfo(className);
		if (c == null) return null;

		if (c != null) {
			if (c.getFieldByName(fieldName, fieldDesc) != null) {
				return className;
			}
		}

		// If not defined, search interface
		Stack<String> worklist = new Stack<String>();
		worklist.push(className);
		while (!worklist.isEmpty()) {
			String current = worklist.pop();
			c = ch.getClassInfo(current);
			if (c != null) {
				if (c.getFieldByName(fieldName, fieldDesc) != null) {
					return current;
				} else {
					if (c.getInterfaces() != null) {
						worklist.addAll(c.getInterfaces());
					}
				}
			} else {
				// If c is not a registered class, ignore it.
			}
		}

		// Recursively search a super class
    	c = ch.getClassInfo(className);
    	if (c.getSuperClass() != null) return resolveStaticFieldOwner(ch, c.getSuperClass(), fieldName, fieldDesc);
    	else return null;
	}


	private ClassHierarchy hierarchy;
	private Map<String, IMethodInfo[]> callCache;
	private Map<String, String> instanceFieldCache;
	private Map<String, String> staticFieldCache;

	/**
	 * A resolver instance keeps a class hierarchy object
	 * and internal cache.
	 * @param hierarchy
	 */
	public DynamicBindingResolver(ClassHierarchy hierarchy) {
		this.hierarchy = hierarchy;
		callCache = new HashMap<String, IMethodInfo[]>(65536);
		instanceFieldCache = new HashMap<String, String>(65536);
		staticFieldCache = new HashMap<String, String>(65536);
	}


	/**
	 * Resolve dynamic binding of a method invocation.
	 * @param invoke specifies a method to be invoked.
	 * @return an array of MethodInfo object representing methods
	 * that might be executed by the invocation.
	 * The return value is an empty array if no method declaration
	 * matched to the invocation.
	 */
	public IMethodInfo[] resolveCall(Invocation invoke) {
		if (invoke.isStaticOrSpecial()) {
			IMethodInfo m = resolveSpecialCall(hierarchy, invoke.getClassName(), invoke.getMethodName(), invoke.getDescriptor());
			if (m == null) {
				return new IMethodInfo[0];
			} else {
				return new IMethodInfo[] {m};
			}
		} else {
			return resolveDynamicCall(invoke.getClassName(), invoke.getMethodName(), invoke.getDescriptor());
		}
	}

	public IFieldInfo resolveField(FieldAccess access) {
		String owner = resolveFieldOwner(access);
		return hierarchy.getClassInfo(owner).getFieldByName(access.getFieldName(), access.getDescriptor());
	}


	/**
	 * Find a class which has a field to be accessed.
	 * @param access specifies a field access instruction.
	 * @return the name of a class which has a specified field.
	 */
	public String resolveFieldOwner(FieldAccess access) {
		if (access.isStatic()) {
			return resolveStaticFieldOwner(access.getClassName(), access.getFieldName(), access.getDescriptor());
		} else {
			return resolveInstanceFieldOwner(access.getClassName(), access.getFieldName(), access.getDescriptor());
		}
	}

	/**
	 * @return a key for HashMap from parameter strings.
	 */
	private String getKey(String className, String memberName, String memberDesc) {
		StringBuilder b = new StringBuilder(256);
		b.append(className);
		b.append("#");
		b.append(memberName);
		b.append("#");
		b.append(memberDesc);
		return b.toString();
	}

	/**
	 * Resolve binding of a static/constructor call.
	 * @param className specifies a class representing a receiver type.
	 * @param methodName specifies a method name.
	 * @param methodDesc specifies a method descriptor (without generics information).
	 * @return MethodInfo object representing the specified method.
	 * The return value may be null if the method is not found.
	 */
	public IMethodInfo resolveSpecialCall(String className, String methodName, String methodDesc) {
		return resolveSpecialCall(hierarchy, className, methodName, methodDesc);
	}

	/**
	 * Resolve dynamic binding of a virtual method call.
	 * @param className specifies a class representing a receiver type.
	 * @param methodName specifies a method name.
	 * @param methodDesc specifies a method descriptor (without generics information).
	 * @return MethodInfo object representing the specified method.
	 * The return value may be an emtpy if the method is not found.
	 */
	public IMethodInfo[] resolveDynamicCall(String className, String methodName, String methodDesc) {
		String key = getKey(className, methodName, methodDesc);
		IMethodInfo[] result = callCache.get(key);
		if (result == null) {
			result = resolveDynamicCall(hierarchy, className, methodName, methodDesc);
			callCache.put(key, result);
		}
		return result;
	}

	/**
	 * Find a class which has an instance field to be accessed.
	 * @param className specifies a class name in a field access instruction.
	 * The class may inherit a field from its parent.
	 * @param fieldName
	 * @param fieldDesc
	 * @return a class name that defines the field specified by the arguments.
	 * The method may return null if an owner is not found.
	 */
	public String resolveInstanceFieldOwner(String className, String fieldName, String fieldDesc) {
		String key = getKey(className, fieldName, fieldDesc);
		String result = instanceFieldCache.get(key);
		if (result == null) {
			result = resolveInstanceFieldOwner(hierarchy, className, fieldName, fieldDesc);
			instanceFieldCache.put(key, result);
		}
		return result;
	}


	/**
	 * Find a class which has a static field to be accessed.
	 * @param className
	 * @param fieldName
	 * @return
	 * @see JVM Specification Section 5.4.3.2.
	 * If two public fields are accessible from the specified className and fieldName,
	 * javac reports the ambiguous field reference as an error.
	 */
	public String resolveStaticFieldOwner(String className, String fieldName, String fieldDesc) {
		String key = getKey(className, fieldName, fieldDesc);
		String result = staticFieldCache.get(key);
		if (result == null) {
			result = resolveStaticFieldOwner(hierarchy, className, fieldName, fieldDesc);
			staticFieldCache.put(key, result);
		}
		return result;
	}

}
