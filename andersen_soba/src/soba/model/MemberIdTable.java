package soba.model;


import soba.util.ObjectIdMap;


/**
 * Translate MethodInfo to an ID integer that specifies the method.
 * @author ishio
 *
 */
public class MemberIdTable {
	
	private ObjectIdMap<IMethodInfo> methodToId;
	private ObjectIdMap<IFieldInfo> fieldToId;

	public MemberIdTable(ClassHierarchy ch) {
		methodToId = new ObjectIdMap<IMethodInfo>();
		fieldToId = new ObjectIdMap<IFieldInfo>();
		for (String className: ch.getClasses()) {
			IClassInfo c = ch.getClassInfo(className);
			for (int methodIndex=0; methodIndex<c.getMethodCount(); ++methodIndex) {
				IMethodInfo m = c.getMethod(methodIndex);
				methodToId.add(m);
//				methodToId.put(m, idToMethods.size());
//				idToMethods.add(m);
			}
			for (int fieldIndex=0; fieldIndex<c.getFieldCount(); ++fieldIndex) {
				IFieldInfo f = c.getField(fieldIndex);
				fieldToId.add(f);
			}
		}
	}
	
	public int getMethodCount() {
		return methodToId.size();
	}
	
	public int getMethodId(IMethodInfo m) {
		return methodToId.getId(m);
	}
	
	public IMethodInfo getMethod(int index) {
		return methodToId.getItem(index);
	}
	
	public int getFieldCount() {
		return fieldToId.size();
	}
	
	public int getFieldId(IFieldInfo f) {
		return fieldToId.getId(f);
	}
	
	public IFieldInfo getField(int index) {
		return fieldToId.getItem(index);
	}
}
