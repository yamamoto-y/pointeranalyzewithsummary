package soba.model;


public class Invocation {

	public enum Kind { STATIC, SPECIAL, VIRTUAL };
	
	private String className;
	private String methodName;
	private String methodDesc;
	private Kind invokeType;
	
	public Invocation(String className, String methodName, String methodDesc, Kind kind) {
		this.className = className;
		this.methodName = methodName;
		this.methodDesc = methodDesc;
		this.invokeType = kind;
	}
	
	/**
	 * @return true if the method is NOT a virtual method call.
	 * In other words, the method to be invoked is declared as static
	 * or a certain implementation is specified by the invocation,  
	 * e.g. "super.m()". 
	 */
	public boolean isStaticOrSpecial() {
		return invokeType != Kind.VIRTUAL;
	}
	
	/**
	 * @return true if the invocation calls a static method.
	 */
	public boolean isStaticMethod() {
		return invokeType == Kind.STATIC;
	}
	
	public String getClassName() {
		return className;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	public String getDescriptor() {
		return methodDesc;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(className);
		b.append(".");
		b.append(methodName);
		b.append(methodDesc);
		return b.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Invocation) {
				Invocation another = (Invocation)obj;
				boolean result = true;
				if (className != null) {
					result = result && className.equals(another.className);
				} else {
					result = result && (another.className == null);
				}
				if (methodName != null) {
					result = result && methodName.equals(another.methodName);
				} else {
					result = result && (another.methodName == null);
				}
				if (methodDesc != null) {
					result = result && methodDesc.equals(another.methodDesc);
				} else {
					result = result && (another.methodDesc == null);
				}
				result = result && (invokeType == another.invokeType);
				return result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		int classHash = 0;
		if (className != null) classHash = className.hashCode();
		int methodHash = 0;
		if (methodName != null) methodHash = methodName.hashCode();
		int descHash = 0;
		if (methodDesc != null) descHash = methodDesc.hashCode();
		return classHash ^ methodHash ^ descHash;
	}
	
}
