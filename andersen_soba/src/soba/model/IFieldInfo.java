package soba.model;

public interface IFieldInfo {

	public String getPackageName();
	public String getClassName();
	public String getFieldName();
	public String getDescriptor();
	
	/**
	 * @return a string representation of the field type.
	 */
	public String getFieldTypeName();
}
