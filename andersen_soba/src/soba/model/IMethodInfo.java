package soba.model;

public interface IMethodInfo {

	public String getPackageName();
	public String getClassName();
	public String getMethodName();
	public String getDescriptor();
	
	public boolean hasMethodBody();
	public IMethodBody getMethodBody();
	
	public int getParamCount();
	public String getParamType(int index);
	public String getReturnType();
	
	/**
	 * @return a string representation of the method signature.
	 */
	public String toLongString();
	
	public boolean isPackagePrivate();
	public boolean isOverridable();
	public boolean isStatic();
}
