package soba.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a helper class to obtain a fully qualified class name from 
 * a class name without package name.
 * @author ishio
 */
public class ClassNameResolver {

	private Map<String, List<String>> classNameToQualifiedName;

	public ClassNameResolver(String[] classNames) {
		classNameToQualifiedName = new HashMap<String, List<String>>();
		for (String className: classNames) {
			registerClassName(className);
		}
	}

	public ClassNameResolver(Iterable<String> classNames) {
		classNameToQualifiedName = new HashMap<String, List<String>>();
		for (String className: classNames) {
			registerClassName(className);
		}
	}
	
	private void registerClassName(String className) {
		String classNameWithoutPackage = extractClassNameWithoutPackage(className);
		List<String> classes = classNameToQualifiedName.get(classNameWithoutPackage);
		if (classes == null) {
			classes = new ArrayList<String>();
			classNameToQualifiedName.put(classNameWithoutPackage, classes);
		}
		classes.add(className);
	}
	
	public static String extractClassNameWithoutPackage(String className) {
		int index = className.lastIndexOf(IClassInfo.PACKAGE_SEPARATOR);
		if (index < 0 || index == className.length() - 1) {
			return className;
		} else {
			return className.substring(index + 1);
		}
	}
	
	/**
	 * @param classNameWithoutPackage specifies a class name excluding its package name.
	 * @return a list of class names including their package names.
	 * An immutable empty list is returned for an unknown class name.
	 */
	@SuppressWarnings("unchecked")
	public List<String> getClassList(String classNameWithoutPackage) {
		List<String> result = classNameToQualifiedName.get(classNameWithoutPackage);
		if (result == null) {
			return Collections.EMPTY_LIST;
		} else {
			return result;
		}
	}
	
	public boolean isUniqueClassName(String className) {
		return getClassList(extractClassNameWithoutPackage(className)).size() == 1;
	}
}
