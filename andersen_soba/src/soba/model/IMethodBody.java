package soba.model;

import java.util.List;

public interface IMethodBody {

	public List<Invocation> listMethodCalls();
	public List<FieldAccess> listFieldAccesses();
}

