/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;




public class ClassHierarchy {

	private boolean frozen;
	
	private static final String JAVA_LANG_OBJECT = "java" + IClassInfo.PACKAGE_SEPARATOR + "lang" + IClassInfo.PACKAGE_SEPARATOR + "Object"; 
	
	private Map<String, IClassInfo> entries; // type -> a method list of the type
	private Map<String, String> parentClass;   // type -> its super type 
	private Map<String, List<String>> parentInterfaces;  // type -> its interfaces
	private Map<String, Set<String>> subtypes; // type -> a set of sub types 
	private Set<String> requestedClasses; // a set of type names that are queried but not found

	private static List<String> EMPTY = Collections.unmodifiableList(new ArrayList<String>(0)); 
	
	
	public ClassHierarchy() {
		frozen = false;
		
		subtypes = new HashMap<String, Set<String>>();
		parentClass = new HashMap<String, String>();
		parentInterfaces = new HashMap<String, List<String>>();
		entries = new HashMap<String, IClassInfo>();

		requestedClasses = new HashSet<String>();

	}
	
	/**
	 * freeze() prevents further modifications to the object.
	 */
	public void freeze() {
		assert !frozen: "ClassHierarchy is already frozen."; 
		frozen = true;
	}
	
	public boolean isFrozen() {
		return frozen;
	}
	
	/**
	 * @return a set of class names which are requested 
	 * by client methods, but not involved in this class hierarchy.
	 */
	public Set<String> getRequestedClasses() {
		return requestedClasses;
	}
	
	public IClassInfo getClassInfo(String className) {
		IClassInfo c = entries.get(className);
		if (c == null) {
			requestedClasses.add(className);			
		}
		return c;
	}
	
	public int getClassCount() {
		return entries.size();
	}

	public Iterable<String> getClasses() {
		return entries.keySet();
	}
	
	/**
	 * Compare package names for the specified two types.
	 * @param typeName1 specifies a type to be compared.
	 * The type name must be registered to the class hierarchy. 
	 * @param typeName2 also specifies a type to be comapred.
	 * @return true if the specified types belong to the same package.
	 */
	public boolean isSamePackage(String typeName1, String typeName2) {
		IClassInfo c1 = entries.get(typeName1);
		IClassInfo c2 = entries.get(typeName2);
		
		if (c1 == null) requestedClasses.add(typeName1);
		if (c2 == null) requestedClasses.add(typeName2);
		
		return (c1 != null)&&(c2 != null)&&(c1.getPackageName().equals(c2.getPackageName()));
	}
	
	/**
	 * 
	 * @return This method returns a super class name for the given class.
	 * This method returns null for "java/lang/Object". 
	 * "java.lang.Object" is returned for an interface and an array type.
	 */
	public String getSuperClass(String className) { 
		if (isArrayType(className)) return JAVA_LANG_OBJECT;
		else {
			if (!parentClass.containsKey(className)) requestedClasses.add(className);
			return parentClass.get(className);
		}
	}
	
	/**
	 * @param className specifies a fully qualified class name. 
	 * @return interfaces implemented by the specified class.
	 * If the class has no interfaces, this method returns an empty collection.
	 * If the class is an interface and it extends another interface B,
	 * B is regarded as a super-interface of the class.
	 * If the specified class has no interfaces, 
	 * the result is an empty colleciton.
	 */
	public Collection<String> getSuperInterfaces(String className) {
		if (isArrayType(className)) return EMPTY;
		else if (parentInterfaces.containsKey(className)) { 
			return parentInterfaces.get(className);
		} else {
			if (!entries.containsKey(className)) requestedClasses.add(className);
			return EMPTY;
		}
	}
	
	/**
	 * List all direct and transitive super-types of a specified type. 
	 * @param className specifies a fully qualified class name. 
	 */
	public Collection<String> listAllSuperTypes(String className) {
		if (!entries.containsKey(className)) requestedClasses.add(className);

		Set<String> classes = new HashSet<String>();
		Queue<String> worklist = new LinkedList<String>();
		worklist.add(className);
		while (!worklist.isEmpty()) {
			String name = worklist.poll();
			String superClass = getSuperClass(name);
			if (superClass != null && !classes.contains(superClass)) {
				classes.add(superClass);
				worklist.add(superClass);
			}
			for (String s: getSuperInterfaces(name)) {
				if (s != null && !classes.contains(s)) {
					classes.add(s);
					worklist.add(s);
				}
			}
		}
		return classes;
	}
	

	public boolean isArrayType(String typeName) { 
		return typeName.endsWith("[]");
	}

	/**
	 * @return a collection of classes which extend/implement 
	 * the specified type.
	 * The result may be an empty collection.
	 */
	public Collection<String> getSubtypes(String typeName) {
		if (!entries.containsKey(typeName)) requestedClasses.add(typeName);

		if (subtypes.containsKey(typeName)) { 
			return subtypes.get(typeName);
		} else {
			return EMPTY;
		}
	}
	
	public Collection<String> getAllSubtypes(Iterable<String> typeNames) {
		Stack<String> worklist = new Stack<String>();
		for (String t: typeNames) {
			worklist.push(t);
		}
		
		HashSet<String> visited = new HashSet<String>();
		while (!worklist.isEmpty()) {
			String t = worklist.pop();
			if (visited.contains(t)) continue;
			
			visited.add(t);
			worklist.addAll(getSubtypes(t));
		}
		return visited;
	}

	/**
	 * This method registers a class info object to the hierarchy.
	 * This method calls registerSuperClass, registerSubtype and registerInterfaces. 
	 * @param c 
	 */
	public void registerClass(IClassInfo c) {
		if (frozen) throw new FrozenHierarchyException();
		entries.put(c.getClassName(), c);
		registerSuperClass(c.getClassName(), c.getSuperClass());
		registerSubtype(c.getClassName(), c.getSuperClass());
		registerInterfaces(c.getClassName(), c.getInterfaces());
		for (String interfaceName: c.getInterfaces()) {
			registerSubtype(c.getClassName(), interfaceName);
		}
	}
	
	/**
	 * This method allows developers to manually modify the class hierarchy.
	 * @param current specifies a class name. 
	 * @param parent specifies the super class name of the current class.
	 */
	public void registerSuperClass(String current, String parent) {
		if (frozen) throw new FrozenHierarchyException();
		parentClass.put(current, parent);
		
	}

	/**
	 * This method allows developers to manually modify the type hierarchy.
	 * A subtype of a class is a subclass.
	 * A subtype of an interface is an implementation class. 
	 * @param typeName
	 * @param parentTypeName
	 */
	public void registerSubtype(String typeName, String parentTypeName) {
		if (frozen) throw new FrozenHierarchyException();
		if (subtypes.containsKey(parentTypeName)) {
			subtypes.get(parentTypeName).add(typeName);
		} else {
			HashSet<String> types = new HashSet<String>();
			types.add(typeName);
			subtypes.put(parentTypeName, types);
		}
	}
	
	public void registerInterfaces(String current, List<String> interfaces) {
		if (frozen) throw new FrozenHierarchyException();
		parentInterfaces.put(current, interfaces);
	}
	
	public class FrozenHierarchyException extends RuntimeException {
		private static final long serialVersionUID = -8288161390304221032L;
	}

}
