package soba.util;


/**
 * This queue manages a pair of integer values.
 * addLast() method is provided to add a pair oof integer values. 
 * Instead of removeFirst(), this class provides "iterateUntilEmpty" method.
 * 
 * This class is not thread-safe.
 * @author ishio
 *
 */
public class IntPairQueue {
	
	private int[] ringBuf;
	private int startIndex, endIndex;

	public IntPairQueue() {
		this(65536);
	}

	/**
	 * @param capacity specifies the expected number of integer pairs 
	 * stored in a queue. 
	 */
	public IntPairQueue(int capacity) {
		if (capacity < 2) throw new EmptyQueueException("Cannot create a zero-length queue.");
		ringBuf = new int[capacity * 2];
		startIndex = 0;
		endIndex = 0;
	}

	public void add(int elem1, int elem2) {
		addLastOne(elem1);
		addLastOne(elem2);
	}
	
	public boolean isEmpty() {
		return startIndex == endIndex;
	}
	
	public void iterateUntilEmpty(IntPairProc proc) {
		boolean continueFlag = true;
		while (!isEmpty() && continueFlag) {
			int elem1 = removeFirstOne();
			int elem2 = removeFirstOne();
			continueFlag = proc.execute(elem1, elem2);
		}
	}

	private int removeFirstOne() {
		if (isEmpty()) throw new EmptyQueueException();
		int retValue = ringBuf[startIndex];
		startIndex++;
		if (startIndex == ringBuf.length) startIndex = 0;
		return retValue;
	}
	
	private void addLastOne(int value) {
		ringBuf[endIndex] = value;
		endIndex++;
		if (endIndex == ringBuf.length) endIndex = 0;
		if (startIndex == endIndex) { // ringBuf is full
			growUp();
		}
	}
	
	private void growUp() {
		int[] largerBuffer = new int[ringBuf.length * 2];
		int largerBufIndex = 0;
		int index = startIndex; 
		do {
			largerBuffer[largerBufIndex++] = ringBuf[index++];
			if (index == ringBuf.length) index = 0;
		} while (index != endIndex);
		
		ringBuf = largerBuffer;
		startIndex = 0;
		endIndex = largerBufIndex;
	}
	
	
	/**
	 * @return the number of integer pairs that can be stored 
	 * in an internal array of a queue.
	 */
	public int getCapacity() {
		return ringBuf.length / 2;
	}
	
	public static class EmptyQueueException extends RuntimeException {
		private static final long serialVersionUID = -5595992831196622099L;

		public EmptyQueueException() {
			super();
		}
		
		public EmptyQueueException(String message) {
			super(message);
		}
	}
}
