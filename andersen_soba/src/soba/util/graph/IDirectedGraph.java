package soba.util.graph;

import soba.util.IntPairProc;

public interface IDirectedGraph {

	public int getVertexCount();
	public int[] getEdges(int memberId);
	public void forEachEdge(IntPairProc proc);

}
