/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.util;

/**
 * Timer records time consumed by this program.
 * @author ishio
 */
public class Timer {

	private long startTimestamp;
	private long timestamp;

	/**
	 * Constructor starts a timer.
	 */
	public Timer() {
		timestamp = System.currentTimeMillis();
		startTimestamp = timestamp;
	}
	
	/**
	 * @return consumed milliseconds since a previous checkpoint.
	 */
	public long checkpoint() {
		long oldTimestamp = timestamp;
		timestamp = System.currentTimeMillis();
		return timestamp - oldTimestamp;
	}
	
	/**
	 * @return consumed milliseconds since the timer is created.
	 */
	public long getTotaltime() {
		return System.currentTimeMillis() - startTimestamp;
	}
}
