/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.util.files;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The factory creates a list of JAR files in the system class paths.
 * @author ishio
 */
public class ClasspathUtil {
	
	public static IFileEnumerator[] getEnumratorsFromString(String[] files) {
		return getEnumratorsFromString(Arrays.asList(files));
	}
	
	public static IFileEnumerator[] getEnumratorsFromString(List<String> files) { 
		List<IFileEnumerator> result = new ArrayList<IFileEnumerator>();
		for (String filepath: files) {
			File f = new File(filepath);
			if (f.isDirectory()) {
				Directory dir = new Directory(f);
				dir.enableRecursiveZipSearch();
				result.add(dir);
			} else if (ZipFile.isZipFile(f)) {
				ZipFile zip = new ZipFile(f);
				zip.enableRecursiveSearch();
				result.add(zip);
			}
		}
		return result.toArray(new IFileEnumerator[0]);
	}

	public static List<String> enumerateSystemClasspath() {
		final String PATH_SPLIT_REGEX = "\\s*" + File.pathSeparatorChar + "\\s*";
		List<String> classpath = new ArrayList<String>(1024);
		
        String classPath = System.getProperty("java.class.path");
        for (String path: classPath.split(PATH_SPLIT_REGEX)) {
        	classpath.add(new File(path).getAbsolutePath());
        }
        
        String bootClassPath = System.getProperty("sun.boot.class.path");
        for (String path: bootClassPath.split(PATH_SPLIT_REGEX)) {
        	classpath.add(new File(path).getAbsolutePath());
        }
        
        String extDirs = System.getProperty("java.ext.dirs");
        for (String extDirPath: extDirs.split(PATH_SPLIT_REGEX)) {
        	File extDir = new File(extDirPath);
        	if (extDir.isDirectory() && extDir.canRead()) {
        		File[] extFiles = extDir.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						String lowerFilename = pathname.getAbsolutePath();
						return lowerFilename.endsWith(".jar") || lowerFilename.endsWith(".zip");
					}
				});
        		for (File extFile: extFiles) {
        			classpath.add(extFile.getAbsolutePath());
        		}
        	}
        }
		return classpath;
	}

}
