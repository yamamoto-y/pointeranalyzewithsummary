package soba.util;


public class IntPairUtil {
	
	/**
	 * Create an IntPairList from an IntPairSet.
	 * The resultant list is not sorted;
	 * the order of elements depends on the set's implementation.
	 */
	public static IntPairList createList(IntPairSet set) {
		final IntPairList list = new IntPairList();
		set.foreach(new IntPairProc() {
			@Override
			public boolean execute(int elem1, int elem2) {
				list.add(elem1, elem2);
				return true;
			}
		});
		return list;
	}

}
