package soba.util;

/**
 * A fixed-size array for boolean values with undefined.
 * An element is one of true, false or undefined.
 *
 * @author ishio
 */
public class ThreeValueArray {
	
	private boolean[] assigned;
	private boolean[] value;
	private int trueCount;
	
	public ThreeValueArray(int size) {
		trueCount = 0;
		assigned = new boolean[size];
		value = new boolean[assigned.length];
	}
	
	/**
	 * @return the number of elements in the array.
	 */
	public int size() {
		return assigned.length;
	}

	/**
	 * Set a new value to a specified element.
	 * @param index
	 * @param newValue specifies true or false.
	 */
	public void setValue(int index, boolean newValue) {
		if (isTrue(index)) {
			if (!newValue) trueCount--;
		} else {
			if (newValue) trueCount++;
		}
		assigned[index] = true;
		value[index] = newValue;
	}
	
	/**
	 * @param index specifies an element in the array.
	 * @return true if an element is defined (true or false).
	 */
	public boolean isAssigned(int index) {
		return assigned[index];
	}
	
	public boolean isTrue(int index) {
		return assigned[index] && value[index];
	}

	public boolean isFalse(int index) {
		return assigned[index] && !value[index];
	}
	
	/**
	 * @return the number of true elements.
	 * Please note that this method accesses 
	 * all elements in the array. 
	 */
	public int getTrueCount() {
		int count = 0;
		for (int i=0; i<assigned.length; ++i) {
			if (isTrue(i)) ++count;
		}
		return count;
	}
}
