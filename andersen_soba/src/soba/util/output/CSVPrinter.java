package soba.util.output;

import java.io.PrintStream;

public class CSVPrinter {
	
	public static final String SEPARATOR_TAB = "\t";
	public static final String SEPARATOR_COMMA = ",";
	private static final String DEFAULT_SEPARATOR = "\t";

	private PrintStream stream;
	private String separator;
	private int columnCount;
	private int line;
	private int col;
	private ColumnType[] columnTypes;
	private boolean quoteString;

	public CSVPrinter(PrintStream stream, String[] columns, String separator) {
		this.stream = stream;
		this.columnCount = columns.length;
		this.separator = separator;
		this.quoteString = false;
		
		for (int i=0; i<columns.length; ++i) {
			stream.print(columns[i]);
			if (i<columns.length-1) {
				stream.print(this.separator);
			}
		}
		columnTypes = new ColumnType[columnCount];
		stream.println();
		col = 0;
		line = 0;
	}
	
	public CSVPrinter(PrintStream stream, String[] columns) {
		this(stream, columns, DEFAULT_SEPARATOR);
	}
	
	/**
	 * If this option is enabled, 
	 * each string for print() method is quoted 
	 * by double quotation characters.
	 * A dobule quotation character involved in a string 
	 * parameter for print() method is escaped by another 
	 * double quotation character so that Microsoft Excel can 
	 * correctly read the string value.   
	 */
	public void setUseQuotation(boolean enabled) {
		this.quoteString = enabled;
	}
	
	/**
	 * Specify a type constraint for a particular column.
	 * @param colIndex
	 * @param t specifies a type.  
	 * A type constraint can be removed by null.
	 */
	public void setColumnType(int colIndex, ColumnType t) {
		columnTypes[colIndex] = t;
	}
	
	public int getCurrentLine() {
		return line;
	}
	
	public void close() {
		stream.close();
	}
	
	public void printLineId() {
		print(line+1);
	}
	
	public void print(int i) {
		if (columnTypes[col] == null || columnTypes[col] == ColumnType.Integer) { 
			printInternal(Integer.toString(i));
		} else {
			throw new InvalidColumnTypeException(col, columnTypes[col], ColumnType.Integer);
		}
	}
	
	public void print(boolean b) {
		if (columnTypes[col] == null || columnTypes[col] == ColumnType.Boolean) { 
			if (b) printInternal("true");
			else printInternal("false");
		} else {
			throw new InvalidColumnTypeException(col, columnTypes[col], ColumnType.Boolean);
		}
	}

	public void print(String s) {
		if (columnTypes[col] == null || columnTypes[col] == ColumnType.String) { 
			if (quoteString) {
				printInternal(quoteString(s));
			} else {
				printInternal(s);
			}
		} else {
			throw new InvalidColumnTypeException(col, columnTypes[col], ColumnType.String);
		}
	}
	
	public static String quoteString(String s) {
		return "\"" + s.replaceAll("\"", "\"\"") + "\"";
	}
	
	private void printInternal(String s) {
		assert (col < columnCount): "There are no more columns. Columns=" + Integer.toString(columnCount);
		if (col != 0) {
			stream.print(this.separator);
		}
		stream.print(s);
		col++;
	}
	
	public void println() {
		assert (col == columnCount): "Not enough columns are output (" + Integer.toString(col) + "/" + Integer.toString(columnCount) + ")";
		stream.println();
		col = 0;
		line++;
	}
	
	public enum ColumnType {
		Integer("int"), Boolean("boolean"), String("String");

		private String typename;
		
		private ColumnType(String name) {
			this.typename = name;
		}
		
		public String toString() {
			return typename;
		}
	}
	
	public static class InvalidColumnTypeException extends RuntimeException {
		private static final long serialVersionUID = -2527237072314669502L;
		public InvalidColumnTypeException(int col, ColumnType expectedType,  ColumnType actualType) {
			super("Column " + col + " must be " + expectedType.toString() + " but " + actualType + " is specified.");
		}
	}
}
