package soba.util.debug;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Frame;


import soba.bytecode.MethodInfo;
import soba.bytecode.method.DataFlowEdge;
import soba.bytecode.method.DataFlowInfo;
import soba.bytecode.method.FastSourceValue;
import soba.bytecode.method.ILocalVariableInfo;
import soba.bytecode.method.OpcodeString;
import soba.util.IntPairProc;
import soba.util.Timer;
import soba.util.files.Directory;
import soba.util.files.IFileEnumerator;
import soba.util.files.IFileEnumeraterCallback;
import soba.util.files.SingleFile;
import soba.util.files.ZipFile;
import soba.util.graph.IDirectedGraph;

public class DumpClass {

	static Timer timer;
	static int classCount = 0;
	static int methodCount = 0;
	static int failedMethodCount = 0;
	static long instructionCount = 0;
	static long dataflowEdgeCount = 0;
	static boolean enableOutput = true;
	static boolean dumpStackframe = false;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		timer = new Timer();
		List<IFileEnumerator> files = new ArrayList<IFileEnumerator>();
		
		if (args.length == 0) {
			Directory dir = new Directory(new File("bin"));
			ZipFile asm = new ZipFile(new File("lib/asm-all-4.0.jar"));  
			files.add(dir);
			files.add(asm);
		} else {
			for (String arg: args) {
				if (arg.equals("--disable-output")) {
					enableOutput = false;
				} else if (arg.equals("--output-frame")) {
					dumpStackframe = true;
				} else {
					File f = new File(arg);
					if (f.isDirectory()) {
						Directory dir = new Directory(f);
						dir.enableRecursiveZipSearch();
						files.add(dir);
					} else if (ZipFile.isZipFile(f)) {
						ZipFile zip = new ZipFile(f);
						zip.enableRecursiveSearch();
						files.add(zip);
					} else {
						SingleFile file = new SingleFile(f);
						files.add(file);
					}
				}
			}
		}
		
		for (IFileEnumerator f: files) {
			f.process(new IFileEnumeraterCallback() {
				
				@Override
				public boolean reportError(String name, Exception e) {
					System.err.print("[ERROR] Last Entry: ");
					System.err.println(name);
					e.printStackTrace(System.err);
					return false;
				}
				
				@Override
				public void process(String name, InputStream stream) throws IOException {
					classCount++;
					processClass(stream);
				}
				
				@Override
				public boolean isTarget(String name) {
					return name.endsWith(".class");
				}
			});
		}
		System.err.println("FINISHED: " +  timer.getTotaltime() + " ms");
		System.err.println("#Classes: " +  classCount);
		System.err.println("#Methods: " +  methodCount);
		System.err.println("#Instructions: " + instructionCount);
		System.err.println("#Edges: " + dataflowEdgeCount);
		System.err.println("#Failed: " +  failedMethodCount);
	}
	
	
	@SuppressWarnings("unchecked")
	private static void processClass(InputStream in) throws IOException {
		ClassReader cr = new ClassReader(in);
		ClassNode node = new ClassNode(); 
		cr.accept(node, 0);
		
		List<MethodNode> methods = (List<MethodNode>)node.methods;
		for (MethodNode m: methods) {
			if (enableOutput) {
				System.out.println(constructMethodNameString(node, m));
			}
			
			MethodInfo methodInfo = new MethodInfo(m);
			if (!methodInfo.hasMethodBody()) continue;

			DataFlowInfo info = methodInfo.getMethodBody().getDataFlow();
			
			if (info != null) {
				methodCount++;
				instructionCount += info.getInstructionCount();

				
				for (int i=0; i<info.getInstructionCount(); ++i) {
					if (enableOutput) {
						System.out.println("  " + OpcodeString.getInstructionString(m, i));	
					}
					if (dumpStackframe) dumpStackframe(info, i);
				}
				
				List<DataFlowEdge> edges = info.getEdges();
				if (enableOutput) {
					for (DataFlowEdge e: edges) {
						System.out.print("    ");
						System.out.print(e.toString());
						System.out.print("  ");
						ILocalVariableInfo v = info.getLocalVariable(e);
						if (v != null) {
							System.out.print(v.getName() + ": " + v.getDescriptor());
						}
						System.out.println();
					}
				}
				dataflowEdgeCount += edges.size();
				
				IDirectedGraph cdepends = info.getControlDependence();
				if (enableOutput) {
					cdepends.forEachEdge(new IntPairProc() {
						@Override
						public boolean execute(int elem1, int elem2) {
							System.out.println("    [Control] " + elem1 + " -> " + elem2);
							return true;
						}
					});
				}
				
			} else {
				failedMethodCount++;
			}
		}
	}
	
	private static void dumpStackframe(DataFlowInfo info, int i) {
		Frame f = info.getFrame(i);
		if (f == null) {
			if (enableOutput) {
				System.out.print("    ");
				System.out.print(i);
				System.out.println(" [STACK] null");
			}
		} else {
			int size = f.getStackSize();
			for (int s=0; s<size; ++s) {
				FastSourceValue source = (FastSourceValue)f.getStack(s);
				for (int pos: source.getInstructions()) {
					if (enableOutput) {
						System.out.print("    ");
						System.out.print(i);
						System.out.print(" [STACK] <");
						System.out.print(s);
						System.out.print("/");
						System.out.print(size);
						System.out.print("> ");
						System.out.print(pos);
						System.out.print(": ");
						if (pos >= 0) {
							System.out.println(info.getInstruction(pos).toString());
						} else {
							System.out.println("method param");
						}
					}
				}
			}
			int locals = f.getLocals();
			for (int l=0; l<locals; ++l) {
				FastSourceValue source = (FastSourceValue)f.getLocal(l);
				for (int pos: source.getInstructions()) {
					if (enableOutput) {
						System.out.print("    ");
						System.out.print(i);
						System.out.print(" [LOCAL] <");
						System.out.print(l);
						System.out.print("/");
						System.out.print(locals);
						System.out.print("> ");
						System.out.print(pos);
						System.out.print(": ");
						if (pos >= 0) {
							System.out.println(info.getInstruction(pos).toString());
						} else {
							System.out.println("method param");
						}
					}
				}
			}

			int consumeStack = info.getOperandCount(i);
			int maxStack = f.getStackSize();
			for (int s=0; s<consumeStack; ++s) {
				FastSourceValue source = (FastSourceValue)f.getStack(maxStack-1-s);
				for (int pos: source.getInstructions()) {
					if (enableOutput) {
						System.out.print("    ");
						System.out.print(i);
						System.out.print(" [OPERAND");
						System.out.print(s);
						System.out.print("] ");
						System.out.print(pos);
						System.out.print(": ");
						if (pos >= 0) {
							System.out.println(info.getInstruction(pos).toString());
						} else {
							System.out.println("method param");
						}
					}
				}
			}
		}
		int[][] def = info.getDataDefinition(i);
		if (enableOutput) {
			for (int operandIndex=0; operandIndex<def.length; ++operandIndex) {
				System.out.print("    Operand " + Integer.toString(operandIndex) + " Data Dependence: ");
				if (def[operandIndex] != null) {
					if (def[operandIndex].length == 0) {
						System.out.print("EMPTY");
					} else {
						for (int fromIndex=0; fromIndex<def[operandIndex].length; ++fromIndex) {
							if (fromIndex>0) System.out.print(", ");
							System.out.print(def[operandIndex][fromIndex]);
						}
					}
				} else {
					System.out.print("null");
				}
				System.out.println();
			}
		}
	}		
	
	private static String constructMethodNameString(ClassNode c, MethodNode m) {
		StringBuilder buf = new StringBuilder();
		buf.append(c.name);
		buf.append("#");
		buf.append(m.name);
		buf.append("#");
		buf.append(m.desc);
		buf.append("#");
		buf.append(m.signature);
		if ((m.access & Opcodes.ACC_SYNTHETIC) != 0) {
			buf.append(" [Synthetic]");
		}
		
		return buf.toString();
	}
	
	@SuppressWarnings({ "unchecked", "unused" } ) 
	private static void dumpVariables(MethodNode m) {
		List<LocalVariableNode> variables = m.localVariables;
		if (variables != null) {
			System.out.println("---VARIABLES BEGIN---");
			for (LocalVariableNode v: variables) {
				System.out.println("   " + v.name + ": " + v.desc + " ");
			}
			System.out.println("---VARIABLES END---");
		}
	}
	
}
