package soba.util.debug;

import java.io.File;
import java.io.FileFilter;

public class ExecuteGraphviz {

	public static void main(String[] args) {
		for (String s: args) {
			File f = new File(s);
			compileAll(f);
		}
	}

	private static void compileAll(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles(new FileFilter() {
				@Override
				public boolean accept(File f) {
					return f.isDirectory() || f.canRead() && f.getName().contains(".dot");
				}
			});
			if (files != null) {
				for (File f: files) {
					compileAll(f);
				}
			}
		} else {
			execute(new String[] {
				"dot",
				"-Tpng",
				"-o",
				file.getAbsolutePath() + ".png",
				file.getAbsolutePath()
			});
		}
	}

	private static String arrayToString(String[] array) {
		StringBuilder builder = new StringBuilder();
		for (String s: array) {
			builder.append(s);
			builder.append(" ");
		}
		return builder.toString();
	}

	private static void execute(String[] command) {
		System.out.println(arrayToString(command));
		try {
			Runtime r;
			Process p;
			r = Runtime.getRuntime();
		    p = r.exec(command);
			p.waitFor();
			p.destroy();
		} catch (Exception e) {
			System.err.println("Failed to execute command: " + command.toString());
		}
	}

}
