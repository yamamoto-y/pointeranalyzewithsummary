package soba.util.options;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Configuration {

	private ArrayList<OptionItem> registeredOptions;
	
	// registeredOptions == options + keyValueOptions + defaultOption 
	private HashMap<String, OptionItem> options;
	private ArrayList<OptionItem> keyValueOptions;
	private OptionItem defaultOption;
	
	
	private ArrayList<String> extraOptions;
	
	public Configuration() {
		registeredOptions = new ArrayList<OptionItem>();
		options = new HashMap<String, OptionItem>();
		keyValueOptions = new ArrayList<OptionItem>();
		defaultOption = null;
		extraOptions = new ArrayList<String>();
	}
	
	/**
	 * The definition of precondition for 
	 * defineOptions methods.
	 * @param item
	 */
	private void checkConflict(OptionItem item) {
		if (registeredOptions.contains(item)) {
			throw new InvalidConfigException("The same option " + item.getName() + " is registered twice.");
		} else if (options.containsKey(item.getName())) {
			throw new InvalidConfigException("Option" + item.getName() + " is already used in the configuration.");
		} else {
			for (OptionItem keyValueOption: keyValueOptions) {
				if (item.getName().startsWith(keyValueOption.getName()) ||
					keyValueOption.getName().startsWith(item.getName())) {
					throw new InvalidConfigException("Option" + item.getName() + " conflicts with another option " + keyValueOption);
				} 
			}
		}
	}
	
	public void defineOptions(OptionItem... items) {
		for (OptionItem item: items) {
			checkConflict(item);
			registeredOptions.add(item);
			options.put(item.getName(), item);
		}
	}
	
	public void defineKeyValueOptions(OptionItem... items) {
		for (OptionItem item: items) {
			checkConflict(item);
			registeredOptions.add(item);
			keyValueOptions.add(item);
		}
	}
	
	public void defineDefaultOption(OptionItem item) {
		this.defaultOption = item;
		if (!registeredOptions.contains(item)) {
			registeredOptions.add(item);
		} 
	}
	
	public void parseArgs(String[] args) {
		for (int i=0; i<args.length; ++i) {
			// Find an OptionItem corresponding to the specified argument.
			OptionItem item = options.get(args[i]);  
			if (item != null) { 
				if (i+1 < args.length) {
					// If additional arguments are processed, increment the index variable.
					String[] optional = new String[args.length-i-1];
					for (int optionIndex=0; optionIndex<optional.length; ++optionIndex) {
						optional[optionIndex] = args[i+1+optionIndex];
					}
					i += item.parse(optional);
				} else {
					throw new InvalidParameterException(item.getName() + " option requires an additional argument.");
				}
			} else {
				boolean found = false;
				for (OptionItem keyValueOption: keyValueOptions) {
					if (args[i].startsWith(keyValueOption.getName())) {
						String arg = args[i].substring(keyValueOption.getName().length());
						keyValueOption.parse(new String[] {arg});
						found = true;
						break;
					}
				}
				if (!found) {
					extraOptions.add(args[i]);
				}
			}
		}
		if (defaultOption != null) {
			defaultOption.parse(extraOptions.toArray(new String[0]));
		}
	}
	
	public boolean isValid() {
		for (OptionItem item: registeredOptions) {
			if (!item.isValid()) return false;
		}
		return true;
	}
	
	public List<String> getExtraOptions() {
		return extraOptions;
	}

	public List<OptionItem> getOptions() {
		return Collections.unmodifiableList(registeredOptions);
	}
	
}
