package soba.util.options;

import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class CompositeOption extends OptionItem {

	private static final String DEFAULT_KEY_VALUE_SEPARATOR = "=";
	private static final String DEFAULT_PAIR_SEPARATOR = ",";
	
	private List<OptionItem> suboptions;
	private String keyValueSeparator;
	private String pairSeparator;
	private HashMap<String, OptionItem> suboptionMap;
	
	public CompositeOption(String name, OptionConstraint constraint, List<OptionItem> suboptions) {
		super(name, constraint);
		this.suboptions = suboptions;
		this.suboptionMap = new HashMap<String, OptionItem>();
		for (OptionItem option: suboptions) {
			suboptionMap.put(option.getName(), option);
		}
		
		this.keyValueSeparator = DEFAULT_KEY_VALUE_SEPARATOR;
		this.pairSeparator = DEFAULT_PAIR_SEPARATOR;
	}
	
	@Override
	public boolean hasValue() {
		for (OptionItem sub: suboptions) {
			if (!sub.hasValue()) return false;
		}
		return true;
	}
	
	public List<OptionItem> getSuboptions() {
		return suboptions;
	}
	
	@Override
	public int parse(String[] args) {
		if (args.length == 0) {
			throw new InvalidParameterException(getName() + " requires an additional argument.");
		}
		StringTokenizer tokens = new StringTokenizer(args[0], pairSeparator);
		while (tokens.hasMoreTokens()) {
			String pair = tokens.nextToken();
			int index = pair.indexOf(keyValueSeparator);
			if ((index <= 0)||(index > pair.length() - 1)) {
				throw new InvalidParameterException(pair + " is not a key" + keyValueSeparator + "value format.");
			} else {
				String key = pair.substring(0, index);
				String value = pair.substring(index + 1);
				String[] valueArray = new String[] {value};
				OptionItem item = suboptionMap.get(key);
				if (item != null) {
					item.parse(valueArray);
				} else {
					throw new InvalidParameterException(key + " is not a sub-option of " + getName() + ".");
				}
			}
		}
		return 1;
	}
	
}
