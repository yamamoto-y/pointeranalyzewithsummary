package soba.util.options;

public class InvalidParameterException extends RuntimeException {

	private static final long serialVersionUID = -5704080110319970277L;

	public InvalidParameterException(String msg) {
		super(msg);
	}

}
