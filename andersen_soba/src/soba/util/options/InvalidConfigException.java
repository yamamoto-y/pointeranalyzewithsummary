package soba.util.options;

public class InvalidConfigException extends RuntimeException {

	private static final long serialVersionUID = 6574735991443042777L;

	public InvalidConfigException(String msg) {
		super(msg);
	}
}
