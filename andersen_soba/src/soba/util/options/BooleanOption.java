package soba.util.options;

public class BooleanOption extends OptionItem {

	private boolean hasDefaultValue;
	private boolean defaultValue;
	
	private boolean hasValue;
	private boolean specifiedValue;
	
	public BooleanOption(String name) {
		super(name, OptionConstraint.Required);
		this.hasDefaultValue = false;
	}

	public BooleanOption(String name, boolean defaultValue) {
		super(name, OptionConstraint.Optional);
		this.hasDefaultValue = true;
		this.defaultValue = defaultValue;
	}
	
	public void clear() {
		hasValue = false;
	}
	
	@Override
	public int parse(String[] args) {
		if (hasValue) {
			throw new InvalidParameterException("The option is specified twice.");
		}
		specifiedValue = true;
		hasValue = true;
		return 0;
	}
	
	public boolean getValue() {
		if (hasValue) {
			return specifiedValue;
		} else if (hasDefaultValue) {
			return defaultValue;
		} else {
			throw new InvalidParameterException("No value is specified.");
		}
	}
	
	@Override
	public boolean hasValue() {
		return hasValue;
	}
	
}
