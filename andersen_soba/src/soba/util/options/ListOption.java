package soba.util.options;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

abstract class ListOption<T> extends OptionItem {

	private boolean hasDefaultValue;
	private List<T> defaultValue;

	private boolean hasValue;
	private List<T> specifiedValue;

	private String delimiter;

	public ListOption(String name) {
		super(name, OptionConstraint.Required);
		this.hasDefaultValue = false;
		this.specifiedValue = new ArrayList<T>();
		this.delimiter = null;
	}

	public ListOption(String name, List<T> defaultValue) {
		super(name, OptionConstraint.Optional);
		this.hasDefaultValue = true;
		// copy the default value list
		this.defaultValue = new ArrayList<T>();
		for (T v: defaultValue) {
			this.defaultValue.add(v);
		}
		this.specifiedValue = new ArrayList<T>();
	}

	/**
	 * Allow a list of values connected by delimiter.
	 * For example, enableDelimiter(",") allows
	 * to regard "-x A,B,C" as "-x A", "-x B" and "-x C".
	 * @param delimiter specifies a delimiter to
	 * split values.  If null, a list of values is disabled.
	 */
	public void enableDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	public void clear() {
		hasValue = false;
	}

	@Override
	public int parse(String[] args) {
		if (args.length == 0) {
			throw new InvalidParameterException(getName() + " requires an additional argument.");
		}
		if (delimiter != null) {
			StringTokenizer tokenizer = new StringTokenizer(args[0], delimiter);
			while (tokenizer.hasMoreTokens()) {
				specifiedValue.add(parseValue(tokenizer.nextToken()));
			}
		} else {
			specifiedValue.add(parseValue(args[0]));
		}
		hasValue = true;
		return 1;
	}

	public abstract T parseValue(String param);

	public List<T> getValue() {
		if (hasValue) {
			return specifiedValue;
		} else if (hasDefaultValue) {
			return defaultValue;
		} else {
			throw new InvalidParameterException("No value is specified.");
		}
	}

	@Override
	public boolean hasValue() {
		return hasValue;
	}

}
