package soba.util.options;

import java.util.List;

public class IntListOption extends ListOption<Integer> {

	public IntListOption(String name) {
		super(name);
	}

	public IntListOption(String name, List<Integer> defaultValue) {
		super(name, defaultValue);
	}
	
	@Override
	public Integer parseValue(String arg) {
		try {
			if (arg.startsWith("+")) arg = arg.substring(1);
			return Integer.valueOf(arg);
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Invalid value: " + arg + " is not an integer.");
		}
	}
	
}
