package soba.util.options;

import java.io.File;
import java.util.List;

public class FileListOption extends ListOption<File> {

	public FileListOption(String name) {
		super(name);
	}

	public FileListOption(String name, List<File> defaultValue) {
		super(name, defaultValue);
	}
	
	@Override
	public File parseValue(String param) {
		return new File(param);
	}
	
}
