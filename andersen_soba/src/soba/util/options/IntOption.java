package soba.util.options;

public class IntOption extends OptionItem {

	private boolean hasDefaultValue;
	private int defaultValue;
	
	private boolean hasValue;
	private int specifiedValue;
	
	public IntOption(String name) {
		super(name, OptionConstraint.Required);
		this.hasDefaultValue = false;
	}

	public IntOption(String name, int defaultValue) {
		super(name, OptionConstraint.Optional);
		this.hasDefaultValue = true;
		this.defaultValue = defaultValue;
	}
	
	public void clear() {
		hasValue = false;
	}
	
	@Override
	public int parse(String[] args) {
		if (args.length == 0) {
			throw new InvalidParameterException(getName() + " requires an additional argument.");
		}
		if (hasValue) {
			throw new InvalidParameterException("The option cannot have two or more values.");
		}
		try {
			String arg = args[0];
			if (arg.startsWith("+")) arg = arg.substring(1);
			specifiedValue = Integer.parseInt(arg);
			hasValue = true;
			return 1;
		} catch (NumberFormatException e) {
			hasValue = false;
			throw new InvalidParameterException("Invalid value: " + args[0] + " is not an integer.");
		}
	}
	
	public int getValue() {
		if (hasValue) {
			return specifiedValue;
		} else if (hasDefaultValue) {
			return defaultValue;
		} else {
			throw new InvalidParameterException("No value is specified.");
		}
	}
	
	@Override
	public boolean hasValue() {
		return hasValue;
	}
}
