package soba.util.options;

public abstract class OptionItem {
	
	private String name;
	private OptionConstraint constraint;

	protected OptionItem(String name, OptionConstraint constraint) {
		this.name = name;
		this.constraint = constraint;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isValid() {
		if (constraint == OptionConstraint.Required) {
			return hasValue();
		} else {
			return true;
		}
	}
	

	/**
	 * Parse additional arguments for the option.
	 * @param values is a list of arguments that follow the option.
	 * @return the number of arguments processed by the option.
	 */
	public abstract int parse(String[] values);

	public abstract boolean hasValue();

}
