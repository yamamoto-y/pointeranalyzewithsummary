package soba.util.options;

public class StringOption extends OptionItem {

	private boolean hasDefaultValue;
	private String defaultValue;
	
	private boolean hasValue;
	private String specifiedValue;
		
	public StringOption(String name) {
		super(name, OptionConstraint.Required);
		this.hasDefaultValue = false;
	}

	public StringOption(String name, String defaultValue) {
		super(name, OptionConstraint.Optional);
		this.hasDefaultValue = true;
		this.defaultValue = defaultValue;
	}
	
	public void clear() {
		hasValue = false;
	}
	
	@Override
	public int parse(String[] args) {
		if (args.length == 0) {
			throw new InvalidParameterException(getName() + " requires an additional argument.");
		}
		if (hasValue) {
			throw new InvalidParameterException("The option cannot have two or more values.");
		}
		specifiedValue = args[0];
		hasValue = true;
		return 1;
	}
	
	public String getValue() {
		if (hasValue) {
			return specifiedValue;
		} else if (hasDefaultValue) {
			return defaultValue;
		} else {
			throw new InvalidParameterException("No value is specified.");
		}
	}
	
	@Override
	public boolean hasValue() {
		return hasValue;
	}
}
