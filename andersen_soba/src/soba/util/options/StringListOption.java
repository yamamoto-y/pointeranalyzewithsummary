package soba.util.options;

import java.util.List;

public class StringListOption extends ListOption<String> {

	public StringListOption(String name) {
		super(name);
	}

	public StringListOption(String name, List<String> defaultValue) {
		super(name, defaultValue);
	}
	
	@Override
	public String parseValue(String param) {
		return param;
	}

}
