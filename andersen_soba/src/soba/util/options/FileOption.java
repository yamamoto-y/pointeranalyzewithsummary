package soba.util.options;

import java.io.File;

public class FileOption extends OptionItem {

	private boolean hasDefaultValue;
	private File defaultValue;
	
	private boolean hasValue;
	private File specifiedValue;
	
	public FileOption(String name) {
		super(name, OptionConstraint.Required);
		this.hasValue = false;
		this.hasDefaultValue = false;
	}

	public FileOption(String name, File defaultValue) {
		super(name, OptionConstraint.Optional);
		this.hasDefaultValue = true;
		this.hasValue = false;
		this.defaultValue = defaultValue;
	}
	
	public void clear() {
		hasValue = false;
	}
	
	@Override
	public int parse(String[] args) {
		if (args.length == 0) {
			throw new InvalidParameterException(getName() + " requires an additional argument.");
		}
		if (hasValue) {
			throw new InvalidParameterException("The option cannot have two or more values.");
		} else {
			specifiedValue = new File(args[0]);
			hasValue = true;
			return 1;
		}
	}
	
	public File getValue() {
		if (hasValue) {
			return specifiedValue;
		} else if (hasDefaultValue) {
			return defaultValue;
		} else {
			throw new InvalidParameterException("No value is specified.");
		}
	}
	
	@Override
	public boolean hasValue() {
		return hasValue;
	}
	
}
