package soba.util;

import java.util.HashMap;

public class ObjectCache<T> {

	private HashMap<T, T> cache;
	
	public ObjectCache() {
		cache = new HashMap<T, T>(65536);
	}
	
	public T getSharedInstance(T key) {
		if (key == null) return null;
		T value = cache.get(key);
		if (value == null) {
			cache.put(key, key);
			return key;
		} else {
			return value;
		}
	}
	
}
