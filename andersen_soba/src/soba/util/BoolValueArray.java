package soba.util;

/**
 * A fixed-size array for boolean value.
 * @author ishio
 */
public class BoolValueArray {
	
	private boolean[] value;
	private int trueCount;
	
	public BoolValueArray(int size) {
		trueCount = 0;
		value = new boolean[size];
	}
	
	public int size() {
		return value.length;
	}

	public void setValue(int index, boolean newValue) {
		if (getValue(index)) {
			if (!newValue) trueCount--;
		} else {
			if (newValue) trueCount++;
		}
		value[index] = newValue;
	}
	
	public boolean getValue(int index) {
		return value[index];
	}

	public int getTrueCount() {
		int count = 0;
		for (int i=0; i<size(); ++i) {
			if (value[i]) ++count;
		}
		return count;
	}
	
	public boolean equals(BoolValueArray another) {
		if (this.size() == another.size()) {
			for (int i=0; i<size(); ++i) {
				if (this.value[i] != another.value[i]) return false;
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param another must have the same number of elements.
	 * @return true if two arrays have no "true" values
	 * at the same index. 
	 */
	public boolean isExclusive(BoolValueArray another) {
		if (this.size() == another.size()) {
			for (int i=0; i<size(); ++i) {
				if (this.value[i] && another.value[i]) return false;
			}
			return true;
		} else {
			return false;
		}
	}
	
}
