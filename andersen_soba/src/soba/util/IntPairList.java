/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.util;

import java.util.Arrays;


public class IntPairList {

	private int count;
	private long[] values;
	private boolean frozen;

	public IntPairList() {
		this(1024);
	}

	public IntPairList(int capacity) {
		values = new long[capacity]; 
		count = 0;
	}
	
	public int size() {
		return count; 
	}
	
	private long compose(int elem1, int elem2) {
		return (((long)elem1) << 32) | elem2;
	}

	public int getFirstValue(int index) {
		if (index < 0 || index >= count) throw new ArrayIndexOutOfBoundsException(index);
		return (int)(values[index] >> 32);
	}

	public int getSecondValue(int index) {
		if (index < 0 || index >= count) throw new ArrayIndexOutOfBoundsException(index);
		return (int)(values[index] & 0xFFFFFFFF);
	}

	public void add(int elem1, int elem2) {
		if (!frozen) {
			if (values.length == count) growUp();
			values[count] = compose(elem1, elem2);
			count++;
		} else {
			throw new FrozenListException();
		}
	}
	
	public void addAll(IntPairList another) {
		if (!frozen) {
			int anotherSize = another.size();
			for (int i=0; i<anotherSize; ++i) {
				if (values.length == count) growUp();
				values[count] = another.values[i];
				count++;
			}
		} else {
			throw new FrozenListException();
		}
	}

	private void growUp() {
		long[] newValues = new long[values.length * 2];
		for (int i=0; i<count; ++i) {
			newValues[i] = values[i];
		}
		values = newValues;
	}
	
	
	public void setFirstValue(int index, int first) {
		if (!frozen) {
			if ((index < 0) || (count <= index)) {
				throw new ArrayIndexOutOfBoundsException();
			} else {
				int second = getSecondValue(index);
				values[index] = compose(first, second);
			}
		} else {
			throw new FrozenListException();
		}
	}
	
	public void setSecondValue(int index, int second) {
		if (!frozen) {
			if ((index < 0) || (count <= index)) {
				throw new ArrayIndexOutOfBoundsException();
			} else {
				int first = getFirstValue(index);
				values[index] = compose(first, second);
			}
		} else {
			throw new FrozenListException();
		}
	}
	
	public void sort() {
		Arrays.sort(values, 0, count);
	}
	
	public void foreach(IntPairProc proc) {
		boolean cont = true;
		for (int i=0; cont && (i<count); ++i) {
			cont = proc.execute(getFirstValue(i), getSecondValue(i));
		}
	}
	
	
	/**
	 * This method freezes the list and releases 
	 * unnecessary memory buffer for additional elements.
	 * If you add a new pair of integers, 
	 * this list throws an exception.
	 */
	public void freeze() {
		frozen = true;
	}
	
	public static class FrozenListException extends RuntimeException {
		private static final long serialVersionUID = 1519361503126979153L;
	}
	
}
