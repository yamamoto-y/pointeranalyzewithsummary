/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.util;

import java.util.EmptyStackException;


public class IntStack {

	private int count = 0;
	private int[] values;

	public IntStack() {
		this(1024);
	}
	
	public IntStack(int capacity) {
		count = 0;
		values = new int[capacity];
	}
	
	public boolean isEmpty() {
		return count == 0;
	}
	
	public void push(int value) {
		if (count >= values.length) {
			growUp();
		}
		values[count] = value;
		count++;
	}
	
	public int pop() {
		if (count == 0) throw new EmptyStackException();
		count--;
		return values[count];
	}
	
	public int peek() {
		if (count == 0) throw new EmptyStackException();
		return values[count-1];
	}
	
	public boolean contains(int value) {
		for (int i=0; i<count; ++i) {
			if (values[i] == value) return true;
		}
		return false;
	}

	private void growUp() {
		int[] newValues = new int[values.length * 2];
		for (int i=0; i<count; ++i) {
			newValues[i] = values[i];
		}
		values = newValues;
	}
	
	public int size() {
		return count;
	}
}
