/*
 * Copyright (C) 2010 Takashi Ishio All Rights Reserved.
 * You can use this source code for any purpose
 * except for including this source code in a commercial product.
 */
package soba.util;

import gnu.trove.map.hash.TObjectIntHashMap;

import java.util.ArrayList;


public class ObjectIdMap<T> {
	
	private TObjectIntHashMap<T> map;
	private ArrayList<T> idToObject;
	private boolean frozen;
	
	public ObjectIdMap() {
		this(1024 * 1024);
	}

	public ObjectIdMap(int capacity) {
		map = new TObjectIntHashMap<T>(capacity * 2);
		idToObject = new ArrayList<T>(capacity);
		frozen = false;
	}
	
	/**
	 * Disable assigning new IDs.
	 * For frozen maps, getId method with a new object 
	 * returns -1 (or throws AssertionError).
	 */
	public void freeze() {
		frozen = true;
	}
	
	public void add(T s) {
		getId(s);
	}
	
	/**
	 * @param item specifies an object.
	 * @return the ID integer corresponding to the object.
	 * If a new object is given, this method returns a new id.
	 */
	public int getId(T item) {
		if (map.containsKey(item)) {
			return map.get(item);
		} else {
			if (frozen) {
				// A new object is added to the frozen id map.
				throw new FrozenMapException();
			} else {
				int newId = idToObject.size();
				map.put(item, newId);
				idToObject.add(item);
				return newId;
			}
		}
	}
	
	public T getItem(int id) {
		if ((id < 0) || (id >= idToObject.size())) return null;
		else return idToObject.get(id);
	}
	
	public int size() {
		return idToObject.size();
	}
	
	public static class FrozenMapException extends RuntimeException {
		
		private static final long serialVersionUID = -1682458461699095201L;

		public FrozenMapException() {
			super();
		}
	}

}
