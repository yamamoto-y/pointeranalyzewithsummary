package soba.experimental.statistics;

import java.util.ArrayList;
import java.util.List;

import soba.bytecode.MethodInfo;
import soba.bytecode.method.LocalVariableTable;
import soba.util.BoolValueArray;
import soba.util.graph.DepthFirstSearch;
import soba.util.graph.IDirectedGraph;


public class LocalDataFlowGraph {

	private ArrayList<LocalDataFlowEdge> dataFlowEdges = new ArrayList<LocalDataFlowEdge>();
	private MethodInfo methodInfo;
	private LocalVariableTable variableInfo;
	
	private int[][] useCount; // varId -> (instrucitonId+1) -> useCount
	private int[][] defCount; // varId -> (instrucitonId+1) -> defCount
	private BoolValueArray[][] edges;
	private soba.util.ThreeValueArray independentPaths;

	public LocalDataFlowGraph(MethodInfo methodInfo, IDirectedGraph cfg, LocalVariableTable vInfo) {
		this.methodInfo = methodInfo;
		this.variableInfo = vInfo;

		// Connect edges to all use vertices of "this" variable.
		// These edges ignore control flow because a control-flow graph 
		// may exclude a control-flow caused by exception handling. 
		int startParam;
		if (methodInfo.isStatic()) startParam = 0;
		else {
			startParam = 1;
			final int THIS_INDEX = 0;
			int thisId = variableInfo.findParamId(THIS_INDEX);
			for (int i=0; i<methodInfo.getInstructionCount(); ++i) {
				if (variableInfo.isUse(i)) {
					if (variableInfo.getAccessedVariableId(i) == thisId) {
						dataFlowEdges.add(new LocalDataFlowEdge(LocalDataFlowEdge.PARAM_VERTEX_INDEX, i, thisId));
					}
				}
			}
		}
		
		// Search def-use for each method argument except for "this".
		for (int paramIndex=startParam; paramIndex<methodInfo.getParamCount(); ++paramIndex) {
			DepthFirstSearch.search(cfg, 0, new LocalDefUseAnalysis(variableInfo, variableInfo.findParamId(paramIndex), true, dataFlowEdges));
		}

		// Search def-use for each store operation.
		for (int srcIndex=0; srcIndex<methodInfo.getInstructionCount(); ++srcIndex) {
			if (variableInfo.isDef(srcIndex)) {
				DepthFirstSearch.search(cfg, srcIndex, new LocalDefUseAnalysis(variableInfo, variableInfo.getAccessedVariableId(srcIndex), false, dataFlowEdges));
			}
		}
		
		// Summarize the result
		useCount = new int[variableInfo.size()][1 + methodInfo.getInstructionCount()];
		defCount = new int[variableInfo.size()][1 + methodInfo.getInstructionCount()];
		for (LocalDataFlowEdge e: dataFlowEdges) {
			useCount[ e.getVariableId() ][ e.getDefVertex() + 1 ] += 1;
			defCount[ e.getVariableId() ][ e.getUseVertex() + 1 ] += 1;
		}
	}
	
	/**
	 * Returns the number of use instructions for the specified definition instruction.
	 * @param variableId
	 * @param defInstructionIndex
	 * @return
	 */
	public int getUseCount(int variableId, int defInstructionIndex) {
		return useCount[ variableId ][ defInstructionIndex + 1 ];
	}

	public int getDefCount(int variableId, int useInstructionIndex) {
		return defCount[ variableId ][ useInstructionIndex + 1 ];
	}
	
	/**
	 * @return true if data-flow edges form a set of subgraphs whose 
	 *  all definition vertices are connected to all use vertices in the subgraph. 
	 *  
	 */
	public boolean hasIndependentPaths(int variableId) {
		if (edges == null) {
			// Translate dataflow edges to boolean arrays
			edges = new BoolValueArray[variableInfo.size()][1 + methodInfo.getInstructionCount()];
			for (LocalDataFlowEdge e: dataFlowEdges) {
				if (edges[e.getVariableId() ][ e.getDefVertex() + 1] == null) {
					edges[e.getVariableId() ][ e.getDefVertex() + 1] = new BoolValueArray(methodInfo.getInstructionCount());
				}
				edges[e.getVariableId() ][ e.getDefVertex() + 1].setValue(e.getUseVertex(), true);
			}
			independentPaths = new soba.util.ThreeValueArray(variableInfo.size());
			
			// If the variable has at least one unreachable "use" vertex, it is not an "independent path".
			for (int i=0; i<methodInfo.getInstructionCount(); ++i) {
				if (isUseVertex(variableId, i) && getDefCount(variableId, i) == 0) {
					independentPaths.setValue(variableId, false);
				}
			}
		}
		if (!independentPaths.isAssigned(variableId)) {
			// Check the condition on the boolean arrays 
			BoolValueArray[] varUses = edges[variableId];
			boolean success = true;
			// two def vertices have the same use vertices OR exclusively different use vertices.
			for (int i=0; i<varUses.length && success; ++i) {
				BoolValueArray array1 = varUses[i];
				for (int j=i+1; j<varUses.length && success; ++j) {
					BoolValueArray array2 = varUses[j];
					if ((array1 != null) && (array2 != null)) {
						if (array1.equals(array2) || array1.isExclusive(array2)) {
							// ok; continue the process. 
						} else {
							success = false;
						}
					}
				}			
			}
			independentPaths.setValue(variableId, success);
		}
		return independentPaths.isTrue(variableId);
	}
	
	private boolean isUseVertex(int variableId, int instructionIndex) { 
		return variableInfo.isUse(instructionIndex) && 
				(variableInfo.getAccessedVariableId(instructionIndex) == variableId);
	}

	public int getDefUsePathCount(int variableId) {
		int count = 0;
		for (int i=0; i<dataFlowEdges.size(); ++i) {
			if (dataFlowEdges.get(i).getVariableId() == variableId) count++;
		}
		return count;
	}
	
	public List<LocalDataFlowEdge> getDataFlowEdges() {
		return dataFlowEdges;
	}
	
}
