package soba.experimental.statistics;

import java.util.List;

import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.ClassInfo;
import soba.bytecode.MethodInfo;
import soba.bytecode.method.DataFlowEdge;
import soba.bytecode.method.DataFlowInfo;
import soba.bytecode.method.ILocalVariableInfo;
import soba.bytecode.method.LocalVariableTable;
import soba.util.graph.IDirectedGraph;


public class MethodDataFlow {

	private MethodInfo methodInfo;
	private MethodNode methodNode;
	private LocalVariableTable variableInfo;
	private LocalDataFlowGraph simpleDFG;
	private IDirectedGraph conservativeCFG;
	private LocalDataFlowGraph conservativeDFG;
	private LocalDefUseCount defUse;
	private FieldAccessList fieldAccess;
	private ArrayDefUseAnalysis arrayDefUse;
	private DataFlowInfo dataflow;
	
	public MethodDataFlow(ClassInfo c, MethodNode methodNode) {
		methodInfo = new MethodInfo(c, methodNode);
		this.methodNode = methodNode;
		variableInfo = new LocalVariableTable(methodInfo, methodNode);
		IDirectedGraph simpleCFG = methodInfo.getMethodBody().getDataFlow().getNormalControlFlow();
        simpleDFG = new LocalDataFlowGraph(methodInfo, simpleCFG, variableInfo);

        dataflow = methodInfo.getMethodBody().getDataFlow();
        conservativeCFG = dataflow.getConservativeControlFlow();
        conservativeDFG = new LocalDataFlowGraph(methodInfo, conservativeCFG, variableInfo);
        defUse = new LocalDefUseCount(methodInfo, variableInfo);
        fieldAccess = new FieldAccessList(methodInfo);
        arrayDefUse = ArrayDefUseAnalysis.analyze(methodNode, conservativeCFG);
	}
	
	public MethodInfo getMethodInfo() {
		return methodInfo;
	}
	
	public IDirectedGraph getConservativeCFG() {
		return conservativeCFG;
	}

	public String getMethodFileName() {
		String methodName = methodInfo.getMethodName();
		if (methodName.equals("<init>")) methodName = "INIT";
		else if (methodName.equals("<clinit>")) methodName = "CLINIT";
		
		return methodName + "_" + Integer.toHexString(methodInfo.getDescriptor().hashCode());
	}

	public int getVariableCount() {
		return variableInfo.size();
	}
	
	public int getParamCount() {
		return methodInfo.getParamCount();
	}
	
	/**
	 * @return the number of variables whose name is available.
	 */
	public int getNamedVariableCount() {
		return variableInfo.getNamedVariableCount();
	}
	
//	public LocalDataFlowGraph getSimpleDFG() {
//		return simpleDFG;
//	}
//	
//	public LocalDataFlowGraph getConservativeDFG() {
//		return conservativeDFG;
//	}
	
	public FieldAccessList getFieldAccess() {
		return fieldAccess;
	}
	
	public int getArrayDefUseCount() {
		return arrayDefUse.getEdgeCount();
	}
	
	public int getBytecodeVariableIndex(int variableId) {
		return defUse.getBytecodeVariableIndex(variableId);
	}
	
	public int getDefCount(int variableId) {
		return defUse.getDefCount(variableId);
	}
	
	public String getVariableKind(int variableId) {
		if (variableInfo.isThis(variableId)) {
			return "this";
		} else if (variableInfo.isParam(variableId)) {
			return "param";
		} else if (variableInfo.isAnonymous(variableId)) {
			return "anonymous";
		} else if (variableInfo.isUnknown(variableId)) {
			return "unknown";
		} else  {
			return "local";
		}
	}
	

//	public boolean isThis(int methodIndex, int variableId) {
//	return methods.get(methodIndex).variableInfo.isThis(variableId);
//}

	public String getDataFlowKind(int variableId) {
		int def = getDefCount(variableId);
		int use = getUseCount(variableId);
		int infeasible = def * use - getSimpleDefUsePathCount(variableId);
		if (infeasible == 0) {
			if (variableInfo.isThis(variableId)) {
				return "Correct-This";
			} else if (variableInfo.isAnonymous(variableId)) {
				return "Correct-Anonymous";
			} else if (variableInfo.isUnknown(variableId)) {
				return "Correct-Unknown";
			} else {
				return "Correct-Regular";
			}
		} else { // has one or more infeasible paths
			int conservativeInfeasible = def * use - getConservativeDefUsePathCount(variableId); 
			if (conservativeInfeasible == 0) {
				return "Infeasible-TryCatch";
			} else {
				if (simpleDFG.hasIndependentPaths(variableId)) {
					return "Infeasible-Independent";
				} else if (conservativeDFG.hasIndependentPaths(variableId)) {
					return "Infeasible-TryCatch-Independent";
				} else if (defUse.isStoreAndIncrement(variableId)) {
					return "Infeasible-Index";
				} else {
					return "Infeasible-Regular";
				}
			}
		}
	}

	public int getConservativeUseCount(int variableId, int defInstructionIndex) {
		return conservativeDFG.getUseCount(variableId, defInstructionIndex);
	}

	public int getSimpleUseCount(int variableId, int defInstructionIndex) {
		return simpleDFG.getUseCount(variableId, defInstructionIndex);
	}

	public int getConservativeDefCount(int variableId, int useInstructionIndex) {
		return conservativeDFG.getDefCount(variableId, useInstructionIndex);
	}

	public int getSimpleDefCount(int variableId, int useInstructionIndex) {
		return simpleDFG.getDefCount(variableId, useInstructionIndex);
	}

	public int getUseCount(int variableId) {
		return defUse.getUseCount(variableId);
	}

	public int getConservativeDefUsePathCount(int variableId) {
		return conservativeDFG.getDefUsePathCount(variableId);
	}

	public int getSimpleDefUsePathCount(int variableId) {
		return simpleDFG.getDefUsePathCount(variableId);
	}

	public String getMethodName() {
		return methodNode.name;
	}

	public String getMethodDesc() {
		return methodNode.desc;
	}

	public String getMethodSignature() {
		return methodNode.signature;
	}

	public String getVariableName(int variableId) {
		return variableInfo.getName(variableId);
	}

	public String getVariableType(int variableId) {
		return variableInfo.getType(variableId);
	}

	public List<DataFlowEdge> getConservativeDataFlowEdges() {
        return dataflow.getEdges();
	}
	
	public ILocalVariableInfo getLocalVariable(DataFlowEdge e) {
		return dataflow.getLocalVariable(e);
	}

}
