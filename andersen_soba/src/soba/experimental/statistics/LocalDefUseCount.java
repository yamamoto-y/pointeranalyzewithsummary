package soba.experimental.statistics;


import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.MethodInfo;
import soba.bytecode.method.LocalVariableTable;
import soba.bytecode.method.OpcodeString;

/**
 * A class representing the number of def/use instructions
 * for each variable.
 * @author ishio
 */
public class LocalDefUseCount {

	private LocalVariableTable variableInfo;
	private int[] defs;
	private int[] uses;
	private int[] store;
	private int[] increment;
	
	public LocalDefUseCount(MethodInfo methodInfo, LocalVariableTable variableInfo) {
		this.variableInfo = variableInfo;
		defs = new int[variableInfo.size()];
		uses = new int[variableInfo.size()];
		store = new int[variableInfo.size()];
		increment = new int[variableInfo.size()];

		// Count parameter defs
		for (int i=0; i<methodInfo.getParamCount(); ++i) {
			defs[variableInfo.findParamId(i)] += 1;
		}
		
		// Count variable def/use
		MethodNode method = methodInfo.getMethodBody().getMethodNode();
		for (int i=0; i<method.instructions.size(); ++i) {
			if (variableInfo.isDef(i)) {
				defs[variableInfo.getAccessedVariableId(i)] += 1;
			} 
			if (variableInfo.isUse(i)) {
				uses[variableInfo.getAccessedVariableId(i)] += 1;
			}				
			if (OpcodeString.isStoreOperation(method.instructions.get(i))) {
				store[variableInfo.getAccessedVariableId(i)] += 1;
			} else if (OpcodeString.isIncrementOperation(method.instructions.get(i))) {
				increment[variableInfo.getAccessedVariableId(i)] += 1;
			}
		}
	}
	
	public int getVariableCount() {
		return variableInfo.size();
	}
	
	/**
	 * @param variableId specifies a variable.  The maximum value is getVariableCount()-1.
	 * @return a variable index in Java bytecode.
	 */
	public int getBytecodeVariableIndex(int variableId) {
		return variableInfo.getVariableIndex(variableId);
	}
	
	public String getVariableName(int variableId) {
		return variableInfo.getName(variableId);
	}
	
	public int getDefCount(int variableId) {
		return defs[variableId];
	}
	
	public int getUseCount(int variableId) { 
		return uses[variableId];
	}
	
	public boolean isStoreAndIncrement(int variableId) {
		return (store[variableId] == 1) && (increment[variableId] > 0);
	}
	
}
