package soba.experimental.statistics;



import gnu.trove.iterator.TIntIterator;
import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;
import soba.bytecode.method.DataFlowEdge;
import soba.util.IntStack;


public class TransitiveDataflowPath {

	private InstructionList method;
	
	/**
	 * "backwardEdges.get(destInstruction).get(destOperand)" 
	 * is a list of source vertices which have an direct edge to 
	 * the specified destination vertex.
	 */
	private TIntObjectHashMap<TIntObjectHashMap<TIntHashSet>> backwardEdges;
	
	private static TIntObjectHashMap<TIntHashSet> EMPTY_MAP = new TIntObjectHashMap<TIntHashSet>();
	private static TIntHashSet EMPTY_SET = new TIntHashSet();
	
	public TransitiveDataflowPath(InstructionList m) {
		this.method = m;
		backwardEdges = new TIntObjectHashMap<TIntObjectHashMap<TIntHashSet>>();
		
	}
	
	public void addEdge(DataFlowEdge e) {
		int sourceInstruction = e.getSourceInstruction();
		if (e.isParameter()) {
			sourceInstruction = method.getInstructionCount() + e.getVariableIndex();
		}

		addEdgeInternal(sourceInstruction, 
				e.getDestinationInstruction(), 
				e.getDestinationOperandIndex());
	}
	
	public void addArtificialEdge(DataFlowEdge source, DataFlowEdge destination) {
		int sourceInstruction = source.getSourceInstruction();
		if (source.isParameter()) {
			sourceInstruction = method.getInstructionCount() + source.getVariableIndex();
		}

		addEdgeInternal(sourceInstruction, 
				destination.getDestinationInstruction(), 
				destination.getDestinationOperandIndex());
	}

	private void addEdgeInternal(int source, int destInstruction, int destOperand) {
		TIntObjectHashMap<TIntHashSet> operands = backwardEdges.get(destInstruction);
		if (operands == null) {
			operands = new TIntObjectHashMap<TIntHashSet>();
			backwardEdges.put(destInstruction, operands);
		}
		TIntHashSet sources = operands.get(destOperand);
		if (sources == null) {
			sources = new TIntHashSet();
			operands.put(destOperand, sources);
		}
		sources.add(source);
	}
	
	
	private TIntObjectHashMap<TIntHashSet> getOperands(int instruction) {
		TIntObjectHashMap<TIntHashSet> operands = backwardEdges.get(instruction);
		if (operands != null) {
			return operands;
		} else {
			return EMPTY_MAP;
		}
	}
	
	private TIntHashSet getSources(int instruction, int operand) { 
		TIntObjectHashMap<TIntHashSet> operands = getOperands(instruction);
		TIntHashSet sources = operands.get(operand);
		if (sources != null) {
			return sources;
		} else {
			return EMPTY_SET;
		}
	}
	
	
	/**
	 * List up source vertices which have data-flow paths to 
	 * the specified operand.
	 * @param instruction specifies a destination instruction.
	 * @param operand specifies a destination operand.
	 * @return a list of 
	 */
	public int[] getReachableSources(int instruction, int operand) { 
		TIntHashSet sources = new TIntHashSet();
		TIntHashSet visited = new TIntHashSet();
		
		final IntStack worklist = new IntStack();
		// Initialize worklist with source vertices for the operand; 
		// the instruction vertex itself is traversed if there is a data-flow loop.  
		TIntIterator init = getSources(instruction, operand).iterator();
		while (init.hasNext()) {
			worklist.push(init.next());
		}
		
		while (!worklist.isEmpty()) {
			int v = worklist.pop();
			if (!visited.contains(v)) {
				visited.add(v);
				if (method.isSource(v)) {
					sources.add(v);
					continue;
				}
				
				TIntObjectIterator<TIntHashSet> it = getOperands(v).iterator();
				while (it.hasNext()) {
					it.advance();
					TIntHashSet instructions = it.value();
					TIntIterator sourceIterator = instructions.iterator();
					while (sourceIterator.hasNext()) {
						int next = sourceIterator.next();
						if (!visited.contains(next)) {
							worklist.push(next);
						}
					}
				}
			}
		}
		
		return sources.toArray();
	}
	
	
}
