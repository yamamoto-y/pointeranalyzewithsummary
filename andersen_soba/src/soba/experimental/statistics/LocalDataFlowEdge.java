package soba.experimental.statistics;


/**
 * This class is a data structure for LocalDataFlowGraph.
 * @author ishio
 *
 */
public class LocalDataFlowEdge {
		
	public static final int PARAM_VERTEX_INDEX = -1;

	private int defVertex;
	private int useVertex;
	private int variableId;

	public LocalDataFlowEdge(int def, int use, int variableId) {
		assert variableId >= 0;
		this.defVertex = def;
		this.useVertex = use;
		this.variableId = variableId;
	}
	
	public int getDefVertex() {
		return defVertex;
	}
	
	public int getUseVertex() {
		return useVertex;
	}
	
	public int getVariableId() {
		return variableId;
	}
	
}
