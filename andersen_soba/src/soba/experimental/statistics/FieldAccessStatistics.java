package soba.experimental.statistics;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;

import soba.bytecode.MethodInfo;
import soba.model.DynamicBindingResolver;
import soba.util.IntPairList;
import soba.util.graph.DepthFirstSearch;
import soba.util.graph.IDirectedGraph;



public class FieldAccessStatistics {
	
	private HashMap<String, FieldCounter> counters; // Statistics for each field 
	private DynamicBindingResolver resolver;

	public FieldAccessStatistics(DynamicBindingResolver resolver) {
		this.counters = new HashMap<String, FieldAccessStatistics.FieldCounter>();
		this.resolver = resolver;
	}
	

	public List<Count> processMethod(MethodDataFlow mdf) {
		assert mdf.getMethodInfo().hasMethodBody();
		
		FieldDataFlow fieldDF = new FieldDataFlow();
		MethodInfo m = mdf.getMethodInfo();
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		int instructionCount = instructions.size();
		IDirectedGraph cfg = m.getMethodBody().getDataFlow().getConservativeControlFlow();
		for (int i=0; i<instructionCount; ++i) {
			if (instructions.get(i).getType() == AbstractInsnNode.FIELD_INSN) {
				FieldInsnNode f = (FieldInsnNode)instructions.get(i);
				boolean isStatic = (f.getOpcode() == Opcodes.PUTSTATIC || f.getOpcode() == Opcodes.GETSTATIC);
				String className;
				if (isStatic) {
					className = resolver.resolveStaticFieldOwner(f.owner, f.name, f.desc);
				} else {
					className = resolver.resolveInstanceFieldOwner(f.owner, f.name, f.desc);
				}
				if (className == null) className = f.owner;
				String fieldNodeString = getFieldString(className, f.name, f.desc);
				
				switch (f.getOpcode()) {
				case Opcodes.PUTFIELD:
				case Opcodes.PUTSTATIC:
					// Search def-use for each store operation.
					if (!counters.containsKey(fieldNodeString)) {
						FieldCounter c = new FieldCounter(className, f.name, f.desc, isStatic);
						counters.put(fieldNodeString, c);
					}
					counters.get(fieldNodeString).write(m);
					fieldDF.incrementWrite(className, f.name, f.desc, isStatic);
					DepthFirstSearch.search(cfg, i, new FieldDefUseAnalysis(className, f.name, f.desc, isStatic, resolver, instructions, fieldDF.getEdges(className, f.name, f.desc)));
					break;
				case Opcodes.GETFIELD:
				case Opcodes.GETSTATIC:
					if (!counters.containsKey(fieldNodeString)) {
						FieldCounter c = new FieldCounter(className, f.name, f.desc, isStatic);
						counters.put(fieldNodeString, c);
					}
					counters.get(fieldNodeString).read(m);
					fieldDF.incrementRead(className, f.name, f.desc, isStatic);
					break;
				default:
					assert false: "Unknown Field Operation Found.";
				}
			}
		}
		return fieldDF.getFieldAccessList();
	}

	private static String getFieldString(String owner, String name, String desc) {
		return owner + "#" + name + "#" + desc;
	}
	
	public Iterable<FieldCounter> counters() {
		return counters.values();
	}
	
	public class FieldDataFlow {
		
		private List<Count> fields;
		private HashMap<String, IntPairList> edges;
		private HashMap<String, Count> counters;
		
		public FieldDataFlow() {
			fields = new ArrayList<FieldAccessStatistics.Count>();
			edges = new HashMap<String, IntPairList>();
			counters = new HashMap<String, FieldAccessStatistics.Count>();
		}
		
		public List<Count> getFieldAccessList() {
			return fields;
		}
		
		public IntPairList getEdges(String className, String fieldName, String fieldDesc) {
			String fieldString = getFieldString(className, fieldName, fieldDesc);
			if (edges.containsKey(fieldString)) {
				return edges.get(fieldString);
			} else {
				IntPairList e = new IntPairList();
				counters.get(fieldString).edges = e;
				edges.put(fieldString, e);
				return e;
			}
		}
		
		public void incrementRead(String className, String fieldName, String fieldDesc, boolean isStatic) {
			String fieldString = getFieldString(className, fieldName, fieldDesc);
			if (counters.containsKey(fieldString)) {
				counters.get(fieldString).read++;
			} else {
				Count c = new Count(className, fieldName, fieldDesc, isStatic);
				fields.add(c);
				c.read = 1;
				counters.put(fieldString, c);
			}
		}
		
		public void incrementWrite(String className, String fieldName, String fieldDesc, boolean isStatic) {
			String fieldString = getFieldString(className, fieldName, fieldDesc);
			if (counters.containsKey(fieldString)) {
				counters.get(fieldString).write++;
			} else {
				Count c = new Count(className, fieldName, fieldDesc, isStatic);
				fields.add(c);
				c.write = 1;
				counters.put(fieldString, c);
			}
		}
		
	}
	
	public static class Count {

		private String owner;
		private String name;
		private String desc;
		private int read;
		private int write;
		private boolean isStatic;
		private IntPairList edges;
		
		public Count(String className, String fieldName, String fieldDesc, boolean isStatic) {
			this.owner = className;
			this.name = fieldName;
			this.desc = fieldDesc;
			this.read = 0;
			this.write = 0;
			this.isStatic = isStatic;
			this.edges = null;
		}
		
		public int getRead() {
			return read;
		}
		
		public int getWrite() {
			return write;
		}
		
		public boolean isStatic() {
			return isStatic;
		}
		
		public int getIntraProceduralFlow() {
			if (edges == null) return 0;
			else return edges.size();
		}
		
		public String getFieldOwner() {
			return owner;
		}
		
		public String getFieldName() {
			return name;
		}
		
		public String getFieldDesc() {
			return desc;
		}
	}
	
	public static class FieldCounter {

		private String owner;
		private String name;
		private String desc;
		private boolean isStatic;
		private HashSet<String> writers;
		private HashSet<String> readers;
		private HashSet<String> writerClasses;
		private HashSet<String> readerClasses;
		private int readInstruction;
		private int writeInstruction;
		
		public FieldCounter(String owner, String name, String desc, boolean isStatic) {
			this.name = name;
			this.owner = owner;
			this.desc = desc;
			this.isStatic = isStatic;
			writers = new HashSet<String>(5);
			readers = new HashSet<String>(5);
			writerClasses = new HashSet<String>(5);
			readerClasses = new HashSet<String>(5);
		}
		
		public void merge(FieldCounter another) {
			//assert exclusiveSet(writers, another.writers);
			//assert exclusiveSet(readers, another.readers); // �����͐��藧���Ȃ��D�N���XC�̃t�B�[���hf�ƁC�N���XD�̃t�B�[���hf���C����Ƃ������e�N���X�̃t�B�[���h�ɂȂ肤��D
			writers.addAll(another.writers);
			readers.addAll(another.readers);
			writerClasses.addAll(another.writerClasses);
			readerClasses.addAll(another.readerClasses);
			writeInstruction += another.writeInstruction;
			readInstruction += another.readInstruction;
		}
		
		private int countInternal(Set<String> methods, String owner) {
			int count = 0;
			for (String m: methods) {
				if (m.startsWith(owner + "#")) {
					count++;
				}
			}
			return count;
		}
		
		public void write(MethodInfo method) {
			writerClasses.add(method.getClassName());
			writers.add(method.getMethodKey());
//			String key = method.getMethodKey();
//			if (!writers.contains(key)) {
//				writers.add(key);
//				writeMethod++;
//				if (owner.equals(method.getClassName())) {
//					writeInternalMethod++;
//				}
//			}
			writeInstruction++;
		}
		
		public void read(MethodInfo method) {
			readerClasses.add(method.getClassName());
			readers.add(method.getMethodKey());
//			String key = method.getMethodKey();
//			if (!readers.contains(key)) {
//				readers.add(key);
//				readMethod++;
//				if (owner.equals(method.getClassName())) {
//					readInternalMethod++;
//				}
//			}
			readInstruction++;
		}

		public String getOwner() {
			return owner;
		}

		public String getName() {
			return name;
		}

		public String getDesc() {
			return desc;
		}

		public boolean isStatic() {
			return isStatic;
		}

		public int getReadMethod() {
			return readers.size();
		}

		public int getWriteMethod() {
			return writers.size();
		}

		public int getReadClass() {
			return readerClasses.size();
		}

		public int getWriteClass() {
			return writerClasses.size();
		}

		public int getReadInternalMethod() {
			return countInternal(readers, this.owner);
		}

		public int getWriteInternalMethod() {
			return countInternal(writers, this.owner);
		}

		public int getReadInstruction() {
			return readInstruction;
		}

		public int getWriteInstruction() {
			return writeInstruction;
		}
		
	}

}
