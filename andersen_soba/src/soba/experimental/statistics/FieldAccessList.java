package soba.experimental.statistics;


import gnu.trove.map.hash.TObjectIntHashMap;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;

import soba.bytecode.MethodInfo;



public class FieldAccessList {
	
	private TObjectIntHashMap<String> readers;
	private TObjectIntHashMap<String> staticReaders;
	private TObjectIntHashMap<String> writers;
	private TObjectIntHashMap<String> staticWriters;

	public FieldAccessList(MethodInfo m) {
		readers = new TObjectIntHashMap<String>();
		staticReaders = new TObjectIntHashMap<String>();
		writers = new TObjectIntHashMap<String>();
		staticWriters = new TObjectIntHashMap<String>();
		
		// TODO 本当は，ここで Field の参照先を解決し，フィールド別の集計をしなくてはならない．
//		List<FieldAccess> access = m.getMethodBody().listFieldAccesses();
//		for (FieldAccess ac: access) {
//			Field
//		}
		
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		int instructionCount = instructions.size();
		for (int i=0; i<instructionCount; ++i) {
			if (instructions.get(i).getType() == AbstractInsnNode.FIELD_INSN) {
				FieldInsnNode f = (FieldInsnNode)instructions.get(i);
				String fieldNodeString = getFieldString(f);
				
				switch (f.getOpcode()) {
				case Opcodes.PUTFIELD:
					if (writers.containsKey(fieldNodeString)) {
						writers.increment(fieldNodeString);
					} else {
						writers.put(fieldNodeString, 1);
					}
					break;
				case Opcodes.PUTSTATIC:
					if (staticWriters.containsKey(fieldNodeString)) {
						staticWriters.increment(fieldNodeString);
					} else {
						staticWriters.put(fieldNodeString, 1);
					}
					break;
				case Opcodes.GETFIELD:
					if (readers.containsKey(fieldNodeString)) {
						readers.increment(fieldNodeString);
					} else {
						readers.put(fieldNodeString, 1);
					}
					break;
				case Opcodes.GETSTATIC:
					if (staticReaders.containsKey(fieldNodeString)) {
						staticReaders.increment(fieldNodeString);
					} else {
						staticReaders.put(fieldNodeString, 1);
					}
					break;
				default:
					assert false: "Unknown Field Operation Found.";
				}
			}
		}
	}

	public static String getFieldString(FieldInsnNode fieldNode) {
		return fieldNode.owner + "#" + fieldNode.name + "#" + fieldNode.desc;
	}
	
	public int getReadInstructionCount() {
		return sum(readers.values());
	}
	
	public int getWriteInstructionCount() {
		return sum(writers.values());
	}

	public int getStaticReadInstructionCount() {
		return sum(staticReaders.values());
	}
	
	public int getStaticWriteInstructionCount() {
		return sum(staticWriters.values());
	}

	public int getReadFieldCount() {
		return readers.size();
	}

	public int getWriteFieldCount() {
		return writers.size();
	}

	public int getStaticReadFieldCount() {
		return staticReaders.size();
	}

	public int getStaticWriteFieldCount() {
		return staticWriters.size();
	}

	public int getMaxReadCount() {
		return max(readers.values());
	}
	
	public int getMaxWriteCount() {
		return max(writers.values());
	}

	public int getMaxStaticReadCount() {
		return max(staticReaders.values());
	}
	
	public int getMaxStaticWriteCount() {
		return max(staticWriters.values());
	}
	
	

	private int sum(int[] values) {
		if (values.length == 0) {
			return 0;
		} else {
			int sum = 0;
			for (int i=0; i<values.length; ++i) {
				sum += values[i];
			}
			return sum;
		}
	}
	
	private int max(int[] values) {
		if (values.length == 0) {
			return 0;
		} else {
			int max = values[0];
			for (int i=1; i<values.length; ++i) {
				if (max < values[i]) max = values[i];
			}
			return max;
		}
	}
	

}
