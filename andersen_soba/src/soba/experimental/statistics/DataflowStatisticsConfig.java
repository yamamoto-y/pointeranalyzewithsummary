package soba.experimental.statistics;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import soba.util.options.BooleanOption;
import soba.util.options.Configuration;
import soba.util.options.FileOption;

public class DataflowStatisticsConfig {
	
	private FileOption outputDir = new FileOption("-o", new File("."));
	private FileOption jdkDir = new FileOption("-jdk", null);
	private BooleanOption outputGraph = new BooleanOption("-dump", false);
	private FileOption projectsContainerDir = new FileOption("-d", null);
	private List<String> files;
	
	public DataflowStatisticsConfig(String[] args) {
		Configuration config = new Configuration();
		config.defineOptions(outputDir, outputGraph, projectsContainerDir, jdkDir);
		
		config.parseArgs(args);
		
		files = new ArrayList<String>();
		files.addAll(config.getExtraOptions());
		if (projectsContainerDir.hasValue() &&
			projectsContainerDir.getValue() != null) {
			File[] dirs = projectsContainerDir.getValue().listFiles(new FileFilter() {
				@Override
				public boolean accept(File f) {
					if (f.getName().equals(".") || f.getName().equals("..")) {
						return false;
					}
					return f.isDirectory() && f.canRead();
				}
			});
			for (File d: dirs) {
				files.add(d.getAbsolutePath());
			}
		}
	}
	
	public List<String> getFiles() {
		return files;
	}
	
	public File getOutputDir() {
		return outputDir.getValue();
	}
	
	public File getJdkDir() {
		return jdkDir.getValue();
	}
	
	public boolean getOutputGraph() {
		return outputGraph.getValue();
	}
	
}
