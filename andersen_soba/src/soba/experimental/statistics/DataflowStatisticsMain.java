package soba.experimental.statistics;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.bytecode.MethodInfo;
import soba.bytecode.method.DataFlowEdge;
import soba.bytecode.method.DataFlowInfo;
import soba.bytecode.method.ILocalVariableInfo;
import soba.bytecode.method.LocalVariableTable;
import soba.model.DynamicBindingResolver;
import soba.util.files.Directory;
import soba.util.files.IFileEnumeraterCallback;
import soba.util.files.IFileEnumerator;
import soba.util.files.ZipFile;
import soba.util.graph.IDirectedGraph;
import soba.util.output.CSVPrinter;
import soba.util.output.CSVPrinter.ColumnType;



public class DataflowStatisticsMain implements IFileEnumeraterCallback {

	private static final String[] CATEGORY_KEYS = { "jp/ac/", "com/nec/", "java/", "javax/", "com/sun/", "sun/", "netscape/" };
	private static final String[] CATEGORY_NAMES = { "Enterprise", "Enterprise", "JDK", "JDK", "JDK", "JDK", "JDK" };
	private static final String CATEGORY_NAME_DEFAULT = "OSS";
	
	private int count = 0;

	private long startTime; 
	private File outputDir;
	private boolean outputGraph;
	
	private HashSet<String> processedClassNames;
	private HashMap<String, String> processedClass;
	private HashSet<String> processedClassNamesInProject;
	private HashMap<String, String> processedClassInProject;
	private PrefixCategorizer categorizer;
	private FieldAccessStatistics fieldInProject;

	private PrintStream progressReport;
	

	private CSVPrinter projectTable;
	private CSVPrinter classTable;
	private CSVPrinter methodTable;
	private CSVPrinter fieldTable;
	private CSVPrinter fieldAccessTable;
	private CSVPrinter variableTable;
	private CSVPrinter defTable;
	private CSVPrinter useTable;
	private CSVPrinter pathTable;

	
	public static void main(String[] args) {
		DataflowStatisticsConfig config = new DataflowStatisticsConfig(args);
		File outputDir = config.getOutputDir();
		if (!outputDir.exists()) {
			boolean success = outputDir.mkdirs();
			if (!success) {
				System.err.println("Error: cannot create an output directory: " + outputDir.getAbsolutePath());
				return;
			}
		}
		if (!outputDir.canWrite()) {
			System.err.println("Error: output directory (" + outputDir.getAbsolutePath() + ") is not writable.");
			return;
		}
		try {
			DataflowStatisticsMain c = new DataflowStatisticsMain(outputDir, config.getOutputGraph());
			for (String arg: config.getFiles()) {
				File f = new File(arg);
				IFileEnumerator dir = null;
				if (f.isDirectory()) {
					Directory d = new Directory(f);
					d.enableZipSearch();
					d.enableRecursiveZipSearch();
					dir = d;
				} else if (ZipFile.isZipFile(f)) {
					ZipFile z = new ZipFile(f);
					z.enableRecursiveSearch();
					dir = z;
				}
				if (dir != null) {
					// Add jdk classes to FieldResolver, but not to ClassAssignmentPrinter
					File jdkDir = config.getJdkDir(); 
					IFileEnumerator jdkEnum = null;
					if (jdkDir != null) {
						if (jdkDir.isDirectory()) {
							Directory d = new Directory(jdkDir);
							d.enableRecursiveZipSearch();
							jdkEnum = d;
						} else if (ZipFile.isZipFile(jdkDir)) {
							ZipFile z = new ZipFile(jdkDir);
							z.enableRecursiveSearch();
							jdkEnum = z;
						}
					}
					
					JavaProgram program = new JavaProgram(new IFileEnumerator[] { dir, jdkEnum });
					DynamicBindingResolver resolver = new DynamicBindingResolver(program.getClassHierarchy());

					c.beginProject(f.getAbsolutePath(), resolver);

					// Provide files to ClassAssignmentPrinter
					// Use "dir" instead of a list of files in JavaProgram
					//  because a directory may include duplicated classes.
					dir.process(c);
					
					c.endProject(f.getAbsolutePath(), program);
					
				}
			}
			c.close();
		} catch (IOException e) { 
			System.err.println("Cannot open a file to store the result.");
			e.printStackTrace();
		}
	}


	public DataflowStatisticsMain(File outputDir, boolean outputGraph) throws IOException {
		this.startTime = System.currentTimeMillis();
		this.processedClassNames = new HashSet<String>();
		this.processedClassNamesInProject = new HashSet<String>();
		this.processedClass = new HashMap<String, String>();
		this.processedClassInProject = new HashMap<String, String>();
		this.outputDir = outputDir;
		this.outputGraph = outputGraph;

		this.categorizer = new PrefixCategorizer(CATEGORY_KEYS, CATEGORY_NAMES, CATEGORY_NAME_DEFAULT);

		progressReport = openFileOrStdout(outputDir, "messages.txt");
		
		projectTable = new CSVPrinter(openFileOrStdout(outputDir, "ProjectTable.txt"), new String[]{ "ProjectKey",  "ProjectName" });

		fieldTable = new CSVPrinter(openFileOrStdout(outputDir, "FieldTable.txt"), new String[] { "FieldKey",  "ProjectKey", "OwnerName", "FieldName", "FieldDescriptor", "Static", "ReadClassCount", "WriteClassCount", "ReadMethodCount", "WriteMethodCount", "ReadInternalMethodCount", "WriteInternalMethodCount", "ReadInstructionCount", "WriteInstructionCount" });
		fieldTable.setColumnType(0, ColumnType.Integer);
		fieldTable.setColumnType(1, ColumnType.Integer);
		fieldTable.setColumnType(5, ColumnType.Boolean);
		for (int c=6; c<=13; ++c) fieldTable.setColumnType(c, ColumnType.Integer);
		
		fieldAccessTable = new CSVPrinter(openFileOrStdout(outputDir, "FieldAccessTable.txt"), new String[] { "FieldAccessKey", "ProjectKey", "ClassKey", "MethodKey", "OwnerName", "FieldName", "FieldDescriptor", "Static", "ReadInstruction", "WriteInstruction", "IntraproceduralFlow", "DuplicatedWhole", "DuplicatedProject" });

		classTable = new CSVPrinter(openFileOrStdout(outputDir, "ClassTable.txt"), new String[]{ "ClassKey",  "ProjectKey", "FileName", "ClassName", "PackageName", "ClassCategory", "DuplicatedWhole", "DuplicatedProject" });
		
		methodTable = new CSVPrinter(openFileOrStdout(outputDir, "MethodTable.txt"), new String[]{ "MethodKey",  "ProjectKey", "ClassKey", "MethodName", "MethodDescriptor", "MethodSignature", "VariableCount", "NamedVariableCount", "ParamCount", "ReadField", "WriteField", "StaticReadField", "StaticWriteField", "ReadInstruction", "WriteInstruction", "StaticReadInstruction", "StaticWriteInstruction", "MaxReadField", "MaxWriteField", "MaxStaticReadField", "MaxStaticWriteField", "ArrayDefUse", "DuplicatedWhole", "DuplicatedProject" });
		
		pathTable = new CSVPrinter(openFileOrStdout(outputDir, "PathTable.txt"), new String[] { "OperandKey", "MethodKey", "ProjectKey", "ClassKey", "InstructionIndex", "InstructionOperand", "PathCount", "FlowInsensitivePathCount", "PathCountR", "FlowInsensitivePathCountR", "OperationType", "Category", "CategoryR", "DuplicatedWhole", "DuplicatedProject" });

		variableTable = new CSVPrinter(openFileOrStdout(outputDir, "VariableTable.txt"), new String[] { "VariableKey", "ProjectKey", "ClassKey", "MethodKey", "VariableName", "VariableType", "VarIndex", "VariableKind", "DataFlowKind", "ClassCategory", "DuplicatedWhole", "DuplicatedProject" , "Def", "Use", "PossiblePaths", "ActualSimplePath", "InfeasibleForSimple", "ActualConservativePaths", "InfeasibleForConservative", "GraphDiff"});
		
		defTable = new CSVPrinter(openFileOrStdout(outputDir, "DefTable.txt"), new String[] { "DefKey", "ProjectKey", "VariableKey", "ClassCategory", "VariableType", "VariableKind", "DefInstruction", "SimpleUseCount", "ConservativeUseCount", "GraphDiff", "DuplicatedWhole", "DuplicatedProject"  });
		useTable = new CSVPrinter(openFileOrStdout(outputDir, "UseTable.txt"), new String[] { "UseKey", "ProjectKey", "VariableKey", "ClassCategory", "VariableType", "VariableKind", "UseInstruction", "SimpleDefCount", "ConservativeDefCount", "GraphDiff", "DuplicatedWhole", "DuplicatedProject"  });
		
	}
	
	private PrintStream openFileOrStdout(File outputDir, String filename) throws IOException {
		File f = new File(outputDir, filename);
		return new PrintStream(new FileOutputStream(f));
	}
	
	
	public void close() {
		progressReport.println("Processed " + Integer.toString(getProcessedClassCount()) + " classes: " + Long.toString(System.currentTimeMillis() - startTime));
		this.progressReport.close();
		this.projectTable.close();
		this.fieldTable.close();
		this.classTable.close();
		this.methodTable.close();
		this.pathTable.close();
		this.variableTable.close();
		this.defTable.close();
		this.useTable.close();
	}
	
	public void beginProject(String filename, DynamicBindingResolver resolver) {
		progressReport.println("Start: " + filename);
		
		projectTable.printLineId();
		projectTable.print(filename);
		projectTable.println();
		processedClassInProject.clear();
		processedClassNamesInProject.clear();
		fieldInProject = new FieldAccessStatistics(resolver);
	}
	
	public void endProject(String filename, JavaProgram program) {
		// Close field table
		for (FieldAccessStatistics.FieldCounter f: fieldInProject.counters()) {
			fieldTable.printLineId();
			fieldTable.print(projectTable.getCurrentLine());
			fieldTable.print(f.getOwner());
			fieldTable.print(f.getName());
			fieldTable.print(f.getDesc());
			fieldTable.print(f.isStatic());
			fieldTable.print(f.getReadClass());
			fieldTable.print(f.getWriteClass());
			fieldTable.print(f.getReadMethod());
			fieldTable.print(f.getWriteMethod());
			fieldTable.print(f.getReadInternalMethod());
			fieldTable.print(f.getWriteInternalMethod());
			fieldTable.print(f.getReadInstruction());
			fieldTable.print(f.getWriteInstruction());
			fieldTable.println();
		}
		
		// Report not-found classes
		Set<String> notFound = program.getClassHierarchy().getRequestedClasses();
		if (notFound.size() > 0) {
			progressReport.println("[Not Found] Classes that are referred by the analysis but not found:");
			for (String className: notFound) {
				progressReport.print("[Not Found]  ");
				progressReport.println(className);
			}
			progressReport.println("[Not Found] -- End of the class list");
		}
		progressReport.println("End: " + filename);
	}
	
	@Override
	public void process(String name, InputStream stream) throws IOException {
		try {
			ClassInfo c = new ClassInfo(name, stream);
			boolean duplicated = false;
			boolean duplicatedInProject = false;
			if (c.getHash() != null) {
				// Find duplicated classes in the whole target classes
				if (c.getClassName().equals(processedClass.get(c.getHash()))) {
					duplicated = true;
				} else {
					if (processedClassNames.contains(c.getClassName())) {
						progressReport.println("Same class name but different hash: " + c.getClassName() + " in " + c.getFileName());
					}
					processedClassNames.add(c.getClassName());
				}
				processedClass.put(c.getHash(), c.getClassName());
				// Find duplicated classes in the target project
				if (c.getClassName().equals(processedClassInProject.get(c.getHash()))) {
					duplicatedInProject = true;
				}
				processedClassInProject.put(c.getHash(), c.getClassName());
				processedClassNamesInProject.add(c.getClassName());
			}
	
			outputClass(name, c.getClassName(), c.getPackageName(), duplicated, duplicatedInProject);

			for (int m=0; m<c.getMethodCount(); ++m) {
				MethodInfo methodInfo = c.getMethod(m);
				if (methodInfo.hasMethodBody()) {
					//System.err.println(methodInfo.toLongString());
					
					MethodDataFlow method = new MethodDataFlow(c, methodInfo.getMethodBody().getMethodNode());
					if (outputGraph) outputGraph(c, method, outputDir, progressReport);
					outputMethodName(c, method, duplicated, duplicatedInProject);
					DataFlowInfo dataflow = method.getMethodInfo().getMethodBody().getDataFlow();
					MethodNode methodNode = method.getMethodInfo().getMethodBody().getMethodNode();
					TransitiveDataflowAnalysis analysis = new TransitiveDataflowAnalysis(dataflow, methodNode);
					outputPathTable(analysis, duplicated, duplicatedInProject);
					for (int v=0; v<method.getVariableCount(); ++v) {
						outputVariable(c, method, v, duplicated, duplicatedInProject);
						outputUsePerDef(c, method, v, duplicated, duplicatedInProject);
						outputDefPerUse(c, method, v, duplicated, duplicatedInProject);
					}
					List<FieldAccessStatistics.Count> fields = fieldInProject.processMethod(method);
					for (int i=0; i<fields.size(); ++i) {
						outputFieldAccess(c, method, fields.get(i), duplicated, duplicatedInProject);
					}
				}
			}
			
			count++;
		} catch (IOException e) {
			progressReport.println("Cannot process a class: " + name);
			progressReport.println("  IOException: " + e.getLocalizedMessage());
		}
	}
	
	@Override
	public boolean isTarget(String name) {
		return name.endsWith(".class");
	}
	
	@Override
	public boolean reportError(String name, Exception e) {
		progressReport.println("Cannot process a class: " + name);
		progressReport.println("  IOException: " + e.getLocalizedMessage());
		return false;
	}

//	@Override
//	public void processLibraryClass(String filename, InputStream binaryStream) {
//		try {
//			ClassInfo c = new ClassInfo(filename, binaryStream);
//			if (processedClassNamesInProject.contains(c.getClassName())) {
//				return;
//			}
//			processedClassNamesInProject.add(c.getClassName());
//			fieldInProject.registerFields(c);
//			count++;
//		} catch (IOException e) {
//			progressReport.println("Cannot process a class: " + filename);
//			progressReport.println("  IOException: " + e.getLocalizedMessage());
//		}
//	}

	private String getDuplicatedFlag(boolean flag) {
		return flag ? "D": "U";
	}
	
	private void outputClass(String fileName, String className, String packageName, boolean duplicated, boolean duplicatedInProject) {
		classTable.printLineId();
		classTable.print(projectTable.getCurrentLine());
		classTable.print(fileName);
		classTable.print(className);
		classTable.print(packageName);
		classTable.print(categorizer.getCategory(className));
		classTable.print(getDuplicatedFlag(duplicated));
		classTable.print(getDuplicatedFlag(duplicatedInProject));
		classTable.println();
	}
	
	
	private void outputMethodName(ClassInfo c, MethodDataFlow method, boolean duplicated, boolean duplicatedInProject) {
		methodTable.printLineId();
		// general
		methodTable.print(projectTable.getCurrentLine());
		methodTable.print(classTable.getCurrentLine());
		methodTable.print(method.getMethodName());
		methodTable.print(method.getMethodDesc());
		methodTable.print(method.getMethodSignature());

		// local variaables
		methodTable.print(method.getVariableCount());
		methodTable.print(method.getNamedVariableCount());
		methodTable.print(method.getParamCount());
		
		// fields
		methodTable.print(method.getFieldAccess().getReadFieldCount());
		methodTable.print(method.getFieldAccess().getWriteFieldCount());
		methodTable.print(method.getFieldAccess().getStaticReadFieldCount());
		methodTable.print(method.getFieldAccess().getStaticWriteFieldCount());
		methodTable.print(method.getFieldAccess().getReadInstructionCount());
		methodTable.print(method.getFieldAccess().getWriteInstructionCount());
		methodTable.print(method.getFieldAccess().getStaticReadInstructionCount());
		methodTable.print(method.getFieldAccess().getStaticWriteInstructionCount());
		methodTable.print(method.getFieldAccess().getMaxReadCount());
		methodTable.print(method.getFieldAccess().getMaxWriteCount());
		methodTable.print(method.getFieldAccess().getMaxStaticReadCount());
		methodTable.print(method.getFieldAccess().getMaxStaticWriteCount());
		
		// arrays
		methodTable.print(method.getArrayDefUseCount());
		
		// duplication
		methodTable.print(getDuplicatedFlag(duplicated));
		methodTable.print(getDuplicatedFlag(duplicatedInProject));
		methodTable.println();
	}
	
	private void outputPathTable(TransitiveDataflowAnalysis analysis, boolean duplicated, boolean duplicatedInProject) {
		for (int i=0; i<analysis.getOperandCount(); ++i) {
			pathTable.printLineId(); // OperandKey
			// general
			pathTable.print(methodTable.getCurrentLine()); // MethodKey
			pathTable.print(projectTable.getCurrentLine());  // ProjectKey
			pathTable.print(classTable.getCurrentLine()); // ClassKey
			pathTable.print(analysis.getInstructionIndex(i));
			pathTable.print(analysis.getInstructionOperand(i));
			pathTable.print(analysis.getPathCount(i));
			pathTable.print(analysis.getFlowInsensitivePathCount(i));
			pathTable.print(analysis.getReducedPathCount(i));
			pathTable.print(analysis.getReducedFlowInsensitivePathCount(i));
			//pathTable.print(analysis.getOpcode(i));
			pathTable.print(analysis.getOperationType(i));
			pathTable.print(analysis.getPathCategory(i));
			pathTable.print(analysis.getReducedPathCategory(i));
			
			pathTable.print(getDuplicatedFlag(duplicated));
			pathTable.print(getDuplicatedFlag(duplicatedInProject));
			pathTable.println();
		}
	}
	
	public int getProcessedClassCount() {
		return count;
	}

	public String getClassDirPath(ClassInfo c) {
		return c.getClassName().replace('/', File.separatorChar); 
	}

	/**
	 * Dump the contents of a method. 
	 * @param c
	 * @param m
	 * @param outputDir
	 * @param progressReport
	 */
	public void outputGraph(ClassInfo c, MethodDataFlow m, File outputDir, PrintStream progressReport) {
		File classNameDir = new File(outputDir, getClassDirPath(c));
		boolean created = (classNameDir.exists() && classNameDir.isDirectory()) || classNameDir.mkdirs();
		if (created) {
        	File textOutput = new File(classNameDir, m.getMethodFileName() + ".txt");
        	boolean success = saveAsText(textOutput, c, m);
        	if (!success) {
        		progressReport.println("Failed to write a file: " + textOutput.getAbsolutePath());
        	}

        	File output = new File(classNameDir, m.getMethodFileName() + ".dot.txt");
        	success = saveAsDOT(output, c, m, m.getMethodInfo()); 
        	if (!success) {
        		progressReport.println("Failed to write a file: " + output.getAbsolutePath());
        	}
		} else {
			progressReport.println("Failed to create: " + classNameDir.getAbsolutePath());
		}
		
	}
	
	public void outputVariable(ClassInfo c, MethodDataFlow m, int variableIndex, boolean duplicated, boolean duplicatedInProject) {
		variableTable.printLineId();
		variableTable.print(projectTable.getCurrentLine());
		variableTable.print(classTable.getCurrentLine());  // A redundant column for analyzability  
		variableTable.print(methodTable.getCurrentLine());
		variableTable.print(m.getVariableName(variableIndex));
		variableTable.print(m.getVariableType(variableIndex));
		variableTable.print(m.getBytecodeVariableIndex(variableIndex));
		variableTable.print(m.getVariableKind(variableIndex));
		variableTable.print(m.getDataFlowKind(variableIndex));
		variableTable.print(categorizer.getCategory(c.getClassName()));
		variableTable.print(getDuplicatedFlag(duplicated));
		variableTable.print(getDuplicatedFlag(duplicatedInProject));
		int def = m.getDefCount(variableIndex);
		int use = m.getUseCount(variableIndex);
		int combination = def * use;
		int simplePath = m.getSimpleDefUsePathCount(variableIndex);
		int conservativePath = m.getConservativeDefUsePathCount(variableIndex);
		variableTable.print(def);
		variableTable.print(use);
		variableTable.print(combination);
		variableTable.print(simplePath);
		variableTable.print(combination - simplePath);
		variableTable.print(conservativePath);
		variableTable.print(combination - conservativePath);
		variableTable.print(conservativePath - simplePath);
		variableTable.println();
	}
	
	public void outputFieldAccess(ClassInfo c, MethodDataFlow m, FieldAccessStatistics.Count count, boolean duplicated, boolean duplicatedInProject) {
		fieldAccessTable.printLineId();
		fieldAccessTable.print(projectTable.getCurrentLine());
		fieldAccessTable.print(classTable.getCurrentLine());  // A redundant column for analyzability  
		fieldAccessTable.print(methodTable.getCurrentLine());
		fieldAccessTable.print(count.getFieldOwner());
		fieldAccessTable.print(count.getFieldName());
		fieldAccessTable.print(count.getFieldDesc());
		fieldAccessTable.print(count.isStatic());
		fieldAccessTable.print(count.getRead());
		fieldAccessTable.print(count.getWrite());
		fieldAccessTable.print(count.getIntraProceduralFlow());
		fieldAccessTable.print(getDuplicatedFlag(duplicated));
		fieldAccessTable.print(getDuplicatedFlag(duplicatedInProject));
		fieldAccessTable.println();
	}
	
	public void outputUsePerDef(ClassInfo c, MethodDataFlow m, int variableId, boolean duplicated, boolean duplicatedInProject) {
		for (int i=0; i<m.getMethodInfo().getInstructionCount(); ++i) {
			int simpleUseCount = m.getSimpleUseCount(variableId, i); 
			int conservativeUseCount = m.getConservativeUseCount(variableId, i);
			if ((simpleUseCount > 0) || (conservativeUseCount > 0)) {
				defTable.printLineId();
				defTable.print(projectTable.getCurrentLine());
				defTable.print(variableTable.getCurrentLine());
				defTable.print(categorizer.getCategory(c.getClassName()));
				defTable.print(m.getVariableType(variableId));
				defTable.print(m.getVariableKind(variableId));
				defTable.print(i);
				defTable.print(simpleUseCount);
				defTable.print(conservativeUseCount);
				defTable.print(conservativeUseCount - simpleUseCount);
				defTable.print(getDuplicatedFlag(duplicated));
				defTable.print(getDuplicatedFlag(duplicatedInProject));
				defTable.println();
			}
		}
	}

	public void outputDefPerUse(ClassInfo c, MethodDataFlow m, int variableId, boolean duplicated, boolean duplicatedInProject) {
		for (int i=0; i<m.getMethodInfo().getInstructionCount(); ++i) {
			int simpleDefCount = m.getSimpleDefCount(variableId, i); 
			int conservativeDefCount = m.getConservativeDefCount(variableId, i);
			if ((simpleDefCount > 0) || (conservativeDefCount > 0)) {
				useTable.printLineId();
				useTable.print(projectTable.getCurrentLine());
				useTable.print(variableTable.getCurrentLine());
				useTable.print(categorizer.getCategory(c.getClassName()));
				useTable.print(m.getVariableType(variableId));
				useTable.print(m.getVariableKind(variableId));
				useTable.print(i);
				useTable.print(simpleDefCount);
				useTable.print(conservativeDefCount);
				useTable.print(conservativeDefCount - simpleDefCount);
				useTable.print(getDuplicatedFlag(duplicated));
				useTable.print(getDuplicatedFlag(duplicatedInProject));
				useTable.println();
			}
		}
	}

	public boolean saveAsDOT(File outputFile, ClassInfo c, MethodDataFlow m, final MethodInfo methodInfo) {
		try {
			final PrintWriter w = new PrintWriter(new FileWriter(outputFile));
	        w.println("digraph { ");
	        IDirectedGraph flow = m.getConservativeCFG();
	        for (int from=0; from<flow.getVertexCount(); ++from) {
	        	for (int to: flow.getEdges(from)) {
					w.println("\"" + methodInfo.getInstructionString(from) + "\" -> \"" + methodInfo.getInstructionString(to) + "\"");
	        		
	        	}
	        }
	        for (DataFlowEdge s: m.getConservativeDataFlowEdges()) {
	        	w.println(getEdgeString(c, m, methodInfo, s));
	        }
	        w.println("} ");
	        w.close();
	        return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	public String getEdgeString(ClassInfo c, MethodDataFlow m, MethodInfo methodInfo, DataFlowEdge edge) {
		String defVertexName;
		if (edge.isParameter()) {
			defVertexName = "ARG" + Integer.toString(m.getBytecodeVariableIndex(edge.getSourceInstruction()));
		} else {
			defVertexName = methodInfo.getInstructionString(edge.getSourceInstruction());
		}
		String useVertexName = methodInfo.getInstructionString(edge.getDestinationInstruction());

		ILocalVariableInfo v = m.getLocalVariable(edge);
		return "\"" + defVertexName + "\" -> \"" + useVertexName + "\" [ label=\"" + v.getName() + "\", style=\"dashed\" ]";
	}


	public boolean saveAsText(File output, ClassInfo c, MethodDataFlow m) {
		try {
			PrintWriter opcodeWriter = new PrintWriter(new FileWriter(output));
			MethodInfo methodInfo = m.getMethodInfo();
			opcodeWriter.println(c.getClassName());
			opcodeWriter.println(m.getMethodName());
			opcodeWriter.println("Instructions: ");
			for (int i=0; i<methodInfo.getInstructionCount(); ++i) {
				opcodeWriter.println(methodInfo.getInstructionString(i));
			}
			
			opcodeWriter.println("Local Variables: ");
			LocalVariableTable locals = new LocalVariableTable(methodInfo, methodInfo.getMethodBody().getMethodNode());
//			List<?> variables = methodInfo.getMethodNode().localVariables; 
//			if (variables != null) {
				for (int i=0; i<locals.size(); ++i) {
					opcodeWriter.println("(" + locals.getVariableIndex(i) + ") " + locals.getName(i) + ": " + locals.getType(i));
				}
//			} else {
//				opcodeWriter.println("<Not Available>");
//			}
			opcodeWriter.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}

}
