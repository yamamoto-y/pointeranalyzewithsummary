package soba.experimental.statistics;

import java.util.List;

import soba.bytecode.method.LocalVariableTable;
import soba.util.graph.IDepthFirstVisitor;

/**
 * This class is used by LocalDataFlowGraph
 * to analyze def-use relation.
 * @author ishio
 *
 */
public class LocalDefUseAnalysis implements IDepthFirstVisitor {
		
	private LocalVariableTable variableInfo;
	private int defVarId;
	private int defVarIndex;
	private boolean isParam;
	private List<LocalDataFlowEdge> dataFlowEdges;

	private int startIndex;
	private boolean loopVisited;

	
	public LocalDefUseAnalysis(LocalVariableTable variableInfo, int varId, boolean isParam, List<LocalDataFlowEdge> dataFlowEdges) {
		this.defVarId =  varId;
		this.defVarIndex = variableInfo.getVariableIndex(varId);
		this.variableInfo = variableInfo;
		this.dataFlowEdges = dataFlowEdges; 
		this.isParam = isParam;
	}
	
	
	@Override
	public boolean onVisit(int vertexId) {
		if (vertexId == startIndex) return true;

		if (variableInfo.isUse(vertexId)) {
			int varId = variableInfo.getAccessedVariableId(vertexId);
			int varIndex = variableInfo.getVariableIndex(varId);
			if (varIndex == defVarIndex) {
				dataFlowEdges.add(new LocalDataFlowEdge(startIndex, vertexId, defVarId));
				return !variableInfo.isDef(vertexId);
			} else {
				return true;
			}
		} else if (variableInfo.isDef(vertexId)) {
			int varId = variableInfo.getAccessedVariableId(vertexId);
			int varIndex = variableInfo.getVariableIndex(varId);
			return (varIndex != defVarIndex);
		} else {
			return true;
		}
	}
	
	@Override
	public void onStart(int startVertexId) {
		this.startIndex = startVertexId;
		this.loopVisited = false;
		if (isParam) {
			this.startIndex = LocalDataFlowEdge.PARAM_VERTEX_INDEX;
		} else {
			assert variableInfo.getAccessedVariableId(startVertexId) == this.defVarId;
		}
	}
	
	@Override
	public void onVisitAgain(int vertexId) {
		// To support a data-flow loop such as "for (int i=0; i<10; ++i) use(i);" (The increment depends on the previous increment).
		if (startIndex == vertexId) {
			if (variableInfo.isUse(vertexId) && !loopVisited) {
				loopVisited = true;
				dataFlowEdges.add(new LocalDataFlowEdge(startIndex, vertexId, defVarId));
			}
		}
	}
	
	@Override
	public void onLeave(int vertexId) {
	}
	
	@Override
	public void onFinished(boolean[] visited) {
	}
	

}
