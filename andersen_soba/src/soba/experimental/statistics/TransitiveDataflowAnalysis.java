package soba.experimental.statistics;


import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.method.DataFlowEdge;
import soba.bytecode.method.DataFlowInfo;
import soba.bytecode.method.LocalVariables;

public class TransitiveDataflowAnalysis {

	private LocalVariables v;
	
	private TransitiveDataflowPath normalEdges;
	private TransitiveDataflowPath flowInsensitiveEdges;
	
	private TIntObjectHashMap<HashMap<String, List<DataFlowEdge>>> edgeGroups;
	private List<List<DataFlowEdge>> edgeGroupList;
	
	private List<Record> records;
	
	/**
	 * Select a List object for a variable.
	 * @param instruction specifies an instruction that accesses a variable.
	 * @param varIndex also specifies the variable; this value is used only for validating the process. 
	 * @return a list which should include data-flow edges for the specified variable.
	 * This method returns null for an anonymous variable.  
	 */
	private List<DataFlowEdge> getGroup(int instruction, int varIndex) {
		int entry = v.findEntryForInstruction(instruction);
		assert v.getVariableIndex(entry) == varIndex;
		String varName = v.getVariableName(entry);
		
		if (entry != -1 && varName != null) {
			HashMap<String, List<DataFlowEdge>> varGroup = edgeGroups.get(entry);
			if (varGroup == null) {
				varGroup = new HashMap<String, List<DataFlowEdge>>();
				edgeGroups.put(varIndex, varGroup);
			}
			List<DataFlowEdge> edges = varGroup.get(varName);
			if (edges == null) {
				edges = new ArrayList<DataFlowEdge>();
				varGroup.put(varName, edges);
				edgeGroupList.add(edges);
			}
			return edges;
		} else {
			return null;
		}

	}
	
	public TransitiveDataflowAnalysis(DataFlowInfo info, MethodNode m) {
		v = info.getLocalVariables();
		InstructionList method = new InstructionList(m);
		normalEdges = new TransitiveDataflowPath(method);
		flowInsensitiveEdges = new TransitiveDataflowPath(method);
		
		edgeGroupList = new ArrayList<List<DataFlowEdge>>();
		edgeGroups = new TIntObjectHashMap<HashMap<String,List<DataFlowEdge>>>();
		for (DataFlowEdge e: info.getEdges()) {
			// Classify local variable edges into groups based on its variable name and index
			if (e.isLocal()) {
				List<DataFlowEdge> list = getGroup(e.getDestinationInstruction(), e.getVariableIndex());
				if (list != null) list.add(e);
			} 
			
			// Create data structure for edges
			normalEdges.addEdge(e);
			flowInsensitiveEdges.addEdge(e);
		}
		
		// Connect artificail flow-insensitive edges
		for (List<DataFlowEdge> group: edgeGroupList) {
			// for all I and J, connect source[i] -> dest[j]
			for (int i=0; i<group.size(); ++i) {
				for (int j=0; j<group.size(); ++j) {
					if (i == j) continue; 
					flowInsensitiveEdges.addArtificialEdge(group.get(i), group.get(j));
				}
			}
		}
		
		// オペランドごとの到達可能頂点集合を求める
		records = new ArrayList<Record>();
		for (int i=0; i<m.instructions.size(); ++i) {
			if (method.isDestination(i)) {
				int opCount = info.getOperandCount(i);
				for (int op=0; op<opCount; op++) {
					int[] sources = normalEdges.getReachableSources(i, op);
					int[] flowInsensitivesources = flowInsensitiveEdges.getReachableSources(i, op);
					int[] reducedSources = method.removeEquivalentInstructions(sources);
					int[] reducedFlowInsensitiveSources = method.removeEquivalentInstructions(flowInsensitivesources);
					records.add(new Record(i, op, sources, flowInsensitivesources, reducedSources, reducedFlowInsensitiveSources,  m.instructions.get(i).getOpcode()));
				}
			}
		}
	}
	
	
	public int getOperandCount() {
		return records.size();
	}
	
	public int getPathCount(int operand) {
		return records.get(operand).sourceCount;
	}
	
	public int getFlowInsensitivePathCount(int operand) {
		return records.get(operand).flowInsensitiveSourceCount;
	}
	
	public int getInstructionIndex(int operand) {
		return records.get(operand).destinationInstruction;
	}

	public int getInstructionOperand(int operand) {
		return records.get(operand).destinationOperand;
	}
	
	public int getOpcode(int operand) {
		return records.get(operand).opcode;
	}
	
	public String getPathCategory(int operand) {
		return records.get(operand).getPathCategory();
	}

	public int getReducedPathCount(int operand) {
		return records.get(operand).reducedSourceCount;
	}
	
	public int getReducedFlowInsensitivePathCount(int operand) {
		return records.get(operand).reducedFlowInsensitiveSourceCount;
	}
	
	public String getReducedPathCategory(int operand) {
		return records.get(operand).getReducedPathCategory();
	}

	public String getOperationType(int operand) {
		return records.get(operand).getOperationType();
	}

	private static class Record {
		int destinationInstruction;
		int destinationOperand;
		int sourceCount;
		int flowInsensitiveSourceCount;
		int reducedSourceCount;
		int reducedFlowInsensitiveSourceCount;
		String sourceComparison;
		String reducedSourceComparison;
		int opcode;
		
		public Record(int destInst, int destOperand, int[] sources, int[] flowInsensitiveSources, int[] reducedSources, int[] reducedFlowInsensitiveSources, int opcode) {
			this.destinationInstruction = destInst;
			this.destinationOperand = destOperand;
			this.sourceCount = sources.length;
			this.flowInsensitiveSourceCount = flowInsensitiveSources.length;
			this.reducedSourceCount = reducedSources.length;
			this.reducedFlowInsensitiveSourceCount = reducedFlowInsensitiveSources.length;
			this.sourceComparison = compareSources(sources, flowInsensitiveSources);
			this.reducedSourceComparison = compareSources(reducedSources, reducedFlowInsensitiveSources);
			this.opcode = opcode;
		}
		
		private static String compareSources(int[] sources, int[] flowInsensitive) {
			boolean sourcesIncludeFlowInsensitive = TransitiveDataflowAnalysis.includeIntArray(sources, flowInsensitive);
			boolean flowInsensitiveIncludeSources = TransitiveDataflowAnalysis.includeIntArray(flowInsensitive, sources);

			if (flowInsensitiveIncludeSources) {
				if (sourcesIncludeFlowInsensitive) {
					return "E"; // equals
				} else {
					return "I"; // insensitive; flowInsensitive contains some additional nodes.
				}
			} else {
				if (sourcesIncludeFlowInsensitive) {
					return "F"; // Failure. sources contains some additional node that should not be involved.
				} else {
					return "G"; // another failure.  sources and flowInsensitive has some inconsistent reachability.
				}
			}
		}
		
		public String getPathCategory() {
			return sourceComparison;
		}
		
		public String getReducedPathCategory() {
			return reducedSourceComparison;
		}
		
		public String getOperationType() {
			switch (opcode) {
			case Opcodes.AALOAD:
			case Opcodes.BALOAD:
			case Opcodes.CALOAD:
			case Opcodes.DALOAD:
			case Opcodes.FALOAD:
			case Opcodes.IALOAD:
			case Opcodes.LALOAD:
			case Opcodes.SALOAD:
				return "AR"; // array reference

			case Opcodes.AASTORE:
			case Opcodes.BASTORE:
			case Opcodes.CASTORE:
			case Opcodes.DASTORE:
			case Opcodes.FASTORE:
			case Opcodes.IASTORE:
			case Opcodes.LASTORE:
			case Opcodes.SASTORE:
				return "AS"; // array store
				 
			case Opcodes.ARRAYLENGTH:
				return "AL"; // array length

			case Opcodes.GETFIELD: 
			case Opcodes.GETSTATIC:
				return "GF"; // field

			case Opcodes.DRETURN:
			case Opcodes.FRETURN:
			case Opcodes.IRETURN:
			case Opcodes.LRETURN:
				return "RE"; // return
				
			case Opcodes.ATHROW:
				return "TH"; // throw
				
			case Opcodes.IF_ACMPEQ:
			case Opcodes.IF_ACMPNE:
			case Opcodes.IF_ICMPEQ:
			case Opcodes.IF_ICMPGE:
			case Opcodes.IF_ICMPGT:
			case Opcodes.IF_ICMPLE:
			case Opcodes.IF_ICMPLT:
			case Opcodes.IF_ICMPNE:
			case Opcodes.IFEQ:
			case Opcodes.IFGE:
			case Opcodes.IFGT:
			case Opcodes.IFLE:
			case Opcodes.IFLT:
			case Opcodes.IFNE:
			case Opcodes.IFNONNULL:
			case Opcodes.IFNULL:
			case Opcodes.LOOKUPSWITCH:
			case Opcodes.TABLESWITCH:
				return "IF";
				
			case Opcodes.INVOKEDYNAMIC:
			case Opcodes.INVOKEINTERFACE:
			case Opcodes.INVOKESPECIAL:
			case Opcodes.INVOKESTATIC:
			case Opcodes.INVOKEVIRTUAL:
				return "MI"; // method invocation
				
			case Opcodes.MONITORENTER:
			case Opcodes.MONITOREXIT:
				return "mo";  // monitor objct
				
			case Opcodes.MULTIANEWARRAY:
			case Opcodes.NEWARRAY:
				return "AC";  // array creation

			case Opcodes.PUTFIELD:
			case Opcodes.PUTSTATIC:
				return "PF";  // put field
				
			}
			return "__"; // other
		}
	}
	
	/**
	 * Return true if s1 includes all elements in s2. 
	 */
	public static boolean includeIntArray(int[] s1, int[] s2) {
		TIntHashSet s1set = new TIntHashSet(s1);
		for (int elem: s2) {
			if (!s1set.contains(elem)) {
				return false;
			}
		}
		return true;
	}

	
}
