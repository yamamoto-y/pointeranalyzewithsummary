package soba.experimental.statistics;

import java.util.ArrayList;

public class PrefixCategorizer {

	private ArrayList<String> categoryKeys;
	private ArrayList<String> categoryNames;
	private String defaultCategory;
	
	public PrefixCategorizer(String[] prefixList, String[] nameList, String defaultName) {
		assert prefixList.length == nameList.length: "Two arrays must be the same length.";
		
		categoryKeys = new ArrayList<String>();
		categoryNames = new ArrayList<String>();
		
		for (String prefix: prefixList) {
			categoryKeys.add(prefix);
		}
		
		for (String name: nameList) {
			categoryNames.add(name);
		}
		
		defaultCategory = defaultName;
	}
	
	public String getCategory(String s) {
		for (int i=0; i<categoryKeys.size(); ++i) {
			if (s.startsWith(categoryKeys.get(i))) {
				return categoryNames.get(i);
			}
		}
		return defaultCategory;
	}
}
