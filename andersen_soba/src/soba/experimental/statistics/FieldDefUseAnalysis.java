package soba.experimental.statistics;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;

import soba.model.DynamicBindingResolver;
import soba.util.IntPairList;
import soba.util.graph.IDepthFirstVisitor;


public class FieldDefUseAnalysis implements IDepthFirstVisitor {

	private String fieldOwner;
	private String fieldName;
	private String fieldDesc;
	private boolean isStatic;
	private int startId;
	private DynamicBindingResolver resolver;
	private InsnList instructions;
	private IntPairList edges;
	
	public FieldDefUseAnalysis(String owner, String name, String desc, boolean isStatic, DynamicBindingResolver resolver, InsnList instructions, IntPairList edges) {
		this.fieldOwner = owner;
		this.fieldName = name;
		this.fieldDesc = desc;
		this.isStatic = isStatic;
		this.resolver = resolver;
		this.instructions = instructions;
		this.edges = edges;
	}
	
	@Override
	public void onStart(int startVertexId) {
		startId = startVertexId;
	}

	@Override
	public boolean onVisit(int vertexId) {
		if (instructions.get(vertexId).getType() == AbstractInsnNode.FIELD_INSN) {
			FieldInsnNode f = (FieldInsnNode)instructions.get(vertexId);
			String className = null;
			if (f.getOpcode() == Opcodes.GETFIELD && !isStatic) {
				className = resolver.resolveInstanceFieldOwner(f.owner, f.name, f.desc);
				if (className == null) className = f.owner;
			} else if (f.getOpcode() == Opcodes.GETSTATIC && isStatic) {
				className = resolver.resolveStaticFieldOwner(f.owner, f.name, f.desc);
				if (className == null) className = f.owner;
			} // other cases: PUTSTATIC, PUTFIELD
			if (className != null) { 
				// If the instruction reads the same field
				if (fieldName.equals(f.name) && fieldDesc.equals(f.desc) && fieldOwner.equals(className)) {
					edges.add(startId, vertexId);
				}
			}
		}

		// Continue the visit because a write instruciton is a "may-kill"
		// rather than a "must-kill" in object-insensitive analysis.
		return true;
	}

	@Override
	public void onVisitAgain(int vertexId) {
		// For a field, there is no single instruction that reads and writes a field.
		// Therefore, we don't implement the method.
	}

	@Override
	public void onLeave(int vertexId) {
	}

	@Override
	public void onFinished(boolean[] visited) {
	}

}
