package soba.experimental.statistics;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;

import soba.util.IntPairList;
import soba.util.graph.DepthFirstSearch;
import soba.util.graph.IDepthFirstVisitor;
import soba.util.graph.IDirectedGraph;



public class ArrayDefUseAnalysis implements IDepthFirstVisitor {

	public static ArrayDefUseAnalysis analyze(MethodNode m, IDirectedGraph cfg) {
		IntPairList result = new IntPairList();
		ArrayDefUseAnalysis analysis = new ArrayDefUseAnalysis(m.instructions, result);
		for (int i=0; i<m.instructions.size(); ++i) {
			AbstractInsnNode instruction = m.instructions.get(i);
			switch (instruction.getOpcode()) {
			case Opcodes.IASTORE:
			case Opcodes.LASTORE:
			case Opcodes.FASTORE:
			case Opcodes.DASTORE:
			case Opcodes.AASTORE:
			case Opcodes.BASTORE:
			case Opcodes.CASTORE:
			case Opcodes.SASTORE:
				DepthFirstSearch.search(cfg, i, analysis);
			}
		}
		return analysis;
	}
	
	
	private InsnList instructions;
	private IntPairList edges;
	private int startId;
	
	public int getEdgeCount() {
		return edges.size();
	}
	
	private ArrayDefUseAnalysis(InsnList instructions, IntPairList edges) {
		this.instructions = instructions;
		this.edges = edges;
	}
	
	@Override
	public void onStart(int startVertexId) {
		this.startId = startVertexId;
	}


	@Override
	public boolean onVisit(int vertexId) {
		AbstractInsnNode insn = (AbstractInsnNode)instructions.get(vertexId);
		switch (insn.getOpcode()) {
		case Opcodes.IALOAD:
		case Opcodes.LALOAD:
		case Opcodes.FALOAD:
		case Opcodes.DALOAD:
		case Opcodes.AALOAD:
		case Opcodes.BALOAD:
		case Opcodes.CALOAD:
		case Opcodes.SALOAD:
			// if the same kind
			if (insn.getOpcode() - Opcodes.IALOAD == instructions.get(startId).getOpcode() - Opcodes.IASTORE) {
				edges.add(startId, vertexId);
			}
		}
		return true; // always continue 
	}

	@Override
	public void onVisitAgain(int vertexId) {
	}

	@Override
	public void onFinished(boolean[] visited) {
	}

	@Override
	public void onLeave(int vertexId) {
	}

	
}
