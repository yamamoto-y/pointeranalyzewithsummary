package soba.experimental.statistics;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TryCatchBlockNode;

import soba.bytecode.method.OpcodeString;

public class InstructionList {

	private MethodNode m;
	private TIntIntHashMap equivalentInstructions;


	public InstructionList(MethodNode m) {
		this.m = m;
		
		equivalentInstructions = new TIntIntHashMap();
		for (int i=0; i<m.instructions.size(); ++i) {
			for (int j=i+1; j<m.instructions.size(); ++j) {
				if (i != j) {
					if (isEquivalentOperation(i, j)) {
						equivalentInstructions.put(j, i);
					}
				}
			}
		}
	}
	
	public int getInstructionCount() {
		return m.instructions.size();
	}
	
	/**
	 * Remove equivalent instructions for comparison.
	 * @param instructions
	 * @return
	 */
	public int[] removeEquivalentInstructions(int[] instructions) {
		TIntHashSet filtered = new TIntHashSet(instructions.length);
		for (int inst: instructions) {
			if (equivalentInstructions.containsKey(inst)) {
				filtered.add(equivalentInstructions.get(inst));
			} else {
				filtered.add(inst);
			}
		}
		return filtered.toArray();
	}
	
	private boolean isCatchInstruction(int instruction) {
		AbstractInsnNode storeInstruction = m.instructions.get(instruction);
		if (storeInstruction.getOpcode() != Opcodes.ASTORE) return false;
		
		List<?> blocks = m.tryCatchBlocks;
		for (int i=0; i<blocks.size(); ++i) {
			TryCatchBlockNode node = (TryCatchBlockNode)blocks.get(i);
			AbstractInsnNode handler = node.handler;
			while (handler != null) {
				if (handler == storeInstruction) {
					return true;
				} else {
					if (handler.getType() == AbstractInsnNode.LABEL || 
						handler.getType() == AbstractInsnNode.FRAME ||
						handler.getType() == AbstractInsnNode.LINE) {
						handler = handler.getNext();
					} else {
						break; // different try-catch or finally block
					}
				}
			}
		}
		return false;
	}
	
	public boolean isSource(int instruction) {
		if (instruction >= m.instructions.size()) { // formal-parameters 
			return true;
		} else {
			AbstractInsnNode node = m.instructions.get(instruction);
			switch (node.getOpcode()) {
			// invoke
			case Opcodes.INVOKEDYNAMIC:
			case Opcodes.INVOKEINTERFACE:
			case Opcodes.INVOKESPECIAL:
			case Opcodes.INVOKESTATIC:
			case Opcodes.INVOKEVIRTUAL:
			// field
			case Opcodes.GETSTATIC:
			case Opcodes.GETFIELD:
			// array
			case Opcodes.AALOAD:
			case Opcodes.BALOAD:
			case Opcodes.CALOAD:
			case Opcodes.DALOAD:
			case Opcodes.FALOAD:
			case Opcodes.IALOAD:
			case Opcodes.LALOAD:
			case Opcodes.SALOAD:
			case Opcodes.ARRAYLENGTH:
			// object instantiation
			case Opcodes.NEW:
			case Opcodes.NEWARRAY:
			case Opcodes.ANEWARRAY:
			case Opcodes.MULTIANEWARRAY:

			case Opcodes.ACONST_NULL:
			case Opcodes.BIPUSH:
			case Opcodes.DCONST_0:
			case Opcodes.DCONST_1:
			case Opcodes.FCONST_0:
			case Opcodes.FCONST_1:
			case Opcodes.FCONST_2:
			case Opcodes.ICONST_0:
			case Opcodes.ICONST_1:
			case Opcodes.ICONST_2:
			case Opcodes.ICONST_3:
			case Opcodes.ICONST_4:
			case Opcodes.ICONST_5:
			case Opcodes.ICONST_M1:
			case Opcodes.LCONST_0:
			case Opcodes.LCONST_1:
			case Opcodes.LDC:
			case Opcodes.SIPUSH:
				
				return true;
			
			case Opcodes.ASTORE:
				return isCatchInstruction(instruction);

			default:
				return false;
			}
		}
	}
	
	
	public boolean isDestination(int instruction) {
		if (instruction >= m.instructions.size()) { // formal-parameters 
			return false;
		} else {
			AbstractInsnNode node = m.instructions.get(instruction);
			switch (node.getOpcode()) {
			// 
			case Opcodes.AASTORE:
			case Opcodes.AALOAD:
			case Opcodes.ARRAYLENGTH:
			case Opcodes.ATHROW:
			case Opcodes.BALOAD:
			case Opcodes.BASTORE:
			case Opcodes.CALOAD:
			case Opcodes.CASTORE:
			case Opcodes.DALOAD:
			case Opcodes.DASTORE:
			case Opcodes.DRETURN:
			case Opcodes.FALOAD:
			case Opcodes.FASTORE:
			case Opcodes.FRETURN:
			case Opcodes.GETFIELD: 
			case Opcodes.GETSTATIC:
			case Opcodes.IALOAD:
			case Opcodes.IASTORE:
			case Opcodes.IF_ACMPEQ:
			case Opcodes.IF_ACMPNE:
			case Opcodes.IF_ICMPEQ:
			case Opcodes.IF_ICMPGE:
			case Opcodes.IF_ICMPGT:
			case Opcodes.IF_ICMPLE:
			case Opcodes.IF_ICMPLT:
			case Opcodes.IF_ICMPNE:
			case Opcodes.IFEQ:
			case Opcodes.IFGE:
			case Opcodes.IFGT:
			case Opcodes.IFLE:
			case Opcodes.IFLT:
			case Opcodes.IFNE:
			case Opcodes.IFNONNULL:
			case Opcodes.IFNULL:
			case Opcodes.INVOKEDYNAMIC:
			case Opcodes.INVOKEINTERFACE:
			case Opcodes.INVOKESPECIAL:
			case Opcodes.INVOKESTATIC:
			case Opcodes.INVOKEVIRTUAL:
			case Opcodes.IRETURN:
			case Opcodes.LALOAD:
			case Opcodes.LASTORE:
			case Opcodes.LOOKUPSWITCH:
			case Opcodes.LRETURN:
			case Opcodes.MONITORENTER:
			case Opcodes.MONITOREXIT:
			case Opcodes.MULTIANEWARRAY:
			case Opcodes.NEWARRAY:
			case Opcodes.PUTFIELD:
			case Opcodes.PUTSTATIC:
			case Opcodes.SALOAD:
			case Opcodes.SASTORE:
			case Opcodes.TABLESWITCH:
				return true;
				
			default:
				return false;
			}
		}
	}

	private boolean isEquivalentOperation(int instruction1, int instruction2) {
		if (instruction1 == instruction2) {
			return true;
		} else {
			// Instructions for constant values are equivalent.
			AbstractInsnNode node1 = m.instructions.get(instruction1);
			AbstractInsnNode node2 = m.instructions.get(instruction2);
			return node1.getOpcode() == node2.getOpcode() &&
			        OpcodeString.isConstantOperation(node1.getOpcode());
		}
	}
	


}
