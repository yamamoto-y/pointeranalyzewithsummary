package soba.bytecode.analysis.vta;

import soba.model.IFieldInfo;
import soba.model.IMethodInfo;

public interface IAnalysisTarget {

	public boolean isTargetMethod(IMethodInfo m);
	public boolean isTargetField(IFieldInfo f);
	public boolean assumeExternalCallers(IMethodInfo m);

	public boolean isExcludedType(String className);
	
}
