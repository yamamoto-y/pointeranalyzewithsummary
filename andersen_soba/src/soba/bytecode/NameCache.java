package soba.bytecode;

import soba.util.ObjectCache;

/**
 * A global instance of cache object for identifiers.
 */
public class NameCache {

	private static ObjectCache<String> typenames = new ObjectCache<String>();
	
	public static String getSharedInstance(String typeName) {
		if (typenames != null) {
			return typenames.getSharedInstance(typeName);
		} else {
			return typeName;
		}
	}
	
	public static String[] replaceWithSharedString(String[] names) {
		for (int i=0; i<names.length; ++i) {
			names[i] = getSharedInstance(names[i]);
		}
		return names;
	}
	
	public static void disableCache() {
		typenames = null;
	}
	
	public static void clearCache() {
		typenames = new ObjectCache<String>();
	}
}
