package soba.bytecode;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import soba.model.ClassHierarchy;
import soba.model.IFieldInfo;
import soba.model.IMethodInfo;
import soba.util.files.IFileEnumerator;
import soba.util.files.IFileEnumeraterCallback;

public class JavaProgram {
	
	private Map<String, ClassInfo> classes;
	private ClassHierarchy classHierarchy;
	private List<ClassInfo> loaded;
	private List<String> duplicated;
	private List<String> filtered;
	private List<ErrorMessage> errors;

	public JavaProgram(final IFileEnumerator[] fileEnumeraters) {
		this(fileEnumeraters, null);
	}
	
	public JavaProgram(final IFileEnumerator[] fileEnumeraters, final IClassFilter filter) {
		classes = new HashMap<String, ClassInfo>(65536);
		errors = new ArrayList<ErrorMessage>(1024);
		loaded = new ArrayList<ClassInfo>(65536);
		duplicated = new ArrayList<String>(1024);
		filtered = new ArrayList<String>(1024);
		classHierarchy = new ClassHierarchy();
		
		for (IFileEnumerator enumerater: fileEnumeraters) {
			if (enumerater == null) continue;
			
			enumerater.process(new IFileEnumeraterCallback() {
				
				@Override
				public boolean reportError(String name, Exception e) {
					errors.add(new ErrorMessage(name, e));
					return false;
				}
				
				@Override
				public void process(String name, InputStream stream) throws IOException {
					if (filter == null || filter.loadClass(name)) {
						ClassInfo c = new ClassInfo(name, stream);
						if (filter == null || filter.acceptClass(c)) {
							if (!classes.containsKey(c.getClassName())) {
								classes.put(c.getClassName(), c);
								loaded.add(c);
								classHierarchy.registerClass(c);
							} else {
								duplicated.add(name);
							}
						} else {
							filtered.add(name);
						}
					} else {
						filtered.add(name);
					}
				}
				
				@Override
				public boolean isTarget(String name) {
					return name.endsWith(".class");
				}
			});
		}
	}
	
	public List<ClassInfo> getClasses() {
		return loaded;
	}
	
	public List<String> getFiltered() {
		return filtered;
	}
	
	public List<String> getDuplicated() {
		return duplicated;
	}
	
	public ClassInfo getClassInfo(String className) {
		return classes.get(className);
	}
	
	public MethodInfo getMethodInfo(IMethodInfo m) {
		ClassInfo c = classes.get(m.getClassName());
		if (c != null) {
			return c.getMethodByName(m.getMethodName(), m.getDescriptor());
		}
		return null;
	}
	
	public FieldInfo getFieldInfo(IFieldInfo f) {
		ClassInfo c = classes.get(f.getClassName());
		if (c != null) {
			return c.getFieldByName(f.getFieldName(), f.getDescriptor());
		}
		return null;
	}
	
	public ClassHierarchy getClassHierarchy() {
		return classHierarchy;
	}
	
	public List<ErrorMessage> getErrors() {
		return errors;
	}
	
	public static class ErrorMessage { 
		private String dataName;
		private Exception exception;
		public ErrorMessage(String name, Exception e) {
			this.dataName = name;
			this.exception = e;
		}
		public String getDataName() {
			return dataName;
		}
		public Exception getException() {
			return exception;
		}
	}
}
