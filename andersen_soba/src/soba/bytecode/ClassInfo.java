package soba.bytecode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import soba.model.IClassInfo;



public class ClassInfo implements IClassInfo {

	private static final String PACKAGE_SEPARATOR ="/";
	private static final String DEFAULT_PACKAGE = "$default$";
	
	private String fileName;
	private String relativeFileName;
	private String packageName;
	private String className;
	private String md5hash;
	private List<?> methods; // List<MethodNode> but ASM has no type information.
	private List<FieldInfo> fields = new ArrayList<FieldInfo>();
	private MethodInfo[] methodInfoList;
	
	private String superclassName;
	private List<String> interfaceNames;
	
	/**
	 * 
	 * @param fileName
	 * @param binaryStream specifies a stream of Java bytecode.  
	 * Please note that the stream is not closed by the method; 
	 * the callers must close the stream by themselves.
	 */
	public ClassInfo(String fileName, InputStream binaryStream) throws IOException {
		this.fileName = fileName; 
		byte[] bytes = readToEnd(binaryStream);
		ClassReader cr1;
		try {
			cr1 = new ClassReader(bytes) {
				/**
				 * This extension reduces the number of allocated strings.
				 * When reading SOBA and GNU Trove class files, 52MB of 72MB strings can be discarded.
				 */
				@Override
				public String readUTF8(int index, char[] buf) {
					return NameCache.getSharedInstance(super.readUTF8(index, buf));
				}
			
			};
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ClassReadFailureException("ASM ClassReader cannot parse the bytecode. " + fileName + " " + e.getLocalizedMessage());
		}
		ClassNode classNode = new ClassNode();
		cr1.accept(classNode, 0);
		this.className = classNode.name;
		this.relativeFileName = classNode.sourceFile;

		int pkgIndex = className.lastIndexOf(PACKAGE_SEPARATOR);
		if (pkgIndex >= 0) {
			packageName = NameCache.getSharedInstance(className.substring(0, pkgIndex));
		} else {
			this.packageName = DEFAULT_PACKAGE;
		}
		this.md5hash = MD5.getMD5(bytes);

//		if (hasImplementation) { // Comment out because an interface also has method declaration.
		methods = classNode.methods;
//	        List<?> methodNodes = classNode.methods;
//	        for (int methodIndex = 0; methodIndex < methodNodes.size(); ++methodIndex) {
//	            MethodNode method = (MethodNode)methodNodes.get(methodIndex);
////	            if (hasMethodBody(method)) {
//            	methods.add(method);
////	            } else {
////	            	abstractMethods.add(method);
////	            }
//	        }
////		}
		for (int i=0; i<classNode.fields.size(); ++i) {
			fields.add(new FieldInfo(this, (FieldNode)classNode.fields.get(i)));
		}
		superclassName = NameCache.getSharedInstance(classNode.superName);
		interfaceNames = new ArrayList<String>(classNode.interfaces.size());
		for (int i=0; i<classNode.interfaces.size(); ++i) {
			interfaceNames.add(NameCache.getSharedInstance((String)classNode.interfaces.get(i)));
		}
	}

	private static byte[] readToEnd(InputStream stream) throws IOException {
		byte[] buf = new byte[4096];
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int n;
		while ((n = stream.read(buf, 0, buf.length)) > 0) {
			buffer.write(buf, 0, n);
		}
		return buffer.toByteArray();
	}

	public String getPackageName() {
		return packageName;
	}
	
	public String getSuperClass() {
		return superclassName;
	}
	
	public List<String> getInterfaces() {
		return interfaceNames;
	}
	
	public String getHash() {
		return md5hash;
	}
	

//	private MethodDataFlow processMethod(final String className, final MethodNode methodNode) {
//        return new MethodDataFlow(className, methodNode);
//	}

	public String getClassDirPath() {
		return packageName.replace('/', File.separatorChar); 
	}

	/**
	 * @return the file name whose containing the class data.
	 * If the class file is contained in a ZIP/JAR file,
	 * the resultant path incidates the zip file and an internal file path in the archive.
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * @return a Java source file name.
	 * The method may return null. 
	 */
	public String getRelativeFileName() {
		return getClassDirPath() + File.separator + relativeFileName;
	}
	
	/**
	 * @return the class name with its package name.
	 * The class name is an internal representation; 
	 * a package name is separated by "/".
	 */
	public String getClassName() {
		return className;
	}
	
	public int getMethodCount() {
		return methods.size();
	}
	
	public MethodInfo getMethod(int methodIndex) {
		if (methodInfoList == null) {
			methodInfoList = new MethodInfo[methods.size()];
		}
		MethodInfo m = methodInfoList[methodIndex];
		if (m == null) {
			m = new MethodInfo(this, (MethodNode)methods.get(methodIndex));
			methodInfoList[methodIndex] = m;
		}
		return m;
	}
	
	public FieldInfo getField(int fieldIndex) {
		return fields.get(fieldIndex);
	}
	
	public int getFieldCount() {
		return fields.size();
	}
	
	/**
	 * Return true if a field is included.
	 * @param fieldName
	 * @param fieldDesc
	 * @return
	 */
	public FieldInfo getFieldByName(String fieldName, String fieldDesc) {
		for (int i=0; i<getFieldCount(); ++i) {
			if (getField(i).getFieldName().equals(fieldName) && getField(i).getDescriptor().equals(fieldDesc)) {
				return getField(i);
			}
		}
		return null;
	}
	
	/**
	 * @param methodName is a method name.
	 * @param methodDesc is a method descriptor without generics information.
	 * @return a MethodInfo object if the class declares the specified method. 
	 */
	public MethodInfo getMethodByName(String methodName, String methodDesc) {
		for (int  i=0; i<getMethodCount(); ++i) {
			MethodNode m = (MethodNode)methods.get(i);
			if (m.name.equals(methodName) &&
				m.desc.equals(methodDesc)) {
				return getMethod(i);
			}
		}
		return null;
	}

}
