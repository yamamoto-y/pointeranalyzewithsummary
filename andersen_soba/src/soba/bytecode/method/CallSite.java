package soba.bytecode.method;

import soba.model.IMethodInfo;
import soba.model.Invocation;

public class CallSite {
	
	private IMethodInfo ownerMethod;
	private int instructionIndex;
	private Invocation invocation;
	
	/**
	 * 
	 * @param m specifies an owner of a call site.
	 * @param instIndex
	 * @param invokedMethod
	 */
	public CallSite(IMethodInfo m, int instIndex, Invocation invokedMethod) {
		this.ownerMethod = m;
		this.instructionIndex = instIndex;
		this.invocation = invokedMethod;
	}
	
	public IMethodInfo getOwnerMethod() {
		return ownerMethod;
	}
	
	public int getInstructionIndex() {
		return instructionIndex;
	}
	
	public Invocation getInvokedMethod() {
		return invocation;
	}
	

}
