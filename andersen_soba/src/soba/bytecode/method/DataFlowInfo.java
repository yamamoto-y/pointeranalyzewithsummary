package soba.bytecode.method;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.analysis.Frame;


import soba.util.IntPairList;
import soba.util.ObjectIdMap;
import soba.util.graph.DirectedGraph;
import soba.util.graph.IDirectedGraph;

/**
 * This class stores data-flow information for a single method.
 * @author ishio
 */
public class DataFlowInfo {

	private ObjectIdMap<AbstractInsnNode> instructions;
	private DataFlowAnalyzer analyzer;
	private LocalVariableTable localVariables;
	private LocalVariables locals;

	private List<DataFlowEdge> dataFlowEdges;
	private List<DataFlowEdge> dataFlowEdgesSourceOrder;

	/**
	 * Construct data-flow information for further anlaysis 
	 * based on DataFlowAnalyzer's result.
	 * @param instructions
	 * @param analyzer that has analyzed a method.
	 * @param table is a local variable table for the method.
	 */
	public DataFlowInfo(ObjectIdMap<AbstractInsnNode> instructions, DataFlowAnalyzer analyzer, LocalVariableTable table) {
		this.instructions = instructions;
		this.analyzer = analyzer;
		this.localVariables = table;
	}
	
	public LocalVariables getLocalVariables() {
		if (locals == null) {
			locals = new LocalVariables(this, analyzer.getAnalyzedMethod());
		}
		return locals;
	}
	
	/**
	 * @return a normal control-flow graph.
	 * The graph assumes that no instructions in a try block throw an exception.
	 */
	public IDirectedGraph getNormalControlFlow() {
		return new DirectedGraph(instructions.size(), analyzer.getNormalControlFlow());
	}

	/**
	 * @return a conservative control-flow graph.
	 * The graph assumes that every instruction in a try block may throw an exception.
	 */
	public IDirectedGraph getConservativeControlFlow() {
		return new DirectedGraph(instructions.size(), analyzer.getConservativeControlFlow());
	}
	
	/**
	 * @return a control dependence graph computed with a normal control-flow graph.
	 */
	public DirectedGraph getControlDependence() {
		IntPairList edges = analyzer.getNormalControlFlow();
		DirectedGraph regularControlFlow = new DirectedGraph(instructions.size(), edges);
		return ControlDependence.getDependence(instructions.size(), regularControlFlow);

	}

	/**
	 * @return the number of instructions in the method.
	 */
	public int getInstructionCount() {
		return analyzer.getFrames().length;
	}
	
	/**
	 * Return a local variable corresponding to a data-flow edge.
	 * @param e specifies a data-flow edge.
	 * The edge must be returned by the same DataFlowInfo object.
	 * @return a local variable information.
	 * If the edge is caused by an operand stack, null is returned.
	 */
	public ILocalVariableInfo getLocalVariable(DataFlowEdge e) {
		if (e.isLocal()) {
			ILocalVariableInfo v1;
			if (e.getSourceInstruction() == FastSourceInterpreter.METHOD_ENTRY) {
				v1 = localVariables.getParam(e.getVariableIndex());
			} else {
				v1  = localVariables.getAccessedVariable(e.getSourceInstruction());
			}
			if (v1 != null && !v1.isAnonymous()) {
				return v1;
			}
			ILocalVariableInfo v2 = localVariables.getAccessedVariable(e.getDestinationInstruction());
			if (v2 != null && !v2.isAnonymous() || v1 == null) {
				return v2;
			}
			// v1.isAnonymous AND (v2 == null OR v2.isAnonymous)
			return v1;
		} else {
			return null;
		}
	}

	/**
	 * Return true if the specified instruction refers to operands on a stack.
	 */
	public boolean useStack(int instructionIndex) {
		return analyzer.getOperandCount(instructionIndex) > 0;
	}

	/**
	 * Return true if the specified instruction refers to a local variable.
	 */
	private boolean referLocal(int instructionIndex) { 
		switch (instructions.getItem(instructionIndex).getOpcode()) {
		case Opcodes.ILOAD:
		case Opcodes.LLOAD:
		case Opcodes.FLOAD:
		case Opcodes.DLOAD:
		case Opcodes.ALOAD:
		case Opcodes.IINC:
			return true;
		}
		return false;
	}
	
	private void computeDataFlowEdges() {
		if (dataFlowEdges != null) return;
		List<DataFlowEdge> edges = new ArrayList<DataFlowEdge>();
		
		for (int instructionIndex=0; instructionIndex<instructions.size(); ++instructionIndex) {
			Frame f = analyzer.getFrames()[instructionIndex];
			if (useStack(instructionIndex)) {
				int operands = analyzer.getOperandCount(instructionIndex);
				for (int opIndex=0; opIndex<operands; ++opIndex) {
					int stackPos = f.getStackSize() - operands + opIndex;
					FastSourceValue value = (FastSourceValue)f.getStack(stackPos);
					for (int from: value.getInstructions()) {
						edges.add(new DataFlowEdge(from, instructionIndex, opIndex, operands, stackPos, false));
					}
				}
			} else if (referLocal(instructionIndex)) {
				AbstractInsnNode to = instructions.getItem(instructionIndex);
				int localIndex = OpcodeString.getVarIndex(to);
				if (f != null) {
					FastSourceValue value = (FastSourceValue)f.getLocal(localIndex);
					for (int from: value.getInstructions()) {
						edges.add(new DataFlowEdge(from, instructionIndex, 0, 1, localIndex, true));
					}
				} else {
					// A frame object is missing for several instructions in certain methods including many JSRs.
					// We skip the data-flow edges for the "unknown" sources.
					// edges.add(new DataFlowEdge(65536, instructionIndex, 0, 1, localIndex, true));
				}
			}
		}
		List<DataFlowEdge> sourceOrder = new ArrayList<DataFlowEdge>(edges.size());
		sourceOrder.addAll(edges);
		Collections.sort(sourceOrder, new DataFlowEdge.SourceComparator());
		dataFlowEdgesSourceOrder = sourceOrder;
		dataFlowEdges = edges;
	}
	
	/**
	 * Return a list of data-definition vertices for each operand used by the specified instruction.
	 * @param instructionIndex specifies an instruction using an operand stack.
	 * @return a two-dimensional array.
	 * array[operandIndex] indicates a list of instructions that defined the operand.
	 * The result is consistent with a return value of getEdges().
	 */
	public int[][] getDataDefinition(int instructionIndex) {
		if (useStack(instructionIndex)) {
			int operands =  analyzer.getOperandCount(instructionIndex);
			int[][] operandDef = new int[operands][];
			for (int i=0; i<operands; ++i) {
				Frame f = analyzer.getFrames()[instructionIndex];
				FastSourceValue value = (FastSourceValue)f.getStack(f.getStackSize() - operands + i);
				operandDef[i] = value.getInstructions();
			}
			return operandDef;
		} else {
			AbstractInsnNode to = instructions.getItem(instructionIndex);
			if (referLocal(instructionIndex)) {
				int localIndex = OpcodeString.getVarIndex(to);
				Frame f = analyzer.getFrames()[instructionIndex];
				if (f != null) {
					FastSourceValue value = (FastSourceValue)f.getLocal(localIndex);
					int[][] localDef = new int[1][];
					localDef[0] = value.getInstructions();
					return localDef;
				} else {
					// To avoid a problem caused by certain methods including many JSRs
					return new int[0][0];
				}
			}
		}
		return new int[0][];
	}
	
	/**
	 * Return a list of data flow edges.
	 * The edges are sorted by their destination instructions.
	 */
	public List<DataFlowEdge> getEdges() {
		computeDataFlowEdges();
		return dataFlowEdges;
	}
	
	/**
	 * Return a list of data flow edges.
	 * The edges are sorted by their source instructions.
	 */
	public List<DataFlowEdge> getEdgesInSourceOrder() {
		computeDataFlowEdges();
		return dataFlowEdgesSourceOrder;
	}

	/**
	 * Return an instruction index for the specified node.
	 */
	public int getInstructionIndex(AbstractInsnNode node) {
		return instructions.getId(node);
	}

	/**
	 * Return an instruction object.
	 */
	public AbstractInsnNode getInstruction(int index) {
		return instructions.getItem(index);
	}

	/**
	 * Return the number of operands used by a specified instruction.
	 * @param instructionIndex specifies an instruction.
	 * @return the number of operands.
	 * For an instruction that uses a local variable,
	 * 1 is returned.
	 */
	public int getOperandCount(int instructionIndex) {
		return analyzer.getOperandCount(instructionIndex);
	}
	
	/**
	 * Return a state of Frame at a specified instruction.
	 * Frame represents the current state of a operand stack and a local variable table.
	 * @param instructionIndex specifies an instruction.
	 * @return Frame object.  The return value may be null if 
	 * control-flow analysis somewhat failed. 
	 * (It is rarely occurs for certain methods.)
	 */
	public Frame getFrame(int instructionIndex) {
		return analyzer.getFrames()[instructionIndex];
	}
	
}
