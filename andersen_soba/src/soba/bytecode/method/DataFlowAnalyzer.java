package soba.bytecode.method;

import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.Frame;

import soba.util.IntPairList;

public class DataFlowAnalyzer extends Analyzer {

	private MethodNode method;
    private IntPairList controlFlow = new IntPairList(512);
    private IntPairList exceptionalFlow = new IntPairList(512);
    private DataFlowInterpreter interpreter;

	public DataFlowAnalyzer(DataFlowInterpreter interpreter) {
		super(interpreter);
		this.interpreter = interpreter;
	}
	
	@Override
	public Frame[] analyze(String owner, MethodNode m) throws AnalyzerException {
		this.method = m;
		return super.analyze(owner, m);
	}
	
	@Override
	protected void newControlFlowEdge(int insn, int successor) {
		controlFlow.add(insn, successor);
		super.newControlFlowEdge(insn, successor);
	}
	
	@Override
	protected boolean newControlFlowExceptionEdge(int insn, int successor) {
		exceptionalFlow.add(insn, successor);
		return super.newControlFlowExceptionEdge(insn, successor);
	}
	
	public MethodNode getAnalyzedMethod() {
		return method;
	}
	
	public IntPairList getNormalControlFlow() {
		return controlFlow;
	}
	
	public IntPairList getConservativeControlFlow() {
		IntPairList edges = new IntPairList(controlFlow.size() + exceptionalFlow.size());
		edges.addAll(controlFlow);
		edges.addAll(exceptionalFlow);
		return edges;
	}
	
	public int getOperandCount(int instructionIndex) {
		return interpreter.getOperandCount(instructionIndex);
	}
	
}
