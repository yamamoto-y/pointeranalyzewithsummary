package soba.bytecode.method;


import gnu.trove.list.array.TIntArrayList;
import gnu.trove.set.hash.TIntHashSet;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.Opcodes;

import soba.bytecode.MethodInfo;
import soba.model.FieldAccess;
import soba.model.IMethodBody;
import soba.model.Invocation;
import soba.util.ObjectIdMap;


public class MethodBody implements IMethodBody {

	private MethodInfo owner;
	private MethodNode method;
	private int[] lines;
	private int maxLine;
	private int minLine;
	private LocalVariableTable table;
	
	public MethodBody(MethodInfo owner, MethodNode m) {
		this.owner = owner;
		this.method = m;

		assert (method.access & Opcodes.ACC_ABSTRACT) == 0;
	}
	
	public MethodNode getMethodNode() {
		return method;
	}
	
	public int getInstructionCount() {
		return method.instructions.size();
	}
	
	private void computeMinMaxLine() {
		if (lines == null) {
			int[] lines = getLineNumbers();
			int max = Integer.MIN_VALUE;
			int min = Integer.MAX_VALUE;
			for (int i=0; i<lines.length; ++i) {
				if (max < lines[i]) max = lines[i];
				if (min > lines[i]) min = lines[i];
			}
			// Unknown methods are regarded as line 0
			if (max == Integer.MIN_VALUE) max = 0;
			if (min == Integer.MAX_VALUE) min = 0;
			this.maxLine = max;
			this.minLine = min;
		}
	}
	
	/**
	 * @return the maximum line including an instruction of the method.
	 * 0 indicates the method has no line number information.
	 */
	public int getMaxLine() {
		computeMinMaxLine();
		return maxLine;
	}
	
	/**
	 * @return the minimum line including an instruction of the method.
	 * 0 indicates the method has no line number information.
	 */
	public int getMinLine() {
		computeMinMaxLine();
		return minLine;
	}
	
	public int[] getLineNumbers() {
		if (lines == null) {
			TIntHashSet array = new TIntHashSet(method.instructions.size());
			for (int i=0; i<method.instructions.size(); ++i) {
				if (method.instructions.get(i).getType() == AbstractInsnNode.LINE) {
					LineNumberNode node = (LineNumberNode)method.instructions.get(i);
					array.add(node.line);
				}
			}
			lines = array.toArray();
		}
		return lines;
	}

	public int getLine(int instructionIndex) {
		for (int i=instructionIndex; i>=0; --i) {
			if (method.instructions.get(i).getType() == AbstractInsnNode.LINE) {
				return ((LineNumberNode)method.instructions.get(i)).line;
			}
		}
		return 0;
	}
	
	public int[] getInstructions(int line) {
		TIntArrayList lineInstructions = new TIntArrayList();
		boolean inside = false;
		for (int i=0; i<method.instructions.size(); ++i) {
			if (method.instructions.get(i).getType() == AbstractInsnNode.LINE) {
				inside = ((LineNumberNode)method.instructions.get(i)).line == line;
			}
			if (inside) lineInstructions.add(i);
		}
		return lineInstructions.toArray();
	}
	

	public List<Invocation> listMethodCalls() {
		List<Invocation> calls = new ArrayList<Invocation>(method.instructions.size());
		for (int i=0; i<method.instructions.size(); ++i) {
			if (method.instructions.get(i).getType() == AbstractInsnNode.METHOD_INSN) {
				MethodInsnNode m = (MethodInsnNode)method.instructions.get(i);
				Invocation invoke = new Invocation(m.owner, m.name, m.desc, getInvokeType(m));
				calls.add(invoke);
			}
		}
		return calls;
	}
	
	public List<CallSite> listCallSites() {
		List<CallSite> callsites = new ArrayList<CallSite>(method.instructions.size());
		for (int i=0; i<method.instructions.size(); ++i) {
			CallSite site = getCallSite(i);
			if (site != null) {
				callsites.add(site);
			}
		}
		return callsites;
	}
	
	public CallSite getCallSite(final int i) {
		if (method.instructions.get(i).getType() == AbstractInsnNode.METHOD_INSN) {
			MethodInsnNode m = (MethodInsnNode)method.instructions.get(i);
			Invocation invoke = new Invocation(m.owner, m.name, m.desc, getInvokeType(m));
			CallSite site = new CallSite(this.owner, i, invoke);
			return site;
		}
		return null;
	}
	
	private Invocation.Kind getInvokeType(MethodInsnNode m) {
		Invocation.Kind k = Invocation.Kind.VIRTUAL;
		if (m.getOpcode() == Opcodes.INVOKESTATIC) k = Invocation.Kind.STATIC;
		else if (m.getOpcode() == Opcodes.INVOKESPECIAL) k = Invocation.Kind.SPECIAL;
		return k;
	}
	
	public List<FieldAccess> listFieldAccesses() {
		List<FieldAccess> fields = new ArrayList<FieldAccess>(32);
		for (int i=0; i<method.instructions.size(); ++i) {
			if (method.instructions.get(i).getType() == AbstractInsnNode.FIELD_INSN) {
				FieldInsnNode f = (FieldInsnNode)method.instructions.get(i);
				
				switch (f.getOpcode()) {
				case Opcodes.PUTFIELD:
					fields.add(FieldAccess.createPutField(f.owner, f.name, f.desc, false));
					break;
				case Opcodes.PUTSTATIC:
					fields.add(FieldAccess.createPutField(f.owner, f.name, f.desc, true));
					break;
				case Opcodes.GETFIELD:
					fields.add(FieldAccess.createGetField(f.owner, f.name, f.desc, false));
					break;
				case Opcodes.GETSTATIC:
					fields.add(FieldAccess.createGetField(f.owner, f.name, f.desc, true));
					break;
				default:
					assert false: "Unknown Field Operation Found.";
				}
			}
		}
		return fields;
	}
	
	public LocalVariableTable getLocalVariables() {
		if (table == null) {
			table = new LocalVariableTable(owner, method);
		}
		return table;
	}
	
	public DataFlowInfo getDataFlow() {
		ObjectIdMap<AbstractInsnNode> instructions = new ObjectIdMap<AbstractInsnNode>(method.instructions.size());
		for (int i=0; i<method.instructions.size(); ++i) {
			instructions.add(method.instructions.get(i));
		}
		instructions.freeze();
		DataFlowInterpreter interpreter = new DataFlowInterpreter(instructions);
		DataFlowAnalyzer analyzer = new DataFlowAnalyzer(interpreter);
		try {
			analyzer.analyze(method.name, method);
			return new DataFlowInfo(instructions, analyzer, getLocalVariables());
		} catch (AnalyzerException e) {
			return null;
		}
	}
	
}
