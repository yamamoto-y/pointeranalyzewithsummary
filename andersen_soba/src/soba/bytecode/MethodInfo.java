package soba.bytecode;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.method.MethodBody;
import soba.bytecode.method.OpcodeString;
import soba.bytecode.signature.MethodSignatureReader;
import soba.model.IClassInfo;
import soba.model.IMethodInfo;




public class MethodInfo implements IMethodInfo {

	private String ownerPackageName;
	private String ownerClassName;
	private MethodNode method;

	private String returnType;
	private String[] paramTypes;
	private int[] paramIndex;
	private int paramCount;
	private boolean[] paramGeneric;
	private String[] opcodeStringCache;
	private MethodBody body;

	public MethodInfo(MethodNode method) {
		this.ownerPackageName = null;
		this.ownerClassName = null;
		this.method = method;
	}

	public MethodInfo(IClassInfo owner, MethodNode method) {
		this.ownerPackageName = owner.getPackageName();
		this.ownerClassName = owner.getClassName();
		this.method = method;
	}

	private void extractParametersIfNecessary() {
		if (paramTypes != null) return;

		MethodSignatureReader reader = new MethodSignatureReader(method.desc);
		int thisParam = isStatic() ? 0: 1;

		// read type names
		this.returnType = reader.getReturnType();
		this.paramCount = reader.getParamCount() + thisParam;
		String[] params = new String[this.paramCount];
		if (!isStatic()) {
			if (ownerClassName != null) {
				params[0] = ownerClassName;
			} else {
				params[0] = "Unknown Owner Class";
			}
		}
		for (int i=0; i<reader.getParamCount(); ++i) {
			params[i+thisParam] = NameCache.getSharedInstance(reader.getParamType(i));
		}

		// read generics flag
		paramGeneric = new boolean[this.paramCount];
		for (int i=0; i<reader.getParamCount(); ++i) {
			paramGeneric[i+thisParam] = reader.isGenericType(i);
		}

		// compute index for local variable table
		this.paramIndex = new int[params.length];
		int index = 0;
		for (int i=0; i<params.length; ++i) {
			paramIndex[i] = index;
			if (params[i].equals("double") || params[i].equals("long")) {
				// Double and Long are double word values.
				index += 2;
			} else {
				index += 1;
			}
		}

		// finished
		this.paramTypes = params;
	}

	/**
	 * @return the descriptor of the method.
	 */
	public String getDescriptor() {
		return method.desc;
	}

	/**
	 * @return a descriptor including generics information.
	 */
	public String getGenericsSignature() {
		return method.signature;
	}

	public boolean hasMethodBody() {
		int access = method.access;
		return (access & (Opcodes.ACC_ABSTRACT | Opcodes.ACC_NATIVE)) == 0;
	}

	public int getInstructionCount() {
		return method.instructions.size();
	}

	public String getMethodName() {
		return method.name;
	}

	/**
	 * @return a shorter string representation of the method signature.
	 */
	public String toString() {
		return method.name + method.desc;
	}


	/**
	 * @return a unique string that specify the method declaration.
	 */
	public String getMethodKey() {
		if (ownerClassName != null) {
			return ownerClassName + "#" + method.name + "#" + method.desc;
		} else {
			return "NOT-AVAILABLE#" + method.name + "#" + method.desc;
		}
	}

	/**
	 * @return the return value type.
	 * The method may return a generic type name such as "T".
	 */
	public String getReturnType() {
		extractParametersIfNecessary();
		return returnType;
	}

	public int getParamCount() {
		extractParametersIfNecessary();
		return paramCount;
	}

	/**
	 * This method translates "N-th" parameter to an index value for local variable table.
	 * This method is required because a double-word (long, double) variable
	 * occupies two words in a local variable table.
	 * @param paramIndex specifies the position of a parameter.
	 * E.g. 0, 1, 2, ... indicate the first, the second, the third, ... parameters.
	 * @return Index value to accesss local variable table.
	 */
	public int getParamIndex(int index) {
		return this.paramIndex[index];
	}

	/**
	 * @param index specifies the position of a parameter.
	 * @return a type name for the parameter.
	 * A class name is fully qualified.
	 * The name may be a generic type parameter such as "T".
	 * An inner class name is concatinated by ".".
	 * For example, "A.B" is returned for a type "A<T>.B".
	 */
	public String getParamType(int index) {
		extractParametersIfNecessary();
		if (index >= paramTypes.length) return null;
		return paramTypes[index];
	}

	public String getParamName(int index) {
		if (method.localVariables == null) return null;
		if (index >= method.localVariables.size()) return null;
		int paramIndex = getParamIndex(index);
		for (int i=0; i<method.localVariables.size(); ++i) {
			LocalVariableNode var = (LocalVariableNode)method.localVariables.get(i);
			if (var.index == paramIndex && var.start == method.instructions.getFirst()) {
				return var.name;
			}
		}
		return null;
	}

	public String toLongString() {
		StringBuilder name = new StringBuilder();
		if (ownerClassName != null) {
			name.append(ownerClassName);
			name.append(".");
		}
		name.append(getMethodName());
		name.append("(");
		for (int i=0; i<getParamCount(); ++i) {
			if (i>0) name.append(", ");
			name.append(getParamType(i));
			String paramName = getParamName(i);
			if (paramName != null) {
				name.append(":");
				name.append(paramName);
			}
		}
		name.append(")");
		name.append(": ");
		name.append(getReturnType());
		return name.toString();
	}

	/**
	 * @return the package name who has the method.
	 * This method may return null
	 * if the object is directly created without ClassInfo.
	 */
	public String getPackageName() {
		return ownerPackageName;
	}

	/**
	 * @return the class name who has the method.
	 * This method may return null
	 * if the object is directly created without ClassInfo.
	 */
	public String getClassName() {
		return ownerClassName;
	}

	public boolean isStatic() {
		return (method.access & Opcodes.ACC_STATIC) != 0;
	}

	public boolean isSynthetic() {
		return (method.access & Opcodes.ACC_SYNTHETIC) != 0;
	}

	/**
	 * @return true if the method may be overridden by a subclass.
	 * In other words, the method is a non-final, non-private instance method.
	 */
	public boolean isOverridable() {
		return (method.access & (Opcodes.ACC_PRIVATE | Opcodes.ACC_FINAL | Opcodes.ACC_STATIC)) == 0;
	}

	public boolean isPackagePrivate() {
		return (method.access & (Opcodes.ACC_PUBLIC | Opcodes.ACC_PRIVATE | Opcodes.ACC_PROTECTED)) == 0;
	}

	@Deprecated
	public MethodNode getMethodNode() {
		return method;
	}

	public MethodBody getMethodBody() {
		if (hasMethodBody()) {
			if (body == null) {
				body = new MethodBody(this, method);
			}
			return body;
		} else {
			return null;
		}
	}

	public String getInstructionString(int index) {
		if (opcodeStringCache == null) {
			this.opcodeStringCache = new String[method.instructions.size() + 1];
		}

		if (opcodeStringCache[index + 1] == null) {
			opcodeStringCache[index + 1] = OpcodeString.getInstructionString(method, index);
		}
		return opcodeStringCache[index + 1];
	}
}
