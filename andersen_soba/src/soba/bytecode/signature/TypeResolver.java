package soba.bytecode.signature;

import java.util.HashMap;

import org.objectweb.asm.signature.SignatureReader;


/**
 * A utility class to resolve a type name using TypeVisitor.
 * @author ishio
 *
 */
public class TypeResolver {

	private HashMap<String, String> types;

	/**
	 * Create a object to translate a type descriptor into a type name.
	 * This object contains a cache for type names.
	 * If you don't need to cache the result, 
	 * you may use a static method to resolve type name.
	 */
	public TypeResolver() {
		types = new HashMap<String, String>(4096);
	}
	
	/**
	 * Return a readable text for a specified type descriptor.
	 * @param typeDesc is a type descriptor of a single type.
	 * @return
	 */
	public String resolve(String typeDesc) {
		String typeName = types.get(typeDesc);
		if (typeName == null) {
			typeName = getTypeName(typeDesc);
			types.put(typeDesc, typeName);
		}
		return typeName;
	}
	
	/**
	 * Return a readable text for a specified type descriptor.
	 * @param typeDescriptor is a type descriptor of a single type.
	 * @return a type name corresponding to a specified descriptor.
	 */
	public static String getTypeName(String typeDesc) {
		if (typeDesc == null) return null;
		SignatureReader sig = new SignatureReader(typeDesc);
		TypeVisitor reader = new TypeVisitor();
		sig.acceptType(reader);
		return reader.getTypeName();
	}

}
