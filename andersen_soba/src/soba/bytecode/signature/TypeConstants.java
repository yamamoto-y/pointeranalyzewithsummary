package soba.bytecode.signature;

public class TypeConstants {

	public static final String BOOLEAN = "boolean";
	public static final String BYTE = "byte";
	public static final String CHAR = "char";
	public static final String SHORT = "short";
	public static final String INT = "int";
	public static final String LONG = "long";
	public static final String FLOAT = "float";
	public static final String DOUBLE = "double";
	public static final String VOID = "void";
	
	public static final String[] PRIMITIVE_TYPES = {
		BOOLEAN, BYTE, CHAR, SHORT, INT, LONG, FLOAT, DOUBLE 
	};

	/**
	 * @param name specifies a type name.
	 * @return true if a given name is a primitive types
	 * (excluding "void").
	 */
	public static boolean isPrimitiveTypeName(String name) { 
		for (int i=0; i<PRIMITIVE_TYPES.length; ++i) {
			if (PRIMITIVE_TYPES[i].equals(name)) return true;
		}
		return false;
	}
	
	public static boolean isPrimitiveOrVoid(String name) {
		return isPrimitiveTypeName(name) || VOID.equals(name);
	}
	
	/**
	 * @param name specifies a type.
	 * @return the number of words to store the type.
	 * 2 is returned for double and long.  
	 */
	public static int getWordCount(String name) {
		if (DOUBLE.equals(name) || LONG.equals(name)) {
			return 2;
		} else {
			return 1;
		}
	}
}
