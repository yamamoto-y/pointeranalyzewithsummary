package soba.bytecode;

import org.objectweb.asm.tree.FieldNode;

import soba.bytecode.signature.TypeResolver;
import soba.model.IFieldInfo;

public class FieldInfo implements IFieldInfo {

	private ClassInfo owner;
	private FieldNode fieldNode;
	private String typeName;
	
	public FieldInfo(ClassInfo owner, FieldNode fieldNode) {
		this.owner = owner;
		this.fieldNode = fieldNode;
		this.typeName = null;
	}
	
	public String getPackageName() {
		return owner.getPackageName();
	}
	
	public String getClassName() {
		return owner.getClassName();
	}
	
	public String getFieldName() {
		return fieldNode.name;
	}
	
	public String getDescriptor() {
		return fieldNode.desc;
	}
	
	public String getFieldTypeName() {
		if (typeName == null) {
			if (fieldNode.signature != null) {
				typeName = TypeResolver.getTypeName(fieldNode.signature);
			} else {
				typeName = TypeResolver.getTypeName(fieldNode.desc);
			}
		}
		return typeName;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClassName());
		builder.append(".");
		builder.append(getFieldName());
		builder.append(": ");
		builder.append(getFieldTypeName());
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fieldNode == null) ? 0 : fieldNode.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result
				+ ((typeName == null) ? 0 : typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FieldInfo other = (FieldInfo) obj;
		if (fieldNode == null) {
			if (other.fieldNode != null)
				return false;
		} else if (!fieldNode.equals(other.fieldNode))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		return true;
	}
	
	
}
