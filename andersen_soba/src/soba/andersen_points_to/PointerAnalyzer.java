package soba.andersen_points_to;

import gnu.trove.map.hash.TIntObjectHashMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import soba.andersen_points_to.graph.PointsToEdgeVisitor;
import soba.andersen_points_to.graph.PointsToGraph;
import soba.andersen_points_to.graph.node.ExternalPointerNode;
import soba.andersen_points_to.graph.node.FreshVarNode;
import soba.andersen_points_to.graph.node.LocalVarNode;
import soba.andersen_points_to.graph.node.PointerNode;
import soba.andersen_points_to.graph.node.ReturnValueNode;
import soba.andersen_points_to.graph.node.StaticFieldVarNode;
import soba.andersen_points_to.graph.node.VarNode;
import soba.andersen_points_to.transfer.TransferInst;
import soba.andersen_points_to.transfer.node.TransferArrayElementNode;
import soba.andersen_points_to.transfer.node.TransferEmptyNode;
import soba.andersen_points_to.transfer.node.TransferFreshVariableNode;
import soba.andersen_points_to.transfer.node.TransferInstanceFieldNode;
import soba.andersen_points_to.transfer.node.TransferInvokeNode;
import soba.andersen_points_to.transfer.node.TransferLocalVariableNode;
import soba.andersen_points_to.transfer.node.TransferNewNode;
import soba.andersen_points_to.transfer.node.TransferNode;
import soba.andersen_points_to.transfer.node.TransferOwnerNode;
import soba.andersen_points_to.transfer.node.TransferReturnValueNode;
import soba.andersen_points_to.transfer.node.TransferStaticFieldNode;
import soba.andersen_points_to.transfer.node.TransferVariableNode;
import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.bytecode.MethodInfo;
import soba.bytecode.method.CallSite;
import soba.bytecode.signature.MethodSignatureReader;
import soba.bytecode.signature.TypeConstants;
import soba.model.ClassHierarchy;
import soba.model.DynamicBindingResolver;
import soba.model.IFieldInfo;
import soba.model.IMethodInfo;
import soba.model.Invocation;
import soba.util.ObjectIdMap;

/**
 * This implementation is based on the algorithm in
 * "Points-to Analysis for Java Using Annotated Constraints", OOPSLA2001.<Br>
 * Analysis semantics is defined as following rules for adding new edges to points-to graphs.
 * <dl>
 * 	<dt>(1) f(G, l = new C) = G ∪ { f(l, oi ) }</dt>
 * 		<dd>l から，new したオブジェクトへ辺を引く．</dd>
 * 	<dt>(2) f(G, l = r) = G ∪ { (l, oi) | oi in Pt(G, r) }</dt>
 * 		<dd>l から，右辺が指すオブジェクト へ辺を引く．</dd>
 * 	<dt>(3) f(G, l.f = r) = G ∪ { (< oi, f >, oj ) | oi in Pt(G, l) ^ oj in Pt(G, r) }</dt>
 * 		<dd>l が指すオブジェクト から， r が指すオブジェクト へ f のラベル付き辺を引く．</dd>
 * 	<dt>(4) f(G, l = r.f) = G ∪{ (l, oi) | oj in Pt(G, r) ^ oi in Pt(G, < oj, f >) }</dt>
 * 		<dd>l から，r が指しているオブジェクトのフィールドが指すオブジェクトへ辺を引く．</dd>
 * 	<dt>(5) f(G, l = r0.m(r1,...,rn)) = G∪   {resolve (G,m, oi, r1,..., rn, l) | oi in Pt(G, r0) }<br>
 *    &emsp;  where<br>
 *    &emsp;&emsp;
 *    resolve (G,m, oi, r1,..., rn, l) = let mj(p0, p1,...,pn, ret j) = dispatch(oi,m)<br>
 *    &emsp;&emsp;
 *    in {(p0, oi)} ∪ f(G, p1 = r1) ∪  ... ∪ f(G, l = retj )</dt>
 * 		<dd>resolve はレシーバの型 oi と m のシグネチャから動的束縛を解決して，呼び出される候補 mj を孵す．
 * 		そして，mj の仮引数から，呼び出し元 m の実引数が指すオブジェクトへ辺を引き，l から mj の戻り値がさすオブジェクトへ辺を引く．</dd>
 * </dl>
 */
public class PointerAnalyzer
{
	final private JavaProgram program;
	final private DynamicBindingResolver dynamicBindingResolver;

	/**
	 * methodInfo -> method id
	 */
	final private ObjectIdMap<MethodInfo> methodIdMap = new ObjectIdMap<MethodInfo>();

	final private PointsToGraph pgraph = new PointsToGraph();
	/**
	 * all transfer instructions
	 */
	private List<TransferInst> allTransInstList = new ArrayList<TransferInst>();

	public PointerAnalyzer(JavaProgram program, DynamicBindingResolver dynamicBindingResolver)
	{
		this.program = program;
		this.dynamicBindingResolver = dynamicBindingResolver;
		analyzePointer();
	}


	private void analyzePointer()
	{
		// calculate allTransInstList
		calculateTransferInst();
		propagateNewPointers();
		// ポインタの変化があった変数について，不動点解析を行う．
		fixedPointAnalysis();
	}


	/**
	 * calculate allTransInstList, which represetns all transfer instructions.
	 */
	private void calculateTransferInst()
	{
		final ClassHierarchy ch =  program.getClassHierarchy();
		ExecutorService executorService = Executors.newFixedThreadPool(4);
		final ConcurrentLinkedQueue<MethodInfoAndTransfer> transfers = new ConcurrentLinkedQueue<MethodInfoAndTransfer>();
		//		ArrayList<MethodInfoAndTransfer> transfers = new ArrayList<MethodInfoAndTransfer>();

		for (ClassInfo c : program.getClasses()) {
			for (int i = 0; i < c.getMethodCount(); i++) {
				final MethodInfo methodInfo = c.getMethod(i);
				if (!methodInfo.hasMethodBody()) continue;

				executorService.execute(new Runnable() {
					@Override
					public void run() {
						TransferInstFactory fac = new TransferInstFactory(ch, methodInfo);
						List<TransferInst> trans = fac.getTransferInsts();
						transfers.add(new MethodInfoAndTransfer(methodInfo, trans));
					}
				});
			}
		}
		executorService.shutdown();

		// This varable is used only for printing instructions below.
		TIntObjectHashMap<List<TransferInst>> methodIDtoTransfersMap = new TIntObjectHashMap<>();
		try {
			if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS))
				executorService.shutdownNow();

			for (MethodInfoAndTransfer trans : transfers)
			{
				//getId(item) returns new IDs if a map gets new object.
				int methodID = methodIdMap.getId(trans.methodInfo);
				methodIDtoTransfersMap.put(methodID, trans.transferInstList);
				System.out.println(methodID + "\t" + trans.methodInfo.getClassName() + "#" + trans.methodInfo);
			}
		} catch (InterruptedException e) {e.printStackTrace();}

		for (List<TransferInst> trans : methodIDtoTransfersMap.valueCollection())
			allTransInstList.addAll(trans);

		//print all transfer instructions
		for (int methodId : methodIDtoTransfersMap.keySet().toArray())
		{
			System.out.println(methodId +" : "+ methodIdMap.getItem(methodId));
			List<TransferInst> list = methodIDtoTransfersMap.get(methodId);
			for (TransferInst inst : list)
			{
				System.out.print("\t");
				System.out.print(inst.instIndex + "|\t");
				System.out.print(inst.leftValue);
				System.out.print(" := ");
				System.out.println(inst.rightValue);
			}
		}
	}


	/**
	 * 	a tmporary inner class for parallel computation.
	 */
	private class MethodInfoAndTransfer
	{
		final MethodInfo methodInfo;
		final List<TransferInst> transferInstList;

		public MethodInfoAndTransfer(MethodInfo methodInfo,	List<TransferInst> transferInstList)
		{
			super();
			this.methodInfo = methodInfo;
			this.transferInstList = transferInstList;
		}
	}


	private void propagateNewPointers()
	{
		// This list is used for the fixed point iteration.
		ArrayList<TransferInst> list = new ArrayList<TransferInst>();
		for (TransferInst transInst : allTransInstList)
		{
			TransferNode left = transInst.leftValue;
			TransferNode right = transInst.rightValue;

			if (right == null || left == null) 	continue;
			if (right instanceof TransferNewNode)
			{
				rule1(transInst);// l = new C
			}
			else
			{
				list.add(transInst);
			}
		}
		allTransInstList = list;
	}


	/**
	 * Propagating pointer information until pointsToGraph is built.
	 * Apply rule 2-5 to transfer instructions until a points-to graph reaches fixedpoint.
	 */
	private void fixedPointAnalysis()
	{
		long start = System.currentTimeMillis();
		int count = 1;

		boolean changed = true;
		while (changed)
		{
			long start2 = System.currentTimeMillis();
			int before = pgraph.edgeSize();
			//			System.out.println("-----------------------------------");
			for (TransferInst transInst : allTransInstList)
			{
				//				System.out.println(transInst + ":");
				TransferNode left = transInst.leftValue;
				TransferNode right = transInst.rightValue;

				// arguments must be propagated to parameters even if left is empty.
				if (isInvokeNode(right))
				{
					rule5(transInst);// l = r.m();
				}

				// No propagation cannot exists if left is empty.
				if (left instanceof TransferEmptyNode) continue;

				if (isSingleVariable(left) && isSingleVariable(right))
				{
					rule2(transInst);// l = r
				}
				else if (isOwnerVariable(left))
				{
					rule3(transInst);// l.f = r , l[i] = r.
				}
				else if (isSingleVariable(left)	&&	isOwnerVariable(right))
				{
					rule4(transInst);// l = r.f or l = r[i]
				}
				else{ }
			}

			changed = (before != pgraph.edgeSize())? true : false;
			System.out.print("ITERATION COUNT : "+ count);
			System.out.print(", TIME : " + (System.currentTimeMillis() - start2) / 1000 +"s");
			System.out.println(", GRAPH SIZE : " + pgraph.edgeSize());
			count++;
		}

		System.out.println("TIME : " + (System.currentTimeMillis() - start) / 1000 +"s");

	}


	/**
	 *  (1) f(G, l = new C) = G ∪ { f(l, oi ) }<br>
	 * 		l から，new したオブジェクトへ辺を引く．
	 * @param transInst must correspond to [l = new C]
	 */
	private void rule1(TransferInst transInst)
	{
		assert (transInst.rightValue instanceof TransferNewNode);

		VarNode var = convertVariableNode(transInst.leftValue);
		PointerNode newPointer = convertPointerNode((TransferNewNode) transInst.rightValue);
		pgraph.addVPEdge(var, newPointer);
	}


	/**
	 * f(G, l = r) = G ∪ { (l, oi) | oi in Pt(G, r) }<br>
	 *  l が指すオブジェクト から， r が指すオブジェクト へ f のラベル付き辺を引く．
	 * @param transInst must correspond to [l = r]
	 */
	private void rule2(TransferInst transInst)
	{
		assert !(transInst.rightValue instanceof TransferNewNode);

		VarNode left = convertVariableNode(transInst.leftValue);
		VarNode right = convertVariableNode((TransferVariableNode) transInst.rightValue);
		pgraph.addVPEdges(left, pgraph.getPointersOf(right));
	}


	/**
	 *   f(G, l.f = r) = G ∪ { (< oi, f >, oj ) | oi in Pt(G, l) ^ oj in Pt(G, r) }<br>
	 *		l が指すオブジェクト から， r が指すオブジェクト へ f のラベル付き辺を引く．
	 * @param transInst must correspond to [l.f = r]
	 */
	private void rule3(TransferInst transInst)
	{
		assert transInst.leftValue instanceof TransferInstanceFieldNode
		||	   transInst.leftValue instanceof TransferArrayElementNode;
		assert !(transInst.rightValue instanceof TransferNewNode);

		TransferOwnerNode leftNode = (TransferOwnerNode) transInst.leftValue;
		VarNode owner = convertVariableNode(leftNode.getOwner());
		String label = leftNode.getChildName();
		VarNode right = convertVariableNode((TransferVariableNode) transInst.rightValue);

		for (PointerNode from : pgraph.getPointersOf(owner))
			for (PointerNode to : pgraph.getPointersOf(right))
				pgraph.addPPEdges(from, label , to);
	}


	/**
	 * f(G, l = r.f) = G ∪{ (l, oi) | oj in Pt(G, r) ^ oi in Pt(G, < oj, f >) }<br>
	 *  l から，r が指しているオブジェクトのフィールドが指すオブジェクトへ辺を引く．
	 * @param transInst must correspond to [l = r.f]
	 */
	private void rule4(TransferInst transInst)
	{
		assert transInst.rightValue instanceof TransferInstanceFieldNode
		||	   transInst.rightValue instanceof TransferArrayElementNode;

		VarNode left = convertVariableNode(transInst.leftValue);

		TransferOwnerNode rightNode = (TransferOwnerNode) transInst.rightValue;
		VarNode owner = convertVariableNode(rightNode.getOwner());
		String label = rightNode.getChildName();

		for (PointerNode ownerPointer : pgraph.getPointersOf(owner))
			pgraph.addVPEdges(left, pgraph.getChildPointersOf(ownerPointer, label));
	}


	/**
	 *  f(G, l = r0.m(r1,...,rn)) = G∪   {resolve (G,m, oi, r1,..., rn, l) | oi in Pt(G, r0) }<br>
	 *    &emsp;  where<br>
	 *    &emsp;&emsp;
	 *    resolve (G,m, oi, r1,..., rn, l) = let mj(p0, p1,...,pn, ret j) = dispatch(oi,m)<br>
	 *    &emsp;&emsp;
	 *    in {(p0, oi)} ∪ f(G, p1 = r1) ∪  ... ∪ f(G, l = retj )<br>
	 *    resolve はレシーバの型 oi と m のシグネチャから動的束縛を解決して，呼び出される候補 mj を孵す．<br>
	 *    そして，mj の仮引数から，呼び出し元 m の実引数が指すオブジェクトへ辺を引き，l から mj の戻り値がさすオブジェクトへ辺を引く．
	 * @param transInst must correspond to [l = r0.m(r1,...,rn))]
	 */
	private void rule5(TransferInst transInst)
	{
		boolean isOnlyExternalMethodCall = isOnlyExternalCandidate(transInst);
		if (!isOnlyExternalMethodCall)
		{
			resolveInternalCall(transInst);
		}
		else
		{
			resolveExternalCall(transInst);
		}
	}


	/**
	 * Add points-to graph edges related to internal method invocation.
	 * @param transInst must correspond to [l = r0.m(r1,...,rn))]
	 */
	private void resolveInternalCall(TransferInst transInst)
	{
		TransferInvokeNode invocationNode = (TransferInvokeNode)transInst.rightValue;
		List<MethodInfo> invokedCandidates = resolveInvokedMethods(invocationNode);

		for (MethodInfo invoked : invokedCandidates)
		{
			final int invokedID = methodIdMap.getId(invoked);

			// this variable is used for fill a gap between parameters and arguments indices.
			int argumentIndex = 0;

			// propagating pointers of arguments on callsite to parameters of invoked methods.
			for (int i = 0; i < invoked.getParamCount(); i++)
			{
				// skip primitive types since ther are not related to pointer propagations.
				String paramType = invoked.getParamType(i);
				if(TypeConstants.isPrimitiveTypeName(paramType)) continue;

				String paramName = invoked.getParamName(i);
				int entryIndex;
				// paramName == null if a invoked method is created by compiler, such as access#? methods.
				if (paramName != null)
					entryIndex = invoked.getMethodBody().getDataFlow().getLocalVariables().findEntryByName(paramName);
				else
					entryIndex = invoked.getParamIndex(i);

				TransferVariableNode arugumentVar = invocationNode.parameters.get(argumentIndex);
				argumentIndex++;
				VarNode argNode = convertVariableNode(arugumentVar);
				LocalVarNode argument = new LocalVarNode(invokedID, entryIndex, paramName);
				pgraph.addVPEdges(argument, pgraph.getPointersOf(argNode));
			}

			{   // propagate a return pointer of invoked method to left hand side l.
				ReturnValueNode varNode = new ReturnValueNode(invokedID);
				Collection<PointerNode> retPointers = pgraph.getPointersOf(varNode);
				VarNode var = convertVariableNode(transInst.leftValue);
				pgraph.addVPEdges(var, retPointers);
			}
		}
	}


	private List<MethodInfo> resolveInvokedMethods(TransferInvokeNode invocationNode)
	{
		final List<MethodInfo> invokedCandidates = new ArrayList<MethodInfo>();
		final Invocation invocation = invocationNode.callSite.getInvokedMethod();

		if (invocation.isStaticMethod())
		{   //without receiver object
			IMethodInfo[] invokedMethods = dynamicBindingResolver.resolveCall(invocation);
			for (IMethodInfo im : invokedMethods) {
				MethodInfo m = program.getMethodInfo(im);
				if (m != null)
					invokedCandidates.add(m);
			}
			return invokedCandidates;
		}
		else
		{   //with receiver object. resolve dispatch (oi, m)
			TransferVariableNode receiver = invocationNode.parameters.get(0);
			VarNode rnode = convertVariableNode(receiver);

			// collect possible types of the receiver from receiver variable pointers.
			Set<String> receiverTypes = new HashSet<String>();
			for (PointerNode pointerOfReceiver : pgraph.getPointersOf(rnode))
				receiverTypes.add(pointerOfReceiver.type);

			// subTypes of receicers are needed since this analysis dot't ccover downcast of receivers.
			Collection<String> subTypes = program.getClassHierarchy().getAllSubtypes(receiverTypes);
			// resolve possible invoked methods.
			for (String type : subTypes) {
				String name = invocation.getMethodName();
				String descriptor = invocation.getDescriptor();
				IMethodInfo im = dynamicBindingResolver.resolveSpecialCall(type, name, descriptor);

				// im == null when downcast of a receiver is necessary.
				if (im == null) continue;
				MethodInfo invoked = program.getMethodInfo(im);
				if (invoked != null)
					invokedCandidates.add(invoked);
			}
			return invokedCandidates;
		}
	}


	/**
	 * let l -> (external serial pointer)
	 * Add points-to graph edges related to external method invocation.
	 * @param transInst must correspond to [l = r0.m(r1,...,rn))] and m is external method.
	 */
	private void resolveExternalCall(TransferInst transInst)
	{
		TransferInvokeNode invocationNode = (TransferInvokeNode)transInst.rightValue;
		CallSite callsite = invocationNode.callSite;

		String returnType = getDeclarationReturnType(callsite);
		MethodInfo m = program.getMethodInfo(callsite.getOwnerMethod());
		int methodId = methodIdMap.getId(m);
		int index = callsite.getInstructionIndex();
		String invoked = callsite.getInvokedMethod().getMethodName();
		PointerNode pointer = new ExternalPointerNode(methodId, index, returnType, invoked);
		VarNode var = convertVariableNode(transInst.leftValue);
		pgraph.addVPEdge(var, pointer);
	}


	private String getDeclarationReturnType(CallSite callsite)
	{
		String desc = callsite.getInvokedMethod().getDescriptor();
		MethodSignatureReader sig = new MethodSignatureReader(desc);
		return sig.getReturnType();
	}


	public void forEachVPEdge(PointsToEdgeVisitor visitor)
	{
		pgraph.forVPEachEdge(visitor);
	}


	/**
	 * convert TransferNode variable into VarNode.
	 */
	private VarNode convertVariableNode(TransferVariableNode transNode)
	{
		VarNode var = null;
		if (transNode instanceof TransferLocalVariableNode)
		{
			TransferLocalVariableNode left = (TransferLocalVariableNode)transNode;
			int methodID = methodIdMap.getId(left.methodInfo);
			var = new LocalVarNode(methodID, left.entryIndex, left.variableName);
		}
		else if (transNode instanceof TransferFreshVariableNode)
		{
			TransferFreshVariableNode left = (TransferFreshVariableNode)transNode;
			int methodID = methodIdMap.getId(left.methodInfo);
			var = new FreshVarNode(methodID, left.serialID);
		}
		else if (transNode instanceof TransferStaticFieldNode)
		{
			TransferStaticFieldNode left = (TransferStaticFieldNode)transNode;
			var = new StaticFieldVarNode(left.fieldInfo);
		}
		else if (transNode instanceof TransferReturnValueNode)
		{
			TransferReturnValueNode left = (TransferReturnValueNode)transNode;
			var = new ReturnValueNode(methodIdMap.getId(left.methodInfo));
		}
		else if (transNode instanceof TransferEmptyNode)
		{
			// return null.
		}
		else
		{
			System.err.println("Cannot convert Variable Node:" + transNode);
		}

		return var;
	}


	/**
	 * convert TransferNode pointer into VarNode.
	 */
	private PointerNode convertPointerNode(TransferNewNode transNode)
	{
		PointerNode pointer = null;
		if(transNode instanceof TransferNewNode)
		{
			TransferNewNode newNode = (TransferNewNode)transNode;
			int mehotdId = methodIdMap.getId(newNode.methodInfo);
			int instIndex = newNode.instIndex;
			String type = newNode.typeName;
			pointer = new PointerNode(mehotdId, instIndex, type);
		}
		else
		{
			System.err.println("Cannot convert  Pointer Node:" + transNode);
		}
		return pointer;
	}


	/**
	 * CHAによる動的束縛解決候補が全て外部のメソッドならば真
	 * @param transInst must correspond to [l = r0.m(r1,...,rn))]
	 * @return
	 */
	private boolean isOnlyExternalCandidate(TransferInst transInst)
	{
		TransferInvokeNode invocationNode = (TransferInvokeNode)transInst.rightValue;
		Invocation invocation = invocationNode.callSite.getInvokedMethod();

		if(invocation.isStaticMethod())
		{
			IMethodInfo[] invokedMethods = dynamicBindingResolver.resolveCall(invocation);
			for (IMethodInfo im : invokedMethods)
			{
				MethodInfo m = program.getMethodInfo(im);
				if (m != null)
					return false;
			}
			return true;
		}
		else
		{   // invocation with a receiver object.
			String receiver   = invocation.getClassName();
			String methodName = invocation.getMethodName();
			String descriptor = invocation.getDescriptor();
			Collection<String> receivers = new ArrayList<String>();
			receivers.add(receiver);
			Collection<String> subtypes = program.getClassHierarchy().getAllSubtypes(receivers);
			for (String subtype : subtypes)
			{
				IMethodInfo m = dynamicBindingResolver.resolveSpecialCall(subtype, methodName, descriptor);
				if (m != null)
					return false;
			}
			return true;
		}
	}


	private static boolean isSingleVariable(TransferNode node)
	{
		return     node instanceof TransferLocalVariableNode
				|| node instanceof TransferFreshVariableNode
				|| node instanceof TransferStaticFieldNode
				|| node instanceof TransferReturnValueNode
				;
	}


	private static boolean isOwnerVariable(TransferNode node)
	{
		return     node instanceof TransferInstanceFieldNode
				|| node instanceof TransferArrayElementNode
				;
	}


	private static boolean isInvokeNode(TransferNode node)
	{
		return node instanceof TransferInvokeNode;
	}


	public static boolean equalsIFieldInfo(IFieldInfo a, IFieldInfo b)
	{
		return (   a.getClassName().equals(b.getClassName())
				&& a.getDescriptor().equals(b.getDescriptor())
				&& a.getFieldName().equals(b.getFieldName()));
	}
}

