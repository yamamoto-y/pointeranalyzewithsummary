package soba.andersen_points_to.transfer;

import soba.andersen_points_to.transfer.node.TransferNode;
import soba.andersen_points_to.transfer.node.TransferVariableNode;
import soba.bytecode.MethodInfo;

public class TransferInst {
	public final MethodInfo methodInfo;
	public final int instIndex;
	public final TransferVariableNode leftValue;
	public final TransferNode rightValue;

	public TransferInst(TransferVariableNode leftValue, TransferNode rightValue, MethodInfo methodInfo, int index) {
		super();
		this.leftValue = leftValue;
		this.rightValue = rightValue;
		this.instIndex = index;
		this.methodInfo = methodInfo;
	}
	@Override
	public String toString() {
		return  instIndex + "\t: " + leftValue + " := "+ rightValue ;
	}


}
