package soba.andersen_points_to.transfer;

import soba.bytecode.signature.TypeResolver;
import soba.model.IFieldInfo;


public class ExternalStaticFieldInfo implements IFieldInfo{
	public final String className;
	public final String fieldName;
	public final String desc;
	public final String typeName;
	public String packageName;

	public ExternalStaticFieldInfo(String className, String fieldName,String desc) {
		super();
		this.className = className;
		this.fieldName = fieldName;
		this.desc = desc;
		this.typeName = TypeResolver.getTypeName(desc);
	}

	@Override
	public String getPackageName() {
		return packageName;
	}

	@Override
	public String getClassName() {
		return className;
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public String getDescriptor() {
		return desc;
	}

	@Override
	public String getFieldTypeName() {
		return typeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result
				+ ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result
				+ ((packageName == null) ? 0 : packageName.hashCode());
		result = prime * result
				+ ((typeName == null) ? 0 : typeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExternalStaticFieldInfo other = (ExternalStaticFieldInfo) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (packageName == null) {
			if (other.packageName != null)
				return false;
		} else if (!packageName.equals(other.packageName))
			return false;
		if (typeName == null) {
			if (other.typeName != null)
				return false;
		} else if (!typeName.equals(other.typeName))
			return false;
		return true;
	}


}
