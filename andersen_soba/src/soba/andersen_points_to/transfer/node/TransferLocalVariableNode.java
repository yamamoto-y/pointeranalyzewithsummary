package soba.andersen_points_to.transfer.node;

import soba.bytecode.MethodInfo;
import soba.bytecode.method.LocalVariables;

public class TransferLocalVariableNode extends TransferScalarNode
{
	public final MethodInfo methodInfo;
	public final int entryIndex;
	public final String variableName;
	public final String typeName;

	public TransferLocalVariableNode(MethodInfo methodInfo, int entryIndex) {
		super();
		this.methodInfo = methodInfo;
		this.entryIndex = entryIndex;
		LocalVariables vars = methodInfo.getMethodBody().getDataFlow().getLocalVariables();
		this.variableName = vars.getVariableName(entryIndex);
		this.typeName = vars.getVariableType(entryIndex);
	}

	@Override
	public String toString() {
		return "LV:" + this.variableName + "("+  this.entryIndex + ")";
	}

	public String getType() {
		return typeName;
	}
}
