package soba.andersen_points_to.transfer.node;

import soba.bytecode.MethodInfo;

public class TransferReturnValueNode extends TransferVariableNode {
	public final MethodInfo methodInfo;

	public TransferReturnValueNode(MethodInfo methodInfo) {
		super();
		this.methodInfo = methodInfo;
	}

	@Override
	public String toString() {
		return "RETURN";
	}
}
