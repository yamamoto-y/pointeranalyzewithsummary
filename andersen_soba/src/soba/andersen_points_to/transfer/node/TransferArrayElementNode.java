package soba.andersen_points_to.transfer.node;


public class TransferArrayElementNode extends TransferOwnerNode {
	public final TransferVariableNode owner;

	public TransferArrayElementNode(TransferVariableNode owner)
	{
		super();
		this.owner = owner;
	}


	@Override
	public String toString()
	{
		return "ARR: " +  owner.toString();
	}


	@Override
	public TransferVariableNode getOwner()
	{
		return owner;
	}


	@Override
	public String getChildName()
	{
		return "123ARRAY";
	}
}
