package soba.andersen_points_to.transfer.node;

public abstract class TransferOwnerNode extends TransferVariableNode
{
	public abstract TransferVariableNode getOwner();
	public abstract String getChildName();
}
