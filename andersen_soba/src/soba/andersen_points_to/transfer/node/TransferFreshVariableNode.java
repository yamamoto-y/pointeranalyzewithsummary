package soba.andersen_points_to.transfer.node;

import soba.bytecode.MethodInfo;

public class TransferFreshVariableNode extends TransferVariableNode{
	public final String typeName;
	public final MethodInfo methodInfo;
	public final int serialID;
	public final int instIndex;

	public TransferFreshVariableNode(String typeName, MethodInfo methodInfo, int serialID, int instIndex) {
		this.typeName = typeName;
		this.methodInfo = methodInfo;
		this.serialID = serialID;
		this.instIndex = instIndex;
	}

	@Override
	public String toString() {
		return "FRV:" + serialID;
	}
}
