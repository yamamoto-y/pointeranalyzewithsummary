package soba.andersen_points_to.transfer.node;

public abstract class TransferScalarNode extends TransferVariableNode
{
	public abstract String getType();
}
