package soba.andersen_points_to.transfer.node;

import soba.model.IFieldInfo;


public class TransferInstanceFieldNode extends TransferOwnerNode{

	public TransferVariableNode fieldOwnerNode;
	public IFieldInfo fieldInfo;

	public TransferInstanceFieldNode(TransferVariableNode recieverNode,IFieldInfo fieldInfo)
	{
		super();
		this.fieldOwnerNode = recieverNode;
		this.fieldInfo = fieldInfo;
	}

	@Override
	public String toString()
	{
		return "FLD:" + fieldOwnerNode.toString() + "<:>" + fieldInfo.getFieldName();
	}

	@Override
	public TransferVariableNode getOwner()
	{
		return fieldOwnerNode;
	}

	@Override
	public String getChildName()
	{
		return fieldInfo.getFieldName();
	}

}
