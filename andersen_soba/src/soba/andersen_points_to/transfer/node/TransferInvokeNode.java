package soba.andersen_points_to.transfer.node;

import java.util.List;

import soba.bytecode.method.CallSite;

public class TransferInvokeNode extends TransferNode {
	public final List<TransferVariableNode> parameters;
	public final CallSite callSite;

	public TransferInvokeNode(List<TransferVariableNode> parameters, CallSite callSite) {
		this.parameters = parameters;
		this.callSite = callSite;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INV:" + callSite.getOwnerMethod().getMethodName()
				+ "#" + callSite.getInvokedMethod().getMethodName());
		sb.append("(");
		boolean isFirst = true;
		for (TransferNode p : parameters) {
			if (!isFirst) {
				sb.append(",");
			}
			isFirst = false;
			sb.append(p);
		}
		sb.append(")");
		return sb.toString();
	}
}
