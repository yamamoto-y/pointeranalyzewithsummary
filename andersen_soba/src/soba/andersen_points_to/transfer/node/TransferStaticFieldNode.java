package soba.andersen_points_to.transfer.node;

import soba.model.IFieldInfo;

public class TransferStaticFieldNode extends TransferScalarNode
{

	public final IFieldInfo fieldInfo;
	public TransferStaticFieldNode(IFieldInfo fieldInfo) {
		this.fieldInfo = fieldInfo;
	}

	@Override
	public String toString() {
		return "GV:" + fieldInfo.getClassName() + "::" + fieldInfo.getFieldName();
	}

	public String getType() {
		return fieldInfo.getFieldTypeName();
	}
}
