package soba.andersen_points_to.transfer.node;

import soba.bytecode.MethodInfo;

public class TransferNewNode extends TransferNode
{
	public final int instIndex;
	public final MethodInfo methodInfo;
	public final String typeName;

	public TransferNewNode(int instIndex, MethodInfo methodInfo, String typename) {
		super();
		this.instIndex = instIndex;
		this.methodInfo = methodInfo;
		this.typeName = typename;
	}

	public String toString() {
		return "NEW:" + this.instIndex;
	}

	public String getType() {
		return typeName;
	}
}
