package soba.andersen_points_to.transfer;

import soba.andersen_points_to.transfer.node.TransferFreshVariableNode;
import soba.bytecode.MethodInfo;
import soba.model.IFieldInfo;

public class TransferFreshVariableNodeFactory {
	private int freshVarserialID = 0;

	private int getNewFreshVariableSerialID () {
		freshVarserialID++;
		return freshVarserialID;
	}

	public TransferFreshVariableNode getNewFreshNode(String typeName, MethodInfo methodInfo, int instIndex) {
		int freshID = getNewFreshVariableSerialID();
		return new TransferFreshVariableNode(typeName, methodInfo, freshID, instIndex);
	}

	public TransferFreshVariableNode getNewFreshNode(IFieldInfo fieldInfo, MethodInfo methodInfo, int instIndex) {
		String typeName = fieldInfo.getFieldTypeName();
		return getNewFreshNode(typeName, methodInfo,instIndex);
	}
}
