package soba.andersen_points_to.debug;

import soba.andersen_points_to.PointerAnalyzer;
import soba.andersen_points_to.graph.PointsToEdgeVisitor;
import soba.andersen_points_to.graph.node.LocalVarNode;
import soba.andersen_points_to.graph.node.PointerNode;
import soba.andersen_points_to.graph.node.VarNode;
import soba.bytecode.JavaProgram;
import soba.model.DynamicBindingResolver;

public class DumpPointerAnalyzer {

	public static void main(String args[]) {
		final String path = (args.length > 0) ? args[0] : Constant.path;
		final JavaProgram program = DumpUtil.makeJavaProgram(path);
		final DynamicBindingResolver dynamicBindingResolver = new DynamicBindingResolver(program.getClassHierarchy());
		final PointerAnalyzer p = new PointerAnalyzer(program, dynamicBindingResolver);

		System.out.println();
		p.forEachVPEdge(new PointsToEdgeVisitor()
		{
			@Override
			public void onVPEdge(VarNode var, PointerNode pointer)
			{
				if(var instanceof LocalVarNode)
					System.out.println(var +" -> "+ pointer);
			}
		});
	}
}
