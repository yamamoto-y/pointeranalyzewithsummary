package soba.andersen_points_to.debug;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import soba.bytecode.JavaProgram;
import soba.util.files.Directory;
import soba.util.files.IFileEnumerator;
import soba.util.files.SingleFile;
import soba.util.files.ZipFile;

public class DumpUtil {

	public static JavaProgram makeJavaProgram(String path) {
		List<IFileEnumerator> files = new ArrayList<IFileEnumerator>();

		File f = new File(path);
		if (f.isDirectory())
		{
			Directory dir = new Directory(f);
			dir.enableRecursiveZipSearch();
			files.add(dir);
		}
		else if (ZipFile.isZipFile(f))
		{
			ZipFile zip = new ZipFile(f);
			zip.enableRecursiveSearch();
			files.add(zip);
		}
		else
		{
			SingleFile file = new SingleFile(f);
			files.add(file);
		}

		return new JavaProgram(files.toArray(new IFileEnumerator[files.size()]));
	}

}
