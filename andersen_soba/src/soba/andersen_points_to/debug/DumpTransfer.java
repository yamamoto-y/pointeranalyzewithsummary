package soba.andersen_points_to.debug;

import java.util.List;

import soba.andersen_points_to.TransferInstFactory;
import soba.andersen_points_to.transfer.TransferInst;
import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.bytecode.MethodInfo;
import soba.model.ClassHierarchy;


public class DumpTransfer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = Constant.path;
		if (args.length > 0) {
			path = args[0];
		}

		JavaProgram program = DumpUtil.makeJavaProgram(path);
		ClassHierarchy ch = program.getClassHierarchy();
//
//		ClassInfo c = program.getClassInfo("soba/example/andersen_points_to/SomeComplicateCase");
//		MethodInfo m = c.getMethodByName("main", "()V");
//

//
//		System.out.println("=== main ===");
//		dump(m, factory);
//
//		System.out.println("=== WR ===");
//		m = c.getMethodByName("WR", "()Lsoba/example/andersen_points_to/A;");
//		dump(m, factory);
//
//		System.out.println("=== list ===");
//		m = c.getMethodByName("listTest", "()V");
//		dump(m, factory);

		for (ClassInfo c : program.getClasses()) {
			for (int i = 0; i < c.getMethodCount(); i++) {
				MethodInfo m = c.getMethod(i);
				if (!m.hasMethodBody()) { continue; }

				System.out.println();
				System.out.println(m.getClassName() + "#" + m.getMethodName());
				TransferInstFactory factory = new TransferInstFactory(ch, m);

				List<TransferInst> list = factory.getTransferInsts();
				printEdges(list);
			}
		}
	}

	private static void printEdges(List<TransferInst> list) {
		for (TransferInst edge : list) {
			System.out.print(edge.instIndex + "|");
			System.out.print(edge.leftValue);
			System.out.print(" := ");
			System.out.println(edge.rightValue);
		}
	}

}
