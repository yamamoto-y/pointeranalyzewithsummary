package soba.andersen_points_to;

import gnu.trove.list.array.TIntArrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TypeInsnNode;

import soba.andersen_points_to.transfer.ExternalStaticFieldInfo;
import soba.andersen_points_to.transfer.TransferFreshVariableNodeFactory;
import soba.andersen_points_to.transfer.TransferInst;
import soba.andersen_points_to.transfer.node.TransferArrayElementNode;
import soba.andersen_points_to.transfer.node.TransferEmptyNode;
import soba.andersen_points_to.transfer.node.TransferFreshVariableNode;
import soba.andersen_points_to.transfer.node.TransferInstanceFieldNode;
import soba.andersen_points_to.transfer.node.TransferInvokeNode;
import soba.andersen_points_to.transfer.node.TransferLocalVariableNode;
import soba.andersen_points_to.transfer.node.TransferNewNode;
import soba.andersen_points_to.transfer.node.TransferNode;
import soba.andersen_points_to.transfer.node.TransferReturnValueNode;
import soba.andersen_points_to.transfer.node.TransferScalarNode;
import soba.andersen_points_to.transfer.node.TransferStaticFieldNode;
import soba.andersen_points_to.transfer.node.TransferVariableNode;
import soba.bytecode.MethodInfo;
import soba.bytecode.method.CallSite;
import soba.bytecode.method.DataFlowEdge;
import soba.bytecode.method.DataFlowInfo;
import soba.bytecode.method.LocalVariables;
import soba.bytecode.signature.MethodSignatureReader;
import soba.bytecode.signature.TypeConstants;
import soba.bytecode.signature.TypeResolver;
import soba.model.ClassHierarchy;
import soba.model.DynamicBindingResolver;
import soba.model.IClassInfo;
import soba.model.IFieldInfo;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import com.sun.org.apache.bcel.internal.generic.AASTORE;
import com.sun.org.apache.bcel.internal.generic.ALOAD;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import com.sun.org.apache.bcel.internal.generic.ARETURN;
import com.sun.org.apache.bcel.internal.generic.ASTORE;
import com.sun.org.apache.bcel.internal.generic.CHECKCAST;
import com.sun.org.apache.bcel.internal.generic.GETFIELD;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;
import com.sun.org.apache.bcel.internal.generic.INVOKEINTERFACE;
import com.sun.org.apache.bcel.internal.generic.INVOKESPECIAL;
import com.sun.org.apache.bcel.internal.generic.INVOKESTATIC;
import com.sun.org.apache.bcel.internal.generic.INVOKEVIRTUAL;
import com.sun.org.apache.bcel.internal.generic.LDC;
import com.sun.org.apache.bcel.internal.generic.MULTIANEWARRAY;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;
import com.sun.org.apache.bcel.internal.generic.PUTFIELD;
import com.sun.org.apache.bcel.internal.generic.PUTSTATIC;



/**
 *
 * This class convert java byte code into transfer instructions
 * , which is a intermediate representation for object transfers.
 * This class covers following jvm operations, related to *object* transfers.<br>
 *
 *
 * These operations make a empty as a result. <br>
 *  ( e.g. operation stack [a,b,c] => [] )<br>
 * {@link PUTSTATIC}
 * {@link ASTORE}
 * {@link ARETURN}
 * {@link PUTFIELD}
 * {@link AASTORE}
 *
 *
 * These operations make a scalar value as a result.<br>
 *  ( e.g. operation stack [a,b,c] => [value] )<br>
 * {@link AALOAD}
 * {@link GETSTATIC}
 *
 *
 * These operations make a scalar value or objects as a result.<br>
 *  ( e.g. operation stack [a,b,c] => [object] )<br>
 * {@link ALOAD}
 * {@link GETFIELD}
 * {@link CHECKCAST}
 * {@link MULTIANEWARRAY}
 * {@link NEW}
 * {@link NEWARRAY}
 * {@link ANEWARRAY}
 * {@link LDC}
 * {@link INVOKEINTERFACE}
 * {@link INVOKESPECIAL}
 * {@link INVOKESTATIC}
 * {@link INVOKEVIRTUAL}
 *
 */
public class TransferInstFactory
{

	private  final DynamicBindingResolver resolver;
	private  final ClassHierarchy hierarchy;
	private  final TransferFreshVariableNodeFactory freshFac;
	private  final MethodInfo methodInfo;
	private  final DataFlowInfo dataFlowInfo;
	private  final InsnList instructions;

	public TransferInstFactory(ClassHierarchy hierarchy, MethodInfo methodInfo) {
		this.resolver = new DynamicBindingResolver(hierarchy);
		this.hierarchy = hierarchy;
		this.freshFac = new TransferFreshVariableNodeFactory();
		this.methodInfo = methodInfo;
		this.dataFlowInfo = methodInfo.getMethodBody().getDataFlow();
		this.instructions = methodInfo.getMethodBody().getMethodNode().instructions;
	}


	private List<TransferInst> transInstList;
	/**
	 * @return list of transfer instructions of the method.
	 */
	public List<TransferInst> getTransferInsts()
	{
		if(!methodInfo.hasMethodBody()) return Collections.emptyList();
		if(transInstList == null)
		{
			transInstList = new ArrayList<TransferInst>();
			computeTransferInsts();
		}
		return transInstList;
	}

	private void addInst(TransferVariableNode left, TransferNode right, int index)
	{
		TransferInst inst = new TransferInst(left, right, methodInfo, index);
		if(left != null && right != null)
		{
//			System.out.println("\t\tADD: " + inst);
			transInstList.add(inst);
		}
	}

	private void computeTransferInsts()
	{
		// compute for all object transfer instructions whose result of operand stack is *empty*.
		for (int i = 0; i < instructions.size(); i++)
		{
			final int index = i;
			final AbstractInsnNode abstractNode = instructions.get(index);

			// System.out.println("\t"+OpcodeString.getInstructionString(methodInfo.getMethodNode(), index));
			switch (abstractNode.getOpcode())
			{
			case Opcodes.ASTORE:
			{
				// [v] => []
				// create "local var <- v"

				int srcIndex = getSourceIndex(index);
				TransferNode right = getNode(srcIndex);

				int dstEntryIndex = dataFlowInfo.getLocalVariables().findEntryForInstruction(index);
				TransferVariableNode leftVar = new TransferLocalVariableNode(methodInfo, dstEntryIndex);

				addInst(leftVar, right, index);
				break;
			}
			case Opcodes.ARETURN:
			{
				// [v] => []
				// create "RET <- v" (v is Scalar or not Scalar)

				int srcIndex = getSourceIndex(index);
				TransferNode right = getNode(srcIndex);

				TransferVariableNode leftVar = new TransferReturnValueNode(methodInfo);

				addInst(leftVar, right, index);
				break;
			}
			case Opcodes.PUTSTATIC:
			{
				// [v] => []
				// create "A.f <- v"

				// ignore transfering primitive type.
				IFieldInfo fieldInfo = getIFieldInfo((FieldInsnNode)abstractNode);
				if (!isNotPrimitiveField(fieldInfo)) continue;

				int srcIndex = getSourceIndex(index);
				TransferNode right = getNode(srcIndex);

				TransferVariableNode leftVar = new TransferStaticFieldNode(fieldInfo);

				addInst(leftVar, right, index);
				break;
			}
			case Opcodes.PUTFIELD:
			{
				// [a,f,v] => []
				// create "a.f <- v" (v is Scalar or not Scalar)

				// ignore transfering primitive type.
				IFieldInfo fieldInfo = getIFieldInfo((FieldInsnNode)abstractNode);
				if (!isNotPrimitiveField(fieldInfo)) continue;

				int srcIndex = getStoredValueIndex(index).getSourceInstruction();
				TransferNode right = getNode(srcIndex);

				int ownerIndex = getOwnerIndex(index);
				TransferVariableNode ownerNode = getNodeAsFreshNode(ownerIndex);
				TransferVariableNode leftVar = new TransferInstanceFieldNode(ownerNode, fieldInfo);

				addInst(leftVar, right, index);
				break;
			}
			case Opcodes.AASTORE:
			{
				// [a,i,v] => []
				// create "a[i] <- v" (v is Scalar or not Scalar)

				int srcIndex = getStoredValueIndex(index).getSourceInstruction();
				TransferNode right = getNode(srcIndex);

				int ownerIndex = getOwnerIndex(index);
				TransferVariableNode ownerNode = getNodeAsFreshNode(ownerIndex);
				TransferVariableNode leftVar = new TransferArrayElementNode(ownerNode) ;

				addInst(leftVar, right, index);
				break;
			}
			case Opcodes.INVOKEVIRTUAL:
			case Opcodes.INVOKESPECIAL:
			case Opcodes.INVOKESTATIC:
			case Opcodes.INVOKEDYNAMIC:
			case Opcodes.INVOKEINTERFACE:
			{
				// [o,m,a1,a2,...] => []
				// create "EMPTY <- o.m(a1,a2,...)"
				// primitive type artuments will be removed.

				CallSite callSite = methodInfo.getMethodBody().getCallSite(index);
				String desc = callSite.getInvokedMethod().getDescriptor();
				MethodSignatureReader sig = new MethodSignatureReader(desc);

				if (TypeConstants.isPrimitiveOrVoid(sig.getReturnType()))
				{
					TransferFreshVariableNode node = getNodeAsFreshNode(index);
					assert node == null; // because created node must be EmptyNode.
				}
				else if (isNotUsedInFuture(index))
				{
					TransferFreshVariableNode node = getNodeAsFreshNode(index);
					assert node == null;
				}
				break;
			}
			default:
				break;
			}
		}
	}


	/**
	 * create a TransferNode, which is a scalar value or an object.
	 * If the node is not scalar, creating new fresh nodes and edges until reaching scalar instructions.
	 * @return created node
	 */
	private TransferNode getNode(int index)
	{
		if(index < 0) return null;// for data flow edge from -1.
		AbstractInsnNode insnNode = instructions.get(index);

		assert !isEmptyResultInsn(insnNode.getOpcode());

		if (insnNode.getOpcode() == Opcodes.CHECKCAST)
		{
			// {@link CHECKCAST}
			int srcIndex = getSourceIndex(index);
			return getNode(srcIndex);
		}
		else if (isScalarLoadInsn(insnNode.getOpcode()))
		{
			// {@link ALOAD}
			// {@link GETSTATIC}
			return getScalarValueNode(index);
		}
		else if (isNewInsn(insnNode.getOpcode()))
		{
			// {@link MULTIANEWARRAY}
			// {@link NEW}
			// {@link NEWARRAY}
			// {@link ANEWARRAY}
			// {@link LDC}
			return getNodeAsFreshNode(index);
		}
		else if (insnNode.getOpcode() == Opcodes.GETFIELD)
		{
			return getNodeAsFreshNode(index);
		}
		else if(insnNode.getOpcode() == Opcodes.AALOAD)
		{
			return getNodeAsFreshNode(index);
		}
		else if (isInvokeInsn(insnNode.getOpcode()))
		{
			// {@link INVOKEINTERFACE}
			// {@link INVOKESPECIAL}
			// {@link INVOKESTATIC}
			// {@link INVOKEVIRTUAL}
			return getNodeAsFreshNode(index);
		}
		return null;
	}


	/**
	 * @param index
	 * @return new created {@link TransferNewNode} or {@link TransferEmptyNode}.
	 * And add new Transfer Instruction to a given list.
	 * Fresh variables are created when a result operand stack of a instruction is not empty.
	 * Fresh variables are mainly used for passing arguments to method calls.
	 * Return <code>null</code> if a type of node is primitive.
	 */
	private TransferFreshVariableNode getNodeAsFreshNode(int index)
	{
		if(index < 0) return null; // for data flow edge from -1.
		AbstractInsnNode insnNode = instructions.get(index);

		if (insnNode.getOpcode() == Opcodes.GETFIELD)
		{
			// {@link GETFIELD}
			// [o.f] => [value] （o is not scalar)
			// create "FR <- o.f" (o is Fresh)

			IFieldInfo fieldInfo = getIFieldInfo((FieldInsnNode)insnNode);
			if (!isNotPrimitiveField(fieldInfo)) return null;

			int srcIndex = getSourceIndex(index);
			// create fresh node recursively.
			TransferVariableNode freshOwner = getNodeAsFreshNode(srcIndex);
			TransferInstanceFieldNode right = new TransferInstanceFieldNode(freshOwner, fieldInfo);

			TransferFreshVariableNode freshLeft = freshFac.getNewFreshNode(fieldInfo, methodInfo, index);

			addInst(freshLeft, right, index);
			return freshLeft;
		}
		else if (insnNode.getOpcode() == Opcodes.AALOAD)
		{
			// {@link AALOAD}
			// [a,i] => [value] (a is not scalar)
			// create "FR <- a[i]" (a is Fresh)

			int srcInstIndex = getOwnerIndex(index);
			TransferVariableNode ownerNode = getNodeAsFreshNode(srcInstIndex);
			TransferArrayElementNode right = new TransferArrayElementNode(ownerNode);

			String type = getTypeNameOfVariable(index);
			TransferFreshVariableNode freshLeft = freshFac.getNewFreshNode(type, methodInfo, index);

			addInst(freshLeft, right, index);
			return freshLeft;
		}
		else if (isScalarLoadInsn(insnNode.getOpcode()))
		{
			// {@link ALOAD}, {@link GETSTATIC}
			// [] => [a]
			// create "FR <- a"

			TransferScalarNode right = getScalarValueNode(index);
			// if an index is primitive type
			if (right == null)	return null;

			String type = right.getType();
			TransferFreshVariableNode freshLeft = freshFac.getNewFreshNode(type, methodInfo, index);

			addInst(freshLeft, right, index);
			return freshLeft;
		}
		else if (isNewInsn(insnNode.getOpcode()))
		{
			// NEW*
			// [] => [a]
			// create "FR <- a"

			TransferNewNode right = getNewNode(index);

			String type = right.typeName;
			TransferFreshVariableNode freshLeft = freshFac.getNewFreshNode(type, methodInfo, index);

			addInst(freshLeft, right, index);
			return freshLeft;
		}
		else if (isInvokeInsn(insnNode.getOpcode()))
		{
			// INVOKE*
			// [o,m,a1,a2,...] => [(return value)]
			// create "FR <- o.m(a1,a2,...)"

			List<TransferVariableNode> parameters = new ArrayList<>();
			for (int src : getSourceindices(index))
			{
				TransferFreshVariableNode node = getNodeAsFreshNode(src);
				// if a node is not primitive.
				if(node != null)
					parameters.add(node);
			}

			CallSite callSite = methodInfo.getMethodBody().getCallSite(index);
			TransferInvokeNode right = new TransferInvokeNode(parameters, callSite);

			String desc = callSite.getInvokedMethod().getDescriptor();
			MethodSignatureReader sig = new MethodSignatureReader(desc);

			TransferVariableNode freshLeft = null;
			if (TypeConstants.isPrimitiveOrVoid(sig.getReturnType()))
				freshLeft = new TransferEmptyNode();
			else if (isNotUsedInFuture(index))
				freshLeft = new TransferEmptyNode();
			else
				freshLeft = freshFac.getNewFreshNode(sig.getReturnType(), methodInfo, index);

			addInst(freshLeft, right, index);
			if (freshLeft instanceof TransferEmptyNode)
				return null;
			else
				return (TransferFreshVariableNode)freshLeft;
		}
		else if (insnNode.getOpcode() == Opcodes.CHECKCAST)
		{
			// {@link CHECKCAST}
			int srcIndex = getSourceIndex(index);
			return getNodeAsFreshNode(srcIndex);
		}
		else
		{
			// primitivee load instructions such as ILOAD.
			// System.err.println(OpcodeString.getInstructionString(methodInfo.getMethodNode(), index));
			return null;
		}
	}


	/**
	 * @param index
	 * @return {@link TransferNewNode}
	 * according to given index and return return <code>null</code> if index is illegal.
	 */
	private TransferNewNode getNewNode(int index)
	{
		if(index < 0) return null;
		AbstractInsnNode insnNode = instructions.get(index);
		assert isNewInsn(insnNode.getOpcode());

		String typeName = null;
		if (       insnNode.getOpcode() == Opcodes.NEW
				|| insnNode.getOpcode() == Opcodes.NEWARRAY
				|| insnNode.getOpcode() == Opcodes.ANEWARRAY
				|| insnNode.getOpcode() == Opcodes.MULTIANEWARRAY)
		{
			typeName = getTypeNameOfNew(insnNode);
		}
		else if (insnNode.getOpcode() == Opcodes.ACONST_NULL)
		{
			typeName = "CONST_NULL";
		}
		else
		{   // only when Opcodes.LDC
			typeName = "STRING_LITERAL";
		}

		return new TransferNewNode(index, methodInfo, typeName);
	}


	/**
	 * @param index
	 * @return {@link TransferLocalVariableNode} or {@link TransferStaticFieldNode}
	 *  according to given index and return return <code>null</code> if index is primitive.
	 */
	private TransferScalarNode getScalarValueNode(int index)
	{
		if(index < 0) return null;
		AbstractInsnNode insnNode = instructions.get(index);
		assert     insnNode.getOpcode() == Opcodes.ALOAD
				|| insnNode.getOpcode() == Opcodes.GETSTATIC;

		if (insnNode.getOpcode() == Opcodes.ALOAD)
		{
			int entryIndex = dataFlowInfo.getLocalVariables().findEntryForInstruction(index);
			return new TransferLocalVariableNode(methodInfo, entryIndex);
		}
		else if (insnNode.getOpcode() == Opcodes.GETSTATIC)
		{
			FieldInsnNode fieldInsnNode = (FieldInsnNode)insnNode;
			IFieldInfo fieldInfo = getIFieldInfo(fieldInsnNode);
			if(isNotPrimitiveField(fieldInfo))
				return new TransferStaticFieldNode(fieldInfo);
			return null;
		}
		return null;
	}


	private String getTypeNameOfNew(AbstractInsnNode insnNode)
	{
		assert isNewInsn(insnNode.getOpcode());

		if (insnNode.getOpcode() == Opcodes.NEW)
		{
			return ((TypeInsnNode)insnNode).desc;
		}
		else if (insnNode.getOpcode() == Opcodes.NEWARRAY)
		{
			return "PRIMITIVE[]";
		}
		else if (insnNode.getOpcode() == Opcodes.ANEWARRAY)
		{
			String desc = ((TypeInsnNode)insnNode).desc;
			return desc + "[]";
		}
		else if (insnNode.getOpcode() == Opcodes.MULTIANEWARRAY)
		{
			String desc = ((MultiANewArrayInsnNode)insnNode).desc;
			return TypeResolver.getTypeName(desc);
		}
		return "UNKNOWN-NEW";
	}


	private String getTypeNameOfVariable(int index)
	{
		AbstractInsnNode insnNode = instructions.get(index);
		assert insnNode.getOpcode() == Opcodes.AALOAD;

		LocalVariables vars = dataFlowInfo.getLocalVariables();
		int entryIndex = vars.findEntryForInstruction(index);

		return (entryIndex == -1) ? "IN-ARRAY-OBJECT" : vars.getVariableType(entryIndex);
	}


	private IFieldInfo getIFieldInfo(FieldInsnNode node)
	{
		String className = node.owner;
		String fieldName = node.name;
		String desc = node.desc;

		String owner = null;
		if (isStaticFieldInsn(node.getOpcode()))
			owner = resolver.resolveStaticFieldOwner(className, fieldName, desc);
		else
			owner = resolver.resolveInstanceFieldOwner(className, fieldName, desc);

		IClassInfo c = hierarchy.getClassInfo(owner);
		return (c != null) ? c.getFieldByName(fieldName, desc)
				: new ExternalStaticFieldInfo(className, fieldName, desc);
	}


	/**
	 * calculate stored object of AASTORE, PUTFIELD comes from.
	 * @param index is instruction index
	 * @return a instruction index from which *stored data* comes.
	 */
	private DataFlowEdge getStoredValueIndex(int index)
	{
		AbstractInsnNode insnNode = instructions.get(index);
		assert     insnNode.getOpcode() == Opcodes.AASTORE
				|| insnNode.getOpcode() == Opcodes.PUTFIELD;

		for (DataFlowEdge edge : dataFlowInfo.getEdges())
		{
			int top = edge.getDestinationOperandCount() - 1;
			if (       edge.getDestinationInstruction() == index
					&& edge.getDestinationOperandIndex()== top)
			{
				return edge;
			}
		}
		return null;
	}


	/**
	 * return data flow edges to a given instruction index.
	 */
	private int[] getSourceindices(int index)
	{
		TIntArrayList ret = new TIntArrayList();
		for (DataFlowEdge edge : dataFlowInfo.getEdges())
		{
			if (edge.getDestinationInstruction() == index)
				ret.add(edge.getSourceInstruction());
		}
		return ret.toArray();
	}


	/**
	 * @return -1 if a target is not found.
	 */
	private int getSourceIndex(int index)
	{
		for (DataFlowEdge edge : dataFlowInfo.getEdges())
		{
			if (edge.getDestinationInstruction() == index)
				return edge.getSourceInstruction();
		}
		return -1;
	}


	/**
	 * @return -1 if a target is not found.
	 */
	private boolean isNotUsedInFuture(int index)
	{
		for (DataFlowEdge edge : dataFlowInfo.getEdges())
		{
			if (edge.getSourceInstruction() == index)
				return false;
		}
		return true;
	}


	/**
	 * calculate owner object of AASTORE, PUTFIELD comes from.
	 * @param index is instruction index
	 * @return a instruction index from which *owner* comes.
	 */
	private int getOwnerIndex(int index)
	{
		AbstractInsnNode insnNode = instructions.get(index);
		assert     insnNode.getOpcode() == Opcodes.AASTORE
				|| insnNode.getOpcode() == Opcodes.AALOAD
				|| insnNode.getOpcode() == Opcodes.PUTFIELD;

		for (DataFlowEdge edge : dataFlowInfo.getEdges())
		{
			// 0 (buttom) is a receiver object.
			if (       edge.getDestinationInstruction() == index
					&& edge.getDestinationOperandIndex() == 0) {
				return edge.getSourceInstruction();
			}
		}
		return -1;
	}


	private static boolean isNotPrimitiveField(IFieldInfo f)
	{
		return ! TypeConstants.isPrimitiveTypeName(f.getFieldTypeName());
	}


	private static boolean isNewInsn(int opcode)
	{
		return  	opcode == Opcodes.NEW
				||	opcode == Opcodes.NEWARRAY
				||	opcode == Opcodes.MULTIANEWARRAY
				||	opcode == Opcodes.ANEWARRAY
				||  opcode == Opcodes.LDC
				||  opcode == Opcodes.ACONST_NULL
				;
	}


	/**
	 * @return true if a opcode represents Scalar value.
	 * "Scalar" is a single value, not compounds such as a[], a.f.
	 */
	private static boolean isScalarLoadInsn(int opcode)
	{
		return  	opcode == Opcodes.ALOAD
				||	opcode == Opcodes.GETSTATIC; // may be primitive types.
	}


	private static boolean isInvokeInsn(int opcode)
	{
		return  	opcode == Opcodes.INVOKEVIRTUAL
				||	opcode == Opcodes.INVOKESPECIAL
				||	opcode == Opcodes.INVOKESTATIC
				||	opcode == Opcodes.INVOKEDYNAMIC
				||  opcode == Opcodes.INVOKEINTERFACE ;
	}


	private static boolean isStaticFieldInsn(int opcode)
	{
		return      opcode == Opcodes.GETSTATIC
				||	opcode == Opcodes.PUTSTATIC;
	}

	/**
	 *  @return true if a operation stack will be empty.
	 */
	private static boolean isEmptyResultInsn(int opcode)
	{
		return  	opcode == Opcodes.PUTSTATIC
				||	opcode == Opcodes.PUTFIELD
				||	opcode == Opcodes.ASTORE
				||  opcode == Opcodes.AASTORE
				||	opcode == Opcodes.ARETURN;
	}

}
