package soba.andersen_points_to.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import soba.andersen_points_to.graph.node.ExternalPointerNode;
import soba.andersen_points_to.graph.node.FreshVarNode;
import soba.andersen_points_to.graph.node.LocalVarNode;
import soba.andersen_points_to.graph.node.PointerNode;
import soba.andersen_points_to.graph.node.ReturnValueNode;
import soba.andersen_points_to.graph.node.StaticFieldVarNode;
import soba.andersen_points_to.graph.node.VarNode;

public class PointsToGraph {
	private final HashMap<VarNode, List<PointerNode>> vpEdges = new HashMap<>();

	/**
	 * :: (Owner object, lable) -> [child object]
	 * e.g. "a[i] = b" is expressed as a edge "p(a) -> p(b)"
	 */
	private final HashMap<PointerLabel, List<PointerNode>> ppEdges = new HashMap<>();
	//	private final HashSet<VPEdge> addedEdges = new HashSet<>();

	public void addVPEdge(VarNode from, PointerNode to)
	{
		if (from == null) return;
		//		VPEdge added = new VPEdge(from, to);
		//		if(addedEdges.contains(added)) return;
		//		addedEdges.add(added);

		List<PointerNode> pointers = vpEdges.get(from);
		if(pointers == null) pointers = new ArrayList<PointerNode>();

		if(!pointers.contains(to))
		{
			pointers.add(to);
			vpEdges.put(from, pointers);
			//					System.out.println("VPEDGE:" + from +" -> " + pointers);
		}
	}

	public void addVPEdges(VarNode from, Collection<PointerNode> to)
	{
		for(PointerNode p : to)
			addVPEdge(from, p);
	}


	public void addPPEdges(PointerNode from, String label, PointerNode to)
	{
		if (from == null) return;

		List<PointerNode> pointers = ppEdges.get(from);
		if(pointers == null) pointers = new ArrayList<PointerNode>();

		if(!pointers.contains(to))
		{
			pointers.add(to);
			PointerLabel pl = new PointerLabel(from, label);
			ppEdges.put(pl, pointers);
			//			System.out.println("PPEDGE:" + pl +" -> " + pointers);
		}
	}


	public void forVPEachEdge(PointsToEdgeVisitor visitor)
	{
		for (VPEdge e : sortedEdges())
			visitor.onVPEdge(e.v, e.p);
	}

	private ArrayList<VPEdge> sortedEdges()
	{
		ArrayList<VPEdge> list = new ArrayList<VPEdge>();
		for (VarNode var : vpEdges.keySet())
			for(PointerNode p : vpEdges.get(var))
				list.add(new VPEdge(var, p));

		Collections.sort(list);
		return list;
	}


	public List<PointerNode> getPointersOf(VarNode node)
	{
		List<PointerNode> ps = vpEdges.get(node);
		// copy a instance.
		return (ps != null)?
				new ArrayList<PointerNode>(ps)
				: Collections.<PointerNode>emptyList();
	}


	public List<PointerNode> getChildPointersOf(PointerNode node, String label)
	{
		if(node instanceof ExternalPointerNode)
		{   // if the owner is external node, child node is also external.
			ArrayList<PointerNode> list = new ArrayList<PointerNode>();
			list.add(node);
			return list;
		}

		PointerLabel pl = new PointerLabel(node, label);
		List<PointerNode> ps = ppEdges.get(pl);
		// copy a instance.
		return (ps != null)?
				new ArrayList<PointerNode>(ps)
				: Collections.<PointerNode>emptyList();
	}


	public int edgeSize()
	{
		return vpEdges.size();
	}


	private class PointerLabel
	{
		final private PointerNode owner;
		final private String label;

		public PointerLabel(PointerNode owner, String label)
		{
			super();
			this.owner = owner;
			this.label = label;
		}

		@Override
		public String toString()
		{
			return "<" + owner + ", " + label + ">";
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((label == null) ? 0 : label.hashCode());
			result = prime * result + ((owner == null) ? 0 : owner.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PointerLabel other = (PointerLabel) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (label == null)
			{
				if (other.label != null)
					return false;
			}
			else if (!label.equals(other.label))
				return false;
			if (owner == null)
			{
				if (other.owner != null)
					return false;
			}
			else if (!owner.equals(other.owner))
				return false;
			return true;
		}

		private PointsToGraph getOuterType()
		{
			return PointsToGraph.this;
		}

	}

	private class VPEdge implements Comparable<VPEdge>
	{
		private static final int oneMethodSpace = 3000;

		public final VarNode v;
		public final PointerNode p;

		public VPEdge(VarNode var, PointerNode p)
		{
			super();
			this.v = var;
			this.p = p;
		}

		private int score()
		{
			if(v instanceof StaticFieldVarNode)
			{
				return 0;
			}
			else if(v instanceof FreshVarNode)
			{
				FreshVarNode n = (FreshVarNode) v;
				return n.methodID * oneMethodSpace + n.serialID;
			}
			else if(v instanceof LocalVarNode)
			{
				LocalVarNode n = (LocalVarNode) v;
				return n.methodID * oneMethodSpace + (oneMethodSpace/2) + n.entryIndex;
			}
			else if(v instanceof ReturnValueNode)
			{
				ReturnValueNode n =  (ReturnValueNode) v;
				return (n.methodID + 1 ) * oneMethodSpace - 1;
			}
			return 0;
		}

		@Override
		public int compareTo(VPEdge o)
		{
			return score() - o.score();
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + ((v == null) ? 0 : v.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VPEdge other = (VPEdge) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (p == null)
			{
				if (other.p != null)
					return false;
			}
			else if (!p.equals(other.p))
				return false;
			if (v == null)
			{
				if (other.v != null)
					return false;
			}
			else if (!v.equals(other.v))
				return false;
			return true;
		}

		private PointsToGraph getOuterType()
		{
			return PointsToGraph.this;
		}
	}

}

