package soba.andersen_points_to.graph.node;

import soba.model.IFieldInfo;

public class StaticFieldVarNode extends VarNode{
	public final IFieldInfo fieldInfo;

	public StaticFieldVarNode(IFieldInfo fieldInfo) {
		super();
		this.fieldInfo = fieldInfo;
	}

	public String toString() {
		return "SF :" + fieldInfo.getClassName() + "#" + fieldInfo.getFieldName();
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fieldInfo == null) ? 0 : fieldInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StaticFieldVarNode other = (StaticFieldVarNode) obj;
		if (fieldInfo == null)
		{
			if (other.fieldInfo != null)
				return false;
		}
		else if (!fieldInfo.equals(other.fieldInfo))
			return false;
		return true;
	}


}
