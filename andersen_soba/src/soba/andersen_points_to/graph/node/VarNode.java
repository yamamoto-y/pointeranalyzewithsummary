package soba.andersen_points_to.graph.node;

public abstract class VarNode extends PointsToNode{
	abstract public int hashCode();
	abstract public boolean equals(Object obj);
}
