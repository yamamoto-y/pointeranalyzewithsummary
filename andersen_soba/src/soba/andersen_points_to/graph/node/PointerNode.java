package soba.andersen_points_to.graph.node;


public class PointerNode extends PointsToNode{
	public final int methodID;
	public final int insnIndex;
	public final String type;

	public PointerNode(int methodID, int insnIndex, String type) {
		super();
		this.methodID = methodID;
		this.insnIndex = insnIndex;
		this.type = type;
	}

	public String toString() {
		return "OBJ:" + methodID + ":" + insnIndex + "(" + type + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + insnIndex;
		result = prime * result + methodID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointerNode other = (PointerNode) obj;
		if (insnIndex != other.insnIndex)
			return false;
		if (methodID != other.methodID)
			return false;
		return true;
	}


}
