package soba.andersen_points_to.graph.node;


public class ExternalPointerNode extends PointerNode
{
	final public String invokedName;

	public ExternalPointerNode(int methodID, int insnIndex, String type, String invokedName)
	{
		super(methodID, insnIndex, type);
		this.invokedName = invokedName;
	}

	@Override
	public String toString()
	{
		return "EXT:" + methodID + ":" + insnIndex + "(" + type + ") from " + invokedName;
	}
}
