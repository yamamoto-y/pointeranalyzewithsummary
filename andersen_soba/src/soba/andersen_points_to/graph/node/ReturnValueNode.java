package soba.andersen_points_to.graph.node;

public class ReturnValueNode extends VarNode{
	public final int methodID;

	public ReturnValueNode(int methodID) {
		super();
		this.methodID = methodID;
	}

	public String toString() {
		return "RET:" + methodID;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + methodID;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReturnValueNode other = (ReturnValueNode) obj;
		if (methodID != other.methodID)
			return false;
		return true;
	}

}
