package soba.andersen_points_to.graph.node;

public class LocalVarNode extends VarNode{
	public final int methodID;
	public final int entryIndex;
	public final String variableName;

	public LocalVarNode(int methodID, int entryIndex, String variableName) {
		super();
		this.methodID = methodID;
		this.entryIndex = entryIndex;
		this.variableName = variableName;
	}

	public String toString() {
		return "LV :" + methodID + ":" + entryIndex +"("+ variableName +")";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + entryIndex;
		result = prime * result + methodID;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocalVarNode other = (LocalVarNode) obj;
		if (entryIndex != other.entryIndex)
			return false;
		if (methodID != other.methodID)
			return false;
		return true;
	}

}
