package soba.andersen_points_to.graph.node;

public class FreshVarNode extends VarNode{
	public final int methodID;
	public final int serialID;

	public FreshVarNode(int methodID, int freshID) {
		super();
		this.methodID = methodID;
		this.serialID = freshID;
	}

	public String toString() {
		return "FRV:" + methodID + ":" + serialID;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + methodID;
		result = prime * result + serialID;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FreshVarNode other = (FreshVarNode) obj;
		if (methodID != other.methodID)
			return false;
		if (serialID != other.serialID)
			return false;
		return true;
	}

}
