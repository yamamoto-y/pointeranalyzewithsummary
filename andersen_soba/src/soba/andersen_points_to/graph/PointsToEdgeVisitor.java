package soba.andersen_points_to.graph;

import soba.andersen_points_to.graph.node.PointerNode;
import soba.andersen_points_to.graph.node.VarNode;

public interface PointsToEdgeVisitor
{
	public void onVPEdge(VarNode var, PointerNode pointer);
}
