package soba.andersen_points_to.graph;

public class ExternalMethodInfo {
	public final String className;
	public final String methodName;
	public final String descriptor;

	public ExternalMethodInfo(String className, String methodName, String descriptor) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.descriptor = descriptor;
	}

}
