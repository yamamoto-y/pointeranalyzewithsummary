package soba.andersen_points_to.graph;

import java.util.HashMap;
import java.util.Map;

import soba.bytecode.method.CallSite;

public class IMethodInfoIDMap {
	Map<String, Integer> map1 = new HashMap<String, Integer>();
	Map<Integer, ExternalMethodInfo> map2 = new HashMap<Integer, ExternalMethodInfo>();
	private static int max_id = -1;
	
	public ExternalMethodInfo getItem(final int id) {
		if (map2.containsKey(id)) {
			return map2.get(id);
		} else {
			return null;
		}
	}
	
	public int getId(String className, String methodName, String desc) {
		final String key = className + "#" + methodName + desc;
//		System.err.println(key);
		if (map1.containsKey(key)) {
			return map1.get(key);
		} else {
			max_id++;
			map1.put(key, max_id);
			map2.put(max_id, new ExternalMethodInfo(className, methodName, desc));
			return max_id;
		}
	}
	
	public int getId(CallSite callsite) {
		return getId(
				callsite.getInvokedMethod().getClassName(), 
				callsite.getInvokedMethod().getMethodName(), 
				callsite.getInvokedMethod().getDescriptor()
				);
	}
}
