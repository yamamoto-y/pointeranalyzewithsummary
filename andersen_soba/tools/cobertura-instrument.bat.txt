@echo off
REM Generate "instrumented" binary files.
REM Please ensure COBERTURA_HOME points to the directory including cobertura batch files.
SET COBERTURA_HOME=..\cobertura-1.9.4.1
cd %~dp0
mkdir instrumented
del cobertura.ser
CALL %COBERTURA_HOME%\cobertura-instrument.bat --destination instrumented ..\bin
cp cobertura.ser ..
pause
