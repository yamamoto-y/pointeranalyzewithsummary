package soba.example.memberlist;

import java.io.File;
import java.util.List;


import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.model.ClassHierarchy;
import soba.model.DynamicBindingResolver;
import soba.model.FieldAccess;
import soba.model.IMethodInfo;
import soba.model.Invocation;
import soba.util.files.Directory;
import soba.util.files.IFileEnumerator;
import soba.util.files.ZipFile;

/**
 * Print out method names in the "bin" directory.
 * If you executes this program on Eclipse, 
 * "bin" direcotry contains the binary of the SOBA library.
 */
public class MemberListMain {

	private static final String INDENT = "    ";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Directory dir = new Directory(new File("bin"));
		ZipFile asm = new ZipFile(new File("lib/asm-all-4.0.jar"));  
		JavaProgram program = new JavaProgram(new IFileEnumerator[]{ dir, asm });
		ClassHierarchy ch = program.getClassHierarchy();
		for (ClassInfo c: program.getClasses()) {
			System.out.println(c.getClassName());
			for (int i=0; i<c.getMethodCount(); ++i) {
				IMethodInfo m = c.getMethod(i);
				System.out.println(INDENT + m.toLongString());
				if (!m.isStatic()) {
					IMethodInfo[] resolved = DynamicBindingResolver.resolveDynamicCall(ch, c.getClassName(), m.getMethodName(), m.getDescriptor());
					for (IMethodInfo called: resolved) {
						if (called != m) {
							System.out.println(INDENT + INDENT + "(overridden by) " + called.toLongString());
						}
					}
				}
				if (m.hasMethodBody()) {
					// list up invocations in the method
					List<Invocation> invocations = m.getMethodBody().listMethodCalls();
					for (Invocation invocation: invocations) {
						// print an invocation
						System.out.println(INDENT + INDENT + "(calls) " + invocation.toString());
						// print its dynamic binding
						IMethodInfo[] resolved = DynamicBindingResolver.resolveCall(ch, invocation);
						for (int resolvedIndex=0; resolvedIndex<resolved.length; ++resolvedIndex) {
							System.out.println(INDENT + INDENT + INDENT + "(binding) " + resolved[resolvedIndex].toLongString());
						}
					}
					// list up fields
					List<FieldAccess> accesses = m.getMethodBody().listFieldAccesses();
					for (FieldAccess access: accesses) {
						if (access.isGet()) {
							System.out.println(INDENT + INDENT + "(reads) " + access.toString());
						} else {
							System.out.println(INDENT + INDENT + "(writes) " + access.toString());
						}
					}

				}
			}
		}
	}

}
