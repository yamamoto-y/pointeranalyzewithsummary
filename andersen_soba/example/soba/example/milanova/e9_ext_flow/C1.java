package soba.example.milanova.e9_ext_flow;

import java.util.ArrayList;


public class C1 {
	public static void main()
	{
		ArrayList<A> list = new ArrayList<A>();
		A aa = new A();
		aa.set("hogehoge");
		list.add(aa);
		for (A a : list.toArray(new A[0]))
		{
			String name = a.f;
			System.out.println(name);
		}

		aa = list.get(0);
		A d = aa;
		System.out.println(d);

		Mylist mylist = new Mylist();
		mylist.add("a");
		String string = mylist.get(0);
		System.out.println(string);
	}
}

class A
{
	String f;

	void set(String string)
	{
		f = string;
	}
}

class Mylist extends ArrayList<String>
{
	String f;

	public boolean add(String str)
	{
		f = str;
		return true;
	}

	@Override
	public String get(int i)
	{
		return f;
	}
}