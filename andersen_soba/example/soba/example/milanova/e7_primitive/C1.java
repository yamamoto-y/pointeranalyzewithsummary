package soba.example.milanova.e7_primitive;

public class C1 {
	public static void main() {
		int[] int_array = new int[100];
		int_array[0] = 1;
		int i = int_array[0];
		int[] alias = int_array;
		System.out.println(i);
		System.out.println(alias);

		boolean[] bool_array = new boolean[100];
		boolean[] a = bool_array;
		System.out.println(a);
 	}
}
