package soba.example.milanova.e5;

public class C1 {
	public static void main() {
		C2 c21 = new C2();
		C3 c31 = c21.get();
		System.out.println(c31);
 	}
	
	
}

class C2 {
	C3 get() {
		return new C3();
	}
}

class C3 {
	
}
