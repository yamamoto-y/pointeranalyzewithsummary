package soba.example.milanova.e1;

public class C1 {
	public static void main() {
		C2 c21 = new C2();
		
		C2 c22 = new C2();
		
		C3 c31 = c21.get();
		
		C3 c32 = c22.get();
		
		
		C3 c33 = c21.id(c31);
		C3 c34 = c22.id(c32);
		
		System.out.println(c33);
		System.out.println(c34);
 	}
	
	
}

class C2 {
	C3 get() {
		return new C3();
	}
	
	C3 id(C3 c3) {
		return c3;
	}
}

class C3 {
	
}
