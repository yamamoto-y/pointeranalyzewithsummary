package soba.example.milanova.e6_array;

public class C1 {
	public static void main() {
		String[] arr = new String[1];
		arr[0] = new String("aaa");
		String s1 = arr[0];
		System.out.println(s1);

		String[] alias = new String[1];
		alias[0] = arr[0];
		System.out.println(alias);
 	}
}

class C2 {

}

class C3 {

}
