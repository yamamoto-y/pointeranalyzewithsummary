package soba.example.milanova.e8_array_field;


public class C1 {
	public static void main()
	{
		A a = new A();
		a.setString(0, "aaa");
		String[] strings = a.getString();

		for(String s : strings)
			System.out.println(s);
 	}
}

class A
{
	private String[] f = new String[1];

	String[] getString()
	{
		return f;
	}

	void setString(int i, String str)
	{
		f[i] = str;
		f[i] = "aaa";
	}
}
