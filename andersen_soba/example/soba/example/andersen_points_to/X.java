package soba.example.andersen_points_to;

/**
 * Sample Program of "Parameterized Object Sensitivity for Points to Analysis for Java"
 * In Fig.1 (page 3)
 * @author y-kasima
 *
 */
public class X {
	Y f;
	void set(Y r) {
		this.f = r;
	}
	
	static void main() {
		X p = new X();
		Y q = new Y();
		p.set(q);
	}
}

class Y {
	// ...
}



/*
 * Answer.
 * p -> o1
 * this -> o1
 * o1 --f--> o2
 * q -> o2
 * r -> o2
 */