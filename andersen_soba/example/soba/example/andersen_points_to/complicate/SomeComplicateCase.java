package soba.example.andersen_points_to.complicate;

import java.util.ArrayList;
import java.util.List;





public class SomeComplicateCase {
	static String get(String s) {
		return s;
	}
	
	static void main() {
		String s1 = "";
		String s2 = get(s1);
		
		A a = new A();
		B b = new B();
		C c = new C();
		B b1 = new B();
		C c1 = new C();
		F f1 = new F();
		A a1 = a;
		Globals.gv = a1;
		a.f = new A().f;
		a.f = f1;
		a.f = b.g;
		a.f.h = b.g;
		
		a.f.bf = a.k().p(b1, c1);
		
		a.k();
		
		f1 = a.f;
	}
	
	A WR() {
		A[] a = new A[0];
		a[0] = new A();
		
		A[][] multa = new A[0][0];
		multa[0][0] = new A();
		
		return multa[0][0];
	}
	
	void listTest() {
		F f = new F();
		f.list = new ArrayList<Object>();
		f.list.add(new Object());
		Object o = f.list.get(0);
	}
}

class Globals {
	public static A gv;
}



class F {
	F h;
	B bf;
	List<Object> list;
}

class A {
	F f;
	A k() {
		return new A();
	}
	
	B p(B b1, C c1) {
		return b1;
	}
}

class B {
	F g;
}

class C {
	A m(){
		return new A();
	}
}