package soba.example.andersen_points_to.simple2;

public class X2 {
 void n() { }
}

class Y extends X2 { void n() { } }

class Z extends X2 { void n() { } }

class A {
	X2 f;
	A (X2 xa) { this.f = xa; }
}

class B extends A {
	B (X2 xb) {
		super(xb);
	}
	
	void m() {
		X2 xb = this.f;
		xb.n();
	}
}

class C extends A {
	C(X2 xc) { super (xc); }
	void m() {
		X2 xc = this.f;
		xc.n(); 
	}
}

class Main {
	public static void main() {
		Y y = new Y();
		Z z = new Z();
		B b = new B(y);
		C c = new C(z);
		b.m();
		c.m();
	}
}