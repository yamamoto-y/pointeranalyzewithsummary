package soba.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soba.model.ClassHierarchy.FrozenHierarchyException;

public class ClassHierarchyTest {

	private ClassHierarchy hierarchy;
	
	// Three classes: pkg.C, pkg.D, pkg.E
	// pkg.D extends pkg.C
	
	private IClassInfo c = new ClassInfoString("pkg", "pkg/C", null, null);
	private IClassInfo d = new ClassInfoString("pkg", "pkg/D", "pkg/C", null);
	private IClassInfo e = new ClassInfoString("pkg", "pkg/E", null, new String[] {"pkg/I" });
	private IClassInfo f = new ClassInfoString("pkg2", "pkg2/F", "pkg/C", null);
	private IClassInfo g = new ClassInfoString("pkg", "pkg/G", "pkg/D", null);
	private IClassInfo h = new ClassInfoString("pkg", "pkg/H", "pkg/I", null);
	
	@Before
	public void createHierarchy() {
		hierarchy = new ClassHierarchy();
		hierarchy.registerClass(c);
		hierarchy.registerClass(d);
		hierarchy.registerClass(e);
		hierarchy.registerClass(f);
		hierarchy.registerClass(g);
		hierarchy.registerClass(h);
	}
	
	@Test
	public void testClasses() {
		Assert.assertEquals(6, hierarchy.getClassCount());
		String[] array = new String[hierarchy.getClassCount()];
		int idx = 0;
		for (String className: hierarchy.getClasses()) {
			array[idx] = className;
			idx++;
		}
		Arrays.sort(array);
		Assert.assertArrayEquals(new String[] {"pkg/C", "pkg/D", "pkg/E", "pkg/G", "pkg/H", "pkg2/F"}, array);
	}
	
	@Test
	public void testGetClassInfo() { 
		Assert.assertEquals(c, hierarchy.getClassInfo("pkg/C"));
		Assert.assertEquals(d, hierarchy.getClassInfo("pkg/D"));
		Assert.assertEquals(e, hierarchy.getClassInfo("pkg/E"));
		Assert.assertNull(hierarchy.getClassInfo("pkg/Unknown"));
	}
	
	@Test
	public void testGetSuperClass() {
		Assert.assertEquals("pkg/C", hierarchy.getSuperClass("pkg/D"));
		Assert.assertNull(hierarchy.getSuperClass("pkg/C"));
		Assert.assertNull(hierarchy.getSuperClass("pkg/E"));
		Assert.assertEquals("java/lang/Object", hierarchy.getSuperClass("pkg/C[]"));
		Assert.assertEquals(null, hierarchy.getSuperClass("pkg/Unknown"));
	}
	
	@Test
	public void testIsSamePackage() {
		Assert.assertTrue(hierarchy.isSamePackage("pkg/C", "pkg/D"));
		Assert.assertFalse(hierarchy.isSamePackage("pkg/C", "pkg2/F"));
		Assert.assertFalse(hierarchy.isSamePackage("pkg/C", "pkg/Unknown"));
		Assert.assertFalse(hierarchy.isSamePackage("pkg/Unknown", "pkg/D"));
	}
	
	@Test
	public void testGetSubtypes() {
		Collection<String> subtypesOfC = hierarchy.getSubtypes("pkg/C");
		Assert.assertEquals(2, subtypesOfC.size());
		Assert.assertTrue(subtypesOfC.contains("pkg/D"));
		Assert.assertTrue(subtypesOfC.contains("pkg2/F"));
		Collection<String> subtypesOfE = hierarchy.getSubtypes("pkg/E");
		Assert.assertEquals(0, subtypesOfE.size());
		Collection<String> subtypesOfI = hierarchy.getSubtypes("pkg/I");
		Assert.assertEquals(2, subtypesOfI.size());
		Assert.assertTrue(subtypesOfI.contains("pkg/E"));
		Assert.assertTrue(subtypesOfI.contains("pkg/H"));
	}
	
	@Test
	public void testListAllSuperTypes() {
		Collection<String> supertypesOfG = hierarchy.listAllSuperTypes("pkg/G");
		Assert.assertEquals(2, supertypesOfG.size());
		Assert.assertTrue(supertypesOfG.contains("pkg/D"));
		Assert.assertTrue(supertypesOfG.contains("pkg/C"));

		Collection<String> supertypesOfE = hierarchy.listAllSuperTypes("pkg/E");
		Assert.assertEquals(1, supertypesOfE.size());
		Assert.assertTrue(supertypesOfE.contains("pkg/I"));
	}
	
	@Test
	public void testGetSuperInterfaces() {
		Collection<String> superOfE = hierarchy.getSuperInterfaces("pkg/E");
		Assert.assertEquals(1, superOfE.size());
		Assert.assertTrue(superOfE.contains("pkg/I"));
		Collection<String> superOfH = hierarchy.getSuperInterfaces("pkg/H");
		Assert.assertEquals(0, superOfH.size());
	}
	
	@Test
	public void testFreeze() {
		Assert.assertFalse(hierarchy.isFrozen());
		hierarchy.freeze();
		Assert.assertTrue(hierarchy.isFrozen());
		try {
			hierarchy.registerClass(f);
			Assert.fail();
		} catch (FrozenHierarchyException e) {
		}
		try {
			hierarchy.registerInterfaces("pkg/C", new ArrayList<String>());
			Assert.fail();
		} catch (FrozenHierarchyException e) {
		}
		try {
			hierarchy.registerSuperClass("pkg/C", "java/lang/Object");
			Assert.fail();
		} catch (FrozenHierarchyException e) {
		}
		try {
			hierarchy.registerSubtype("pkg/NewChild", "pkg/C");
			Assert.fail();
		} catch (FrozenHierarchyException e) {
		}
	}
}
