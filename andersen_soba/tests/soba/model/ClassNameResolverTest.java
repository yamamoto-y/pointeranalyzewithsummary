package soba.model;


import org.junit.Assert;
import org.junit.Test;


public class ClassNameResolverTest {

	@Test
	public void testExtractClassNameWithoutPackage() {
		Assert.assertEquals("String", ClassNameResolver.extractClassNameWithoutPackage("java/lang/String"));
		Assert.assertEquals("String", ClassNameResolver.extractClassNameWithoutPackage("String"));
		Assert.assertEquals("Foo$String", ClassNameResolver.extractClassNameWithoutPackage("my/package/Foo$String"));
	}
	
	@Test
	public void testClassNameList() {
		String[] classes = new String[] { "java/awt/List", "java/lang/String", "java/lang/Object", "java/util/List", "java/io/InputStream" };
		ClassNameResolver resolver = new ClassNameResolver(classes);
		Assert.assertEquals(2, resolver.getClassList("List").size());
		Assert.assertEquals(1, resolver.getClassList("String").size());
		Assert.assertEquals(1, resolver.getClassList("Object").size());
		Assert.assertEquals(1, resolver.getClassList("InputStream").size());
		Assert.assertEquals(0, resolver.getClassList("Input").size());
		
		Assert.assertFalse(resolver.isUniqueClassName("List"));
		Assert.assertFalse(resolver.isUniqueClassName("java/util/List"));
		Assert.assertTrue(resolver.isUniqueClassName("String"));
		Assert.assertTrue(resolver.isUniqueClassName("java/lang/String"));
		Assert.assertFalse(resolver.isUniqueClassName("Input"));
	}
}
