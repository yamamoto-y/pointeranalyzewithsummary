package soba.model;

import java.util.ArrayList;
import java.util.List;

public class ClassInfoString implements IClassInfo {
	
	private String packageName;
	private String className;
	private String superClassName;
	private List<String> interfaces;
	
	public ClassInfoString(String packageName, String className, String superClassName, String[] interfaces) { 
		this.packageName = packageName;
		this.className = className;
		this.superClassName = superClassName;
		this.interfaces = new ArrayList<String>();
		if (interfaces != null) {
			for (String inf: interfaces) {
				this.interfaces.add(inf);
			}
		}
	}
	
	@Override
	public String getClassName() {
		return className;
	}
	
	@Override
	public String getPackageName() {
		return packageName;
	}
	
	@Override
	public String getSuperClass() {
		return superClassName;
	}
	
	@Override
	public List<String> getInterfaces() {
		return interfaces;
	}
	
	@Override
	public int getFieldCount() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public IFieldInfo getField(int index) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IFieldInfo getFieldByName(String fieldName, String fieldDesc) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IMethodInfo getMethod(int index) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public IMethodInfo getMethodByName(String methodName, String methodDesc) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getMethodCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
