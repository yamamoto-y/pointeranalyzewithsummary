package soba.model;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import soba.bytecode.ExampleProgram;
import soba.bytecode.JavaProgram;
import soba.bytecode.JavaProgramTest;

public class DynamicBindingResolverTest implements ExampleProgram {
	
	
	private DynamicBindingResolver resolver;
	
	@Before
	public void setupResolver() {
		JavaProgram program = JavaProgramTest.readExampleProgram();
		ClassHierarchy ch = program.getClassHierarchy();
		resolver = new DynamicBindingResolver(ch);
	}
	
	
	/**
	 * @param classNames specify a list of class names must be contained in methods.
	 * @param methods
	 */
	private void checkClasses(String[] classNames, IMethodInfo[] methods) {
		Assert.assertEquals(classNames.length, methods.length);
		boolean[] found = new boolean[classNames.length];
		for (int i=0; i<methods.length; ++i) {
			boolean elementFound = false;
			for (int j=0; j<classNames.length; ++j) {
				if (methods[i].getClassName().equals(classNames[j])) {
					elementFound = true;
					found[j] = true;
					break;
				}
			}
			Assert.assertTrue(methods[i].getClassName() + " is not included in expected class names.", elementFound);
		}

		for (int i=0; i<classNames.length; ++i) {
			Assert.assertTrue(classNames[i] + " is not found in methods.", found[i]);
		}
	}

	@Test
	public void testMain() {
		IMethodInfo[] methodMain = resolver.resolveCall(new Invocation(CLASS_E, "main", "([Ljava/lang/String;)V", Invocation.Kind.STATIC));
		checkClasses(new String[]{CLASS_E}, methodMain);
	}
	
	@Test
	public void testPackagePrivate() {
		// C.n() has 4 implementation: C, D, G, H.  F.n() is different.
		IMethodInfo[] methodsN = resolver.resolveCall(new Invocation(CLASS_C, "n", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_C, CLASS_D, CLASS_G, CLASS_H}, methodsN);

		IMethodInfo[] methodsNf = resolver.resolveCall(new Invocation(CLASS_F, "n", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_F}, methodsNf);
	}

	@Test
	public void testInheritance() {
		// C.m() and D.m() are defined.  H.m() is not implemented. 
		IMethodInfo[] methodsM = resolver.resolveCall(new Invocation(CLASS_D, "m", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_D}, methodsM);

		IMethodInfo[] methodsM2 = resolver.resolveCall(new Invocation(CLASS_H, "m", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_D}, methodsM2);

		IMethodInfo[] methodsM3 = resolver.resolveCall(new Invocation(CLASS_C, "m", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_C, CLASS_D}, methodsM3);
	}
	
	@Test
	public void testProtected() {
		// p() is defined by C and H, not by D.
		IMethodInfo[] methodsPc = resolver.resolveCall(new Invocation(CLASS_C, "p", "(I)V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_C, CLASS_H}, methodsPc);
		IMethodInfo[] methodsPd = resolver.resolveCall(new Invocation(CLASS_C, "p", "(I)V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_C, CLASS_H}, methodsPd);
		IMethodInfo[] methodsPh = resolver.resolveCall(new Invocation(CLASS_H, "p", "(I)V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_H}, methodsPh);
	}
	
	@Test
	public void testPrivateMethod() {
		// q() is defined by C and H but it is private.
		IMethodInfo[] methodsQc = resolver.resolveCall(new Invocation(CLASS_C, "q", "(D)V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_C}, methodsQc);
		IMethodInfo[] methodsQh = resolver.resolveCall(new Invocation(CLASS_H, "q", "(D)V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_H}, methodsQh);
	}
	
	@Test
	public void testInterface() {
		IMethodInfo[] methodsMi = resolver.resolveCall(new Invocation(CLASS_I, "m", "()V", Invocation.Kind.VIRTUAL));
		checkClasses(new String[]{CLASS_D}, methodsMi);
	}
	
	@Test
	public void testFields() {
		Assert.assertEquals(CLASS_C, resolver.resolveInstanceFieldOwner(CLASS_D, "x", "I"));
		Assert.assertEquals(CLASS_C, resolver.resolveInstanceFieldOwner(CLASS_C, "x", "I"));
		Assert.assertEquals(CLASS_H, resolver.resolveInstanceFieldOwner(CLASS_H, "x", "I"));
		Assert.assertEquals(CLASS_I, resolver.resolveStaticFieldOwner(CLASS_I, "x", "I"));
		Assert.assertEquals(CLASS_J, resolver.resolveStaticFieldOwner(CLASS_J, "x", "I"));
		Assert.assertEquals(CLASS_I, resolver.resolveStaticFieldOwner(CLASS_K, "x", "I")); // static field is also regarded as inherited.
	}
}
