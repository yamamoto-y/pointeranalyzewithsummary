package soba.util;

import org.junit.Assert;
import org.junit.Test;


public class BoolValueArrayTest {

	@Test
	public void testBoolValueArray() throws Exception {
		BoolValueArray array = new BoolValueArray(10);
		
		Assert.assertFalse(array.getValue(0));
		Assert.assertEquals(0, array.getTrueCount());
		
		array.setValue(1, true);
		Assert.assertTrue(array.getValue(1));
		Assert.assertFalse(array.getValue(0));
		Assert.assertEquals(1, array.getTrueCount());

		array.setValue(2, true);
		array.setValue(1, false);
		Assert.assertFalse(array.getValue(0));
		Assert.assertFalse(array.getValue(1));
		Assert.assertTrue(array.getValue(2));
		Assert.assertEquals(1, array.getTrueCount());

		array.setValue(1, true);
		Assert.assertEquals(2, array.getTrueCount());

		array.setValue(1, true);
		array.setValue(2, true);
		Assert.assertEquals(2, array.getTrueCount());

		array.setValue(1, false);
		array.setValue(1, true);
		Assert.assertEquals(2, array.getTrueCount());

	}
	
	@Test
	public void testEquals() {
		BoolValueArray a1 = new BoolValueArray(4);
		BoolValueArray a2 = new BoolValueArray(4);
		BoolValueArray a3 = new BoolValueArray(3);
		
		a1.setValue(1, true);
		a1.setValue(2, true);
		a2.setValue(1, true);
		a3.setValue(1, true);
		Assert.assertFalse(a1.equals(a2));
		Assert.assertFalse(a2.equals(a3)); // different size 
		a2.setValue(2, true);
		Assert.assertTrue(a1.equals(a2));
		Assert.assertFalse(a1.equals(a3));
	}
	
	@Test
	public void testExclusive() {
		BoolValueArray a1 = new BoolValueArray(4);
		BoolValueArray a2 = new BoolValueArray(4);
		BoolValueArray a3 = new BoolValueArray(3);
		a1.setValue(1, true);
		a1.setValue(2, true);
		a2.setValue(0, true);
		a2.setValue(3, true);
		a3.setValue(1, true);
		a3.setValue(2, true);
		Assert.assertTrue(a1.isExclusive(a2));
		Assert.assertFalse(a2.isExclusive(a3)); // different size 
		a1.setValue(2, false);
		Assert.assertTrue(a1.isExclusive(a2));
		a1.setValue(3, true);
		Assert.assertFalse(a1.isExclusive(a2));
	}

}
