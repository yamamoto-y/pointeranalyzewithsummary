package soba.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soba.util.IntPairQueue.EmptyQueueException;


public class IntPairQueueTest {

	private IntPairQueue queue;

	@Before
	public void setUp() throws Exception {
		queue = new IntPairQueue(4);
	}

	@Test
	public void testIsEmpty() throws Exception {
		Assert.assertTrue(queue.isEmpty());
		queue.add(0, 1);
		Assert.assertFalse(queue.isEmpty());
	}

	@Test
	public void testDefaultConstructor() throws Exception {
		Assert.assertTrue(new IntPairQueue().getCapacity() > 1024);
	}

	@Test
	public void testZeroLengthQueuePrevention() {
		try {
			new IntPairQueue(0);
			Assert.fail();
		} catch (EmptyQueueException e) {
		}
		try {
			new IntPairQueue(1);
			Assert.fail();
		} catch (EmptyQueueException e) {
		}
		try {
			new IntPairQueue(-1);
			Assert.fail();
		} catch (EmptyQueueException e) {
		}
	}

	@Test
	public void testGrowup() throws Exception {
		Assert.assertEquals(4, queue.getCapacity());
		queue.add(0, 1);
		queue.add(2, 3);
		queue.add(4, 5);
		queue.add(6, 7);
		Assert.assertEquals(8, queue.getCapacity());
		queue.add(8, 9);
		queue.add(10, 11);
		queue.add(12, 13);
		queue.add(14, 15);
		Assert.assertEquals(16, queue.getCapacity());
	}

	@Test
	public void testLoop() throws Exception {
		Checker c = new Checker();
		queue.add(0, 1);
		queue.add(2, 3);
		queue.add(4, 5);
		queue.iterateUntilEmpty(c);

		queue.add(6, 7);
		queue.add(8, 9);
		queue.add(10, 11);
		Assert.assertEquals(4, queue.getCapacity());

		queue.add(12, 13);
		Assert.assertEquals(8, queue.getCapacity());

		queue.add(14, 15);
		queue.iterateUntilEmpty(c);
		Assert.assertTrue(queue.isEmpty());

		queue.add(16, 17);
		queue.add(18, 19);
		queue.iterateUntilEmpty(new IntPairProc() {
			@Override
			public boolean execute(int elem1, int elem2) {
				Assert.assertEquals(16, elem1);
				Assert.assertEquals(17, elem2);
				return false;
			}
		});
		Assert.assertFalse(queue.isEmpty());
		queue.iterateUntilEmpty(new IntPairProc() {
			@Override
			public boolean execute(int elem1, int elem2) {
				Assert.assertEquals(18, elem1);
				Assert.assertEquals(19, elem2);
				return true;
			}
		});
		Assert.assertTrue(queue.isEmpty());
	}

	private class Checker implements IntPairProc {
		int idx = 0;
		@Override
		public boolean execute(int elem1, int elem2) {
			Assert.assertEquals(idx++, elem1);
			Assert.assertEquals(idx++, elem2);
			return true;
		}
	}

}
