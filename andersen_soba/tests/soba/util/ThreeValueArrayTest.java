package soba.util;

import junit.framework.Assert;

import org.junit.Test;

public class ThreeValueArrayTest {

	@Test
	public void testArray() {
		ThreeValueArray array = new ThreeValueArray(3);
		Assert.assertEquals(3, array.size());
		Assert.assertFalse(array.isAssigned(0));
		Assert.assertFalse(array.isAssigned(1));
		Assert.assertFalse(array.isAssigned(2));
		array.setValue(1, true);
		array.setValue(2, false);
		Assert.assertFalse(array.isAssigned(0));
		Assert.assertTrue(array.isAssigned(1));
		Assert.assertTrue(array.isAssigned(2));
		Assert.assertFalse(array.isTrue(0));
		Assert.assertTrue(array.isTrue(1));
		Assert.assertFalse(array.isTrue(2));
		Assert.assertFalse(array.isFalse(0));
		Assert.assertFalse(array.isFalse(1));
		Assert.assertTrue(array.isFalse(2));
	}
	
	@Test
	public void testTrueCount() {
		ThreeValueArray array = new ThreeValueArray(3);
		Assert.assertEquals(0, array.getTrueCount());
		array.setValue(1, true);
		Assert.assertEquals(1, array.getTrueCount());
		array.setValue(2, true);
		Assert.assertEquals(2, array.getTrueCount());
		array.setValue(1, false);
		Assert.assertEquals(1, array.getTrueCount());
		array.setValue(0, false);
		Assert.assertEquals(1, array.getTrueCount());
		array.setValue(0, true);
		Assert.assertEquals(2, array.getTrueCount());
		array.setValue(0, false);
		Assert.assertEquals(1, array.getTrueCount());
		array.setValue(2, false);
		Assert.assertEquals(0, array.getTrueCount());
	}
	
	
}
