package soba.util;

import java.util.EmptyStackException;

import org.junit.Assert;
import org.junit.Test;


public class IntStackTest {

	@Test
	public void testStack() throws Exception {
		IntStack stack = new IntStack(3);
		Assert.assertTrue(stack.isEmpty());
		stack.push(0);
		Assert.assertEquals(0, stack.peek());
		Assert.assertFalse(stack.isEmpty());
		stack.push(1);
		Assert.assertEquals(1, stack.peek());
		Assert.assertFalse(stack.isEmpty());
		stack.push(2);
		Assert.assertEquals(2, stack.peek());
		Assert.assertFalse(stack.isEmpty());
		
		Assert.assertEquals(2, stack.pop());
		Assert.assertEquals(1, stack.peek());
		Assert.assertEquals(1, stack.pop());
		Assert.assertEquals(0, stack.pop());
		Assert.assertTrue(stack.isEmpty());
		try {
			stack.pop();
			Assert.fail();
		} catch (EmptyStackException e) {
		}
		try {
			stack.peek();
			Assert.fail();
		} catch (EmptyStackException e) {
		}
		
		stack.push(0);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		Assert.assertEquals(6, stack.pop());
		Assert.assertTrue(stack.contains(3));
		Assert.assertEquals(5, stack.pop());
		Assert.assertEquals(4, stack.pop());
		Assert.assertTrue(stack.contains(3));
		Assert.assertEquals(3, stack.pop());
		Assert.assertFalse(stack.contains(3));
		Assert.assertEquals(2, stack.pop());
		Assert.assertEquals(1, stack.pop());
		Assert.assertFalse(stack.isEmpty());
		Assert.assertEquals(0, stack.pop());
		Assert.assertTrue(stack.isEmpty());
	}
}
