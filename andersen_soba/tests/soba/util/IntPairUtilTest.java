package soba.util;

import junit.framework.Assert;

import org.junit.Test;

public class IntPairUtilTest {

	@Test
	public void testCreateList() {
		IntPairSet set = new IntPairSet();
		set.add(4, 1);
		set.add(3, 2);
		set.add(1, 4);
		set.add(1, 2);
		set.add(4, 1);
		set.add(0, 1);
		set.add(2, 9);
		set.add(3, 2);
		IntPairList list = IntPairUtil.createList(set);
		list.sort();
		Assert.assertEquals(6, list.size());
		Assert.assertEquals(0, list.getFirstValue(0));
		Assert.assertEquals(1, list.getSecondValue(0));
		Assert.assertEquals(1, list.getFirstValue(1));
		Assert.assertEquals(2, list.getSecondValue(1));
		Assert.assertEquals(1, list.getFirstValue(2));
		Assert.assertEquals(4, list.getSecondValue(2));
		Assert.assertEquals(2, list.getFirstValue(3));
		Assert.assertEquals(9, list.getSecondValue(3));
		Assert.assertEquals(3, list.getFirstValue(4));
		Assert.assertEquals(2, list.getSecondValue(4));
		Assert.assertEquals(4, list.getFirstValue(5));
		Assert.assertEquals(1, list.getSecondValue(5));
	}
	
}
