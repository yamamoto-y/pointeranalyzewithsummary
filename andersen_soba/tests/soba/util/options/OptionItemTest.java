package soba.util.options;

import org.junit.Assert;

import org.junit.Test;


public class OptionItemTest {

	@Test
	public void testBooleanOption() {
		BooleanOption opt1 = new BooleanOption("test1");
		BooleanOption opt2 = new BooleanOption("test2", false);
		BooleanOption opt3 = new BooleanOption("test3", true);
		
		Assert.assertEquals("test1", opt1.getName());
		Assert.assertFalse(opt1.hasValue());
		Assert.assertFalse(opt1.isValid()); 
		Assert.assertFalse(opt2.hasValue()); // opt2 has no values, but 
		Assert.assertTrue(opt2.isValid());   // opt2 has a default value.
		Assert.assertFalse(opt3.hasValue());
		Assert.assertTrue(opt3.isValid());
		
		try {
			opt1.getValue();
			Assert.fail();
		} catch (InvalidParameterException e) {
		}
		Assert.assertFalse(opt2.getValue());
		Assert.assertTrue(opt3.getValue());
		opt2.parse(new String[0]);
		Assert.assertTrue(opt2.hasValue());
		Assert.assertTrue(opt2.getValue());
		opt3.parse(new String[0]);
		Assert.assertTrue(opt3.hasValue());
		Assert.assertTrue(opt3.getValue());
		opt2.clear();
		Assert.assertTrue(opt2.isValid());
		Assert.assertFalse(opt2.hasValue());
		Assert.assertFalse(opt2.getValue());
		try {
			opt2.parse(new String[0]);
			opt3.parse(new String[0]);
			Assert.fail();
		} catch (InvalidParameterException e) {
		}
	}
	
	
}
