package soba.util.output;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CSVPrinterTest {

	private PrintStream out;
	private ByteArrayOutputStream buf; 
	private CSVPrinter printer;

	private String newline;
	
	@Before
	public void createStream() {
		buf = new ByteArrayOutputStream(4096);
		out = new PrintStream(buf);
		printer = new CSVPrinter(out, new String[] {"col1", "col2", "col3", "col4"});
		newline = System.getProperty("line.separator");
	}
	
	@Test
	public void testCurrentLine() {
		Assert.assertEquals(0, printer.getCurrentLine());
		printer.print("");
		printer.print(2);
		printer.print(false);
		printer.print("");
		printer.println();
		Assert.assertEquals(1, printer.getCurrentLine());
	}

	@Test
	public void testSeparator() {
		ByteArrayOutputStream anotherBuf = new ByteArrayOutputStream(128);
		PrintStream anotherPrint = new PrintStream(anotherBuf);
		CSVPrinter another = new CSVPrinter(anotherPrint, new String[]{"col1", "col2", "col3"}, CSVPrinter.SEPARATOR_COMMA);
		another.print("");
		another.print(2);
		another.print(false);
		another.println();
		another.close();
		Assert.assertEquals("col1,col2,col3" + newline + ",2,false" + newline, anotherBuf.toString());
	}
	
	@Test
	public void testQuotation() {
		printer.setUseQuotation(true);
		printer.setColumnType(1, CSVPrinter.ColumnType.Integer);
		printer.print("");
		printer.print(3);
		printer.print("My \"word\"");
		printer.print(false);
		printer.close();
		Assert.assertEquals("col1\tcol2\tcol3\tcol4" + newline + "\"\"\t3\t\"My \"\"word\"\"\"\tfalse", buf.toString());
	}

	@Test
	public void testColumnType() { 
		printer.setColumnType(0, CSVPrinter.ColumnType.String);
		printer.setColumnType(1, CSVPrinter.ColumnType.Integer);
		printer.setColumnType(2, CSVPrinter.ColumnType.Boolean);
		printer.print("");
		printer.print(1);
		printer.print(false);
		printer.print("");
		printer.println();
		try {
			printer.print(1);
			Assert.fail();
		} catch (CSVPrinter.InvalidColumnTypeException e) {
		}
		printer.print("");
		try {
			printer.print("");
			Assert.fail();
		} catch (CSVPrinter.InvalidColumnTypeException e) {
		}
		printer.print(2);
		try {
			printer.print(1);
			Assert.fail();
		} catch (CSVPrinter.InvalidColumnTypeException e) {
		}
		printer.print(true);
		printer.printLineId();
		printer.println();
	}
}
