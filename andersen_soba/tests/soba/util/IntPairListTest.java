package soba.util;

import org.junit.Assert;
import org.junit.Test;


public class IntPairListTest {

	@Test
	public void testAdd() throws Exception {
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		list.add(3, 4);
		list.add(5, 6);
		
		Assert.assertEquals(1, list.getFirstValue(0));
		Assert.assertEquals(2, list.getSecondValue(0));
		Assert.assertEquals(3, list.getFirstValue(1));
		Assert.assertEquals(4, list.getSecondValue(1));
		Assert.assertEquals(5, list.getFirstValue(2));
		Assert.assertEquals(6, list.getSecondValue(2));
		Assert.assertEquals(3, list.size());
	}

	@Test
	public void testSortByFirstValues() throws Exception {
		IntPairList list = new IntPairList();
		list.add(1, 1); // 1
		list.add(2, 0); // 2
		list.add(3, 0); // 3
		list.add(4, 4); // 4
		list.add(5, 5); // 5
		list.add(6, 3); // 6
		list.add(7, 2); // 7
		list.add(8, 4); // 8
		list.add(9, 1); // 9
		list.add(0, 5); // 0
		
		list.sort();
		
		Assert.assertEquals(2, list.getFirstValue(2));
		Assert.assertEquals(0, list.getSecondValue(2));
		Assert.assertEquals(4, list.getFirstValue(4));
		Assert.assertEquals(4, list.getSecondValue(4));
		Assert.assertEquals(9, list.getFirstValue(9));
		Assert.assertEquals(1, list.getSecondValue(9));
	}


	@Test
	public void testSetValues() throws Exception {
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		list.add(3, 4);
		list.add(5, 6);
		
		list.setFirstValue(1, 10);
		Assert.assertEquals(1, list.getFirstValue(0));
		Assert.assertEquals(10, list.getFirstValue(1));
		Assert.assertEquals(5, list.getFirstValue(2));
		Assert.assertEquals(2, list.getSecondValue(0));
		Assert.assertEquals(4, list.getSecondValue(1));
		Assert.assertEquals(6, list.getSecondValue(2));
		
		list.setSecondValue(2, 27);
		Assert.assertEquals(1, list.getFirstValue(0));
		Assert.assertEquals(10, list.getFirstValue(1));
		Assert.assertEquals(5, list.getFirstValue(2));
		Assert.assertEquals(2, list.getSecondValue(0));
		Assert.assertEquals(4, list.getSecondValue(1));
		Assert.assertEquals(27, list.getSecondValue(2));
	}
	
	@Test
	public void testAddAll() { 
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		list.add(3, 4);
		IntPairList another = new IntPairList(2);
		another.add(5, 6);
		another.add(7, 8);
		
		list.addAll(another);
		Assert.assertEquals(4, list.size());
		Assert.assertEquals(1, list.getFirstValue(0));
		Assert.assertEquals(2, list.getSecondValue(0));
		Assert.assertEquals(3, list.getFirstValue(1));
		Assert.assertEquals(4, list.getSecondValue(1));
		Assert.assertEquals(5, list.getFirstValue(2));
		Assert.assertEquals(6, list.getSecondValue(2));
		Assert.assertEquals(7, list.getFirstValue(3));
		Assert.assertEquals(8, list.getSecondValue(3));
		Assert.assertEquals(2, another.size());

		another.addAll(another);
		Assert.assertEquals(4, another.size());
		Assert.assertEquals(5, another.getFirstValue(0));
		Assert.assertEquals(6, another.getSecondValue(0));
		Assert.assertEquals(7, another.getFirstValue(1));
		Assert.assertEquals(8, another.getSecondValue(1));
		Assert.assertEquals(5, another.getFirstValue(2));
		Assert.assertEquals(6, another.getSecondValue(2));
		Assert.assertEquals(7, another.getFirstValue(3));
		Assert.assertEquals(8, another.getSecondValue(3));
	}
	
	@Test
	public void testFreeze() {
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		list.freeze();
		try {
			list.add(3, 4);
			Assert.fail();
		} catch (IntPairList.FrozenListException e) {
		}
		try {
			list.setFirstValue(0, 1);
			Assert.fail();
		} catch (IntPairList.FrozenListException e) {
		}
		try {
			list.addAll(list);
			Assert.fail();
		} catch (IntPairList.FrozenListException e) {
		}
	}
	
	@Test
	public void testArrayIndexOutOfBounds() {
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		try {
			list.getFirstValue(-1);
			Assert.fail();
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			list.getFirstValue(1);
			Assert.fail();
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			list.getSecondValue(-1);
			Assert.fail();
		} catch (ArrayIndexOutOfBoundsException e) {
		}
		try {
			list.getSecondValue(1);
			Assert.fail();
		} catch (ArrayIndexOutOfBoundsException e) {
		}
	}
	
	@Test
	public void testForEach() {
		IntPairList list = new IntPairList(2);
		list.add(1, 2);
		list.add(3, 4);
		list.add(5, 6);
		list.foreach(new IntPairProc() {
			int times = 0;
			@Override
			public boolean execute(int elem1, int elem2) {
				if (times == 0) {
					Assert.assertEquals(1, elem1);
					Assert.assertEquals(2, elem2);
				} else if (times == 1) {
					Assert.assertEquals(3, elem1);
					Assert.assertEquals(4, elem2);
				} else {
					Assert.fail();
				}
				times++;
				return times == 1;
			}
		});
	}
}
