package soba.util;

import junit.framework.Assert;

import org.junit.Test;

public class ObjectIdMapTest {

	@Test
	public void testObjectIdMap() {
		ObjectIdMap<String> idMap = new ObjectIdMap<String>();
		int idABC = idMap.getId("abc");
		int idAB = idMap.getId("ab");
		int idABC2 = idMap.getId("abc");
		int idD = idMap.getId("d");
		Assert.assertEquals(3, idMap.size());
		Assert.assertEquals(0, idABC);
		Assert.assertEquals(0, idABC2);
		Assert.assertEquals(1, idAB);
		Assert.assertEquals(2, idD);
		Assert.assertEquals("abc", idMap.getItem(idABC));
		Assert.assertEquals("ab", idMap.getItem(idAB));
		Assert.assertEquals("d", idMap.getItem(idD));
		Assert.assertNull(idMap.getItem(-1));
		Assert.assertNull(idMap.getItem(4));
	}
	
	@Test
	public void testAdd() { 
		ObjectIdMap<String> idMap = new ObjectIdMap<String>();
		idMap.add("abc");
		idMap.add("ab");
		idMap.add("ab");
		idMap.add("abc");
		Assert.assertEquals(2, idMap.size());
		Assert.assertEquals("abc", idMap.getItem(0));
		Assert.assertEquals("ab", idMap.getItem(1));
	}

	@Test
	public void testFreeze() { 
		ObjectIdMap<String> idMap = new ObjectIdMap<String>();
		idMap.add("abc");
		idMap.add("ab");
		idMap.freeze();
		try {
			idMap.add("abc"); // ignored already registerd item
			idMap.add("xyz"); // throws an exception
			Assert.fail();
		} catch (ObjectIdMap.FrozenMapException e) {
		}
		Assert.assertEquals(1, idMap.getId("ab"));
		Assert.assertEquals("abc", idMap.getItem(0));
		Assert.assertNull(idMap.getItem(3));
	}

}
