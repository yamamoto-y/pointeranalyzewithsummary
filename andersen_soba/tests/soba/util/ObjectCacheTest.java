package soba.util;

import org.junit.Assert;
import org.junit.Test;

public class ObjectCacheTest {

	@Test
	public void testCache() { 
		ObjectCache<String> stringCache = new ObjectCache<String>();
		String s1 = "xyz";
		String s2 = "x".concat("yz");
		String s3 = "xy".concat("z");
		String shared = stringCache.getSharedInstance(s1);
		String another = stringCache.getSharedInstance("abc");
		String nullString = stringCache.getSharedInstance(null);
		String shared2 = stringCache.getSharedInstance(s2);
		String shared3 = stringCache.getSharedInstance(s3);
		Assert.assertTrue(s1 == shared);
		Assert.assertTrue(shared2 == shared);
		Assert.assertTrue(shared3 == shared);
		Assert.assertTrue(another != shared);
		Assert.assertNull(nullString);
	}
	
	
}
