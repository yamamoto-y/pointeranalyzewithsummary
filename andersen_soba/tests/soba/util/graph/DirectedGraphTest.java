package soba.util.graph;

import gnu.trove.list.array.TIntArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soba.util.IntPairList;


public class DirectedGraphTest {
	
	private DirectedGraph graph;
	
	@Before
	public void buildGraph() {
		IntPairList edges = new IntPairList();
		edges.add(0, 1); // cycle 0->1->2
		edges.add(1, 2);
		edges.add(2, 0);
		edges.add(1, 3);
		
		edges.add(3, 4);
		edges.add(3, 5);
		
		edges.add(5, 6); // two cycles: 5->6->8->5
		edges.add(6, 7); // and 5->6->7->8->5
		edges.add(6, 8);
		edges.add(7, 8);
		edges.add(8, 5);

		edges.add(7, 9);
		edges.add(7, 10);
		edges.add(9, 11);
		edges.add(10, 11);
		edges.add(11, 12); // 13 is not connected to any other vertices
		
		graph = new DirectedGraph(14, edges);
	}
	
	
	@Test
	public void testDepthFirstSearch() throws Exception {
		final TIntArrayList visited = new TIntArrayList();
		DepthFirstSearch.search(graph, 0, new IDepthFirstVisitor() {
			@Override
			public void onStart(int startVertexId) {
			}
			@Override
			public boolean onVisit(int vertexId) {
				visited.add(vertexId);
				return true;
			}
			@Override
			public void onLeave(int vertexId) {
			}
			@Override
			public void onFinished(boolean[] visited) {
				Assert.assertEquals(14, visited.length);
				for (int i=0; i<13; ++i) {
					Assert.assertTrue(visited[i]);
				}
				Assert.assertFalse(visited[13]);
			}
			@Override
			public void onVisitAgain(int vertexId) {
				Assert.assertTrue(visited.contains(vertexId));
			}
		});

		Assert.assertEquals(13, visited.size());
	}	
	
	@Test
	public void testEdgeCount() {
		Assert.assertEquals(16, graph.getEdgeCount());
	}
	
	@Test
	public void testUndirectedGraph() {
		DirectedGraph g = graph.getUndirectedGraph();
		Assert.assertArrayEquals(new int[]{0, 2, 3}, g.getEdges(1));
		Assert.assertArrayEquals(new int[]{7, 11}, g.getEdges(10));
		Assert.assertArrayEquals(new int[]{11}, g.getEdges(12));
		Assert.assertArrayEquals(new int[0], g.getEdges(13));
		Assert.assertEquals(32, g.getEdgeCount());
	}
}
