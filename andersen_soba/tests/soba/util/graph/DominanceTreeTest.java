package soba.util.graph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class DominanceTreeTest {

	private DirectedGraph graph;
	
	@Before
	public void buildGraph() throws Exception {
		graph = GraphTestBase.buildGraph();
	}

	@Test
	public void testDominanceTree() {
		SingleRootDirectedGraph g = new SingleRootDirectedGraph(graph);
		DominanceTree tree = new DominanceTree(g);
		Assert.assertTrue(tree.isRoot(g.getRootId()));
		Assert.assertFalse(tree.isRoot(0));
		Assert.assertFalse(tree.isRoot(13));
		Assert.assertEquals(14, tree.getDominator(0));
		Assert.assertEquals(0, tree.getDominator(1));
		Assert.assertEquals(1, tree.getDominator(2));
		Assert.assertEquals(1, tree.getDominator(3));
		Assert.assertEquals(3, tree.getDominator(4));
		Assert.assertEquals(3, tree.getDominator(5));
		Assert.assertEquals(5, tree.getDominator(6));
		Assert.assertEquals(6, tree.getDominator(7));
		Assert.assertEquals(6, tree.getDominator(8));
		Assert.assertEquals(7, tree.getDominator(9));
		Assert.assertEquals(7, tree.getDominator(10));
		Assert.assertEquals(7, tree.getDominator(11));
		Assert.assertEquals(11, tree.getDominator(12));
		Assert.assertEquals(14, tree.getDominator(13));
		
		Assert.assertEquals(14, tree.nearestCommonAncestor(1, 13));
		Assert.assertEquals(5, tree.nearestCommonAncestor(5, 12));
		Assert.assertEquals(3, tree.nearestCommonAncestor(4, 12));
		Assert.assertEquals(3, tree.nearestCommonAncestor(12, 4));
		Assert.assertEquals(6, tree.nearestCommonAncestor(8, 6));
	}
}
