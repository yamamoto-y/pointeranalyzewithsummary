package soba.util.graph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soba.util.IntPairProc;

public class DirectedAcyclicGraphTest {

	DirectedGraph graph;
	DirectedAcyclicGraph dag;
	
	@Before
	public void buildGraph() throws Exception {
		graph = GraphTestBase.buildGraph();
		dag = new DirectedAcyclicGraph(graph);
	}
	
	@Test
	public void testAcyclicGraphNodes() {
		Assert.assertEquals(graph.getVertexCount(), dag.getVertexCount());
		Assert.assertTrue(dag.isRepresentativeNode(0));
		Assert.assertFalse(dag.isRepresentativeNode(1));
		Assert.assertFalse(dag.isRepresentativeNode(2));
		Assert.assertTrue(dag.isRepresentativeNode(3));
		Assert.assertTrue(dag.isRepresentativeNode(4));
		Assert.assertTrue(dag.isRepresentativeNode(5));
		Assert.assertFalse(dag.isRepresentativeNode(6));
		Assert.assertFalse(dag.isRepresentativeNode(7));
		Assert.assertFalse(dag.isRepresentativeNode(8));
		Assert.assertTrue(dag.isRepresentativeNode(9));
		Assert.assertTrue(dag.isRepresentativeNode(10));
		Assert.assertTrue(dag.isRepresentativeNode(11));
		Assert.assertTrue(dag.isRepresentativeNode(12));
		Assert.assertTrue(dag.isRepresentativeNode(13));
	}
	
	@Test
	public void testAcyclicGraphEdges() {
		dag.forEachEdge(new IntPairProc() {

			int index = 0;
			int[][] expected = new int[][] {
				{0, 3}, {3, 4}, {3, 5}, {5, 9}, {5, 10},
				{9, 11}, {10, 11}, {11, 12} };

			@Override
			public boolean execute(int elem1, int elem2) {
				Assert.assertEquals(expected[index][0], elem1);
				Assert.assertEquals(expected[index][1], elem2);
				index++;
				return true;
			}
		});
		
		Assert.assertArrayEquals(new int[0], dag.getEdges(13));
		Assert.assertArrayEquals(new int[]{4, 5}, dag.getEdges(3));
	}

	
}
