package soba.util.graph;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import soba.util.IntPairProc;


public class SingleRootDirectedGraphTest {


	private DirectedGraph graph;
	
	@Before
	public void buildGraph() throws Exception {
		graph = GraphTestBase.buildGraph();
	}

	@Test
	public void testDominanceTree() throws Exception {
		SingleRootDirectedGraph base = new SingleRootDirectedGraph(graph);
		Assert.assertEquals(15, base.getVertexCount());
		Assert.assertEquals(14, base.getRootId());
		Assert.assertEquals(2, base.getEdges(14).length);
		Assert.assertEquals(0, base.getEdges(14)[0]);
		Assert.assertEquals(13, base.getEdges(14)[1]);
	}
	
	@Test
	public void testEdges() throws Exception {
		final SingleRootDirectedGraph base = new SingleRootDirectedGraph(graph);
		base.forEachEdge(new IntPairProc() {
			
			int index = 0;
			int[][] expected = new int[][] {
				{0, 1}, {1, 2}, {1, 3}, {2, 0},
				{3, 4}, {3, 5}, {5, 6}, {6, 7},
				{6, 8}, {7, 8}, {7, 9}, {7, 10},
				{8, 5}, {9, 11}, {10, 11}, {11, 12},
				{14, 0}, {14, 13}
			};
			@Override
			public boolean execute(int elem1, int elem2) {
				Assert.assertEquals(expected[index][0], elem1);
				Assert.assertEquals(expected[index][1], elem2);
				index++;
				return true;
			}
		});
		base.forEachEdge(new IntPairProc() {
			private boolean first = true;
			@Override
			public boolean execute(int elem1, int elem2) {
				if (first) {
					first = false;
					return false;
				} else {
					Assert.fail();
					return false;
				}
			}
		});
		base.forEachEdge(new IntPairProc() {
			private boolean firstFromRoot = true;
			@Override
			public boolean execute(int elem1, int elem2) {
				if (base.getRootId() == elem1) {
					if (firstFromRoot) {
						firstFromRoot = false;
						return false;
					} else {
						Assert.fail();
						return false;
					}
				} else {
					return true;
				}
			}
		});
	}
}
