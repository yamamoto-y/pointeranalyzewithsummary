package soba.util.files;

import java.io.File;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;


public class DirectoryTest {
	
	@Test
	public void testListSubdirectories() {
		File f = new File("src");
		Assume.assumeTrue(f.isDirectory());
		
		Directory[] dir = Directory.listSubdirectories(f, 0);
		Assert.assertEquals(1, dir.length);
		Assert.assertEquals(f, dir[0].getDirectory());

		File soba = new File(f, "soba");
		Directory[] subdirs = Directory.listSubdirectories(f, 1);
		Assert.assertEquals(1, dir.length);
		Assert.assertEquals(soba, subdirs[0].getDirectory());

	}

}
