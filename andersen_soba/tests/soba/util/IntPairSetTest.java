package soba.util;

import org.junit.Assert;
import org.junit.Test;

public class IntPairSetTest {

	@Test
	public void testIntPairSet() {
		IntPairSet set = new IntPairSet();
		Assert.assertEquals(0, set.size());
		set.add(0, 1);
		Assert.assertEquals(1, set.size());
		set.add(3, 2);
		Assert.assertEquals(2, set.size());
		Assert.assertTrue(set.contains(0, 1));
		Assert.assertTrue(set.contains(3, 2));
		Assert.assertFalse(set.contains(0, 2));
		set.add(1, 2);
		Assert.assertEquals(3, set.size());
		set.add(0, 1);
		Assert.assertEquals(3, set.size());
		Assert.assertTrue(set.contains(0, 1));
		Assert.assertTrue(set.contains(1, 2));
		Assert.assertTrue(set.contains(3, 2));
	}

	private boolean visited01 = false;
	private boolean visited12 = false;
	private boolean visited14 = false;
	private boolean visited32 = false;
	private boolean visited41 = false;
	
	@Test
	public void testForEach() {
		IntPairSet set = new IntPairSet();
		set.add(4, 1);
		set.add(3, 2);
		set.add(1, 4);
		set.add(1, 2);
		set.add(0, 1);
		set.foreach(new IntPairProc() {
			
			@Override
			public boolean execute(int elem1, int elem2) {
				switch (elem1) {
				case 0:	
					Assert.assertEquals(1, elem2);
					Assert.assertFalse(visited01);
					visited01 = true;
					break;
				case 1:
					if (elem2 == 2) {
						Assert.assertFalse(visited12);
						visited12 = true;
					} else if (elem2 == 4) {
						Assert.assertFalse(visited14);
						visited14 = true;
					} else {
						Assert.fail();
					}
					break;
				case 3:
					Assert.assertEquals(2, elem2);
					Assert.assertFalse(visited32);
					visited32 = true;
					break;
				case 4:
					Assert.assertEquals(1, elem2);
					Assert.assertFalse(visited41);
					visited41 = true;
					break;
				default:
					Assert.fail();
				}
				return true;
			}
		});
		Assert.assertTrue(visited01);
		Assert.assertTrue(visited12);
		Assert.assertTrue(visited14);
		Assert.assertTrue(visited32);
		Assert.assertTrue(visited41);
	}
	
}
