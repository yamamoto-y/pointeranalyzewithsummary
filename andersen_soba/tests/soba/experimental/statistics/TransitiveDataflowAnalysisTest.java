package soba.experimental.statistics;

import junit.framework.Assert;

import org.junit.Test;

public class TransitiveDataflowAnalysisTest {

	@Test
	public void testIncludeIntArray() {
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{2, 3, 1}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{1, 2}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{1, 3}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{2, 3}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{1}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{2}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{3}));
		Assert.assertTrue(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[0]));
		Assert.assertFalse(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{0}));
		Assert.assertFalse(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{1, 2, 3, 4}));
		Assert.assertFalse(TransitiveDataflowAnalysis.includeIntArray(new int[]{1, 2, 3}, new int[]{0, 1, 2, 3}));
	}

}
