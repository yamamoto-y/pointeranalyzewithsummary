package soba.bytecode.analysis.vta;

import java.util.List;

import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.bytecode.MethodInfo;
import soba.bytecode.method.CallSite;
import soba.model.IFieldInfo;
import soba.model.IMethodInfo;
import soba.util.files.ClasspathUtil;
import soba.util.files.IFileEnumerator;

public class VTADebug {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		IFileEnumerator[] target = ClasspathUtil.getEnumratorsFromString(ClasspathUtil.enumerateSystemClasspath());
//		IFileEnumerator[] target = new IFileEnumerator[] { new Directory(new File("bin")), new ZipFile(new File("lib/trove-2.1.0.jar")) };
	//	IFileEnumerator[] target = new IFileEnumerator[] { new Directory(new File("bin")) };
		IAnalysisTarget soba = new IAnalysisTarget() {
			
			@Override
			public boolean isTargetMethod(IMethodInfo m) {
				//return true;
				//return m.getClassName().startsWith("soba") || m.getClassName().startsWith("gnu/trove");
				return m.getClassName().startsWith("soba"); 
			}
			
			@Override
			public boolean isTargetField(IFieldInfo f) {
				return f.getClassName().startsWith("soba") || f.getClassName().startsWith("gnu/trove");
			}
			
			@Override
			public boolean assumeExternalCallers(IMethodInfo m) {
				return false;
			}
			
			@Override
			public boolean isExcludedType(String className) {
				return className.startsWith("com/sun/") ||
					className.startsWith("sun/");
			}
		};
		long t = System.currentTimeMillis();
		JavaProgram program = new JavaProgram(target);
		long loadTime = System.currentTimeMillis() - t;
		t = System.currentTimeMillis();
		
		VTAResolver graph = new VTAResolver(program, soba);
		long constructionTime = System.currentTimeMillis() - t;
		t = System.currentTimeMillis();

//		JavaProgram program = JavaProgramTest.readExampleProgram();
//		VTACallGraph graph = new VTACallGraph(program, null);
		for (ClassInfo c: program.getClasses()) {
			for (int i=0; i<c.getMethodCount(); ++i) {
				MethodInfo m = c.getMethod(i);
				if (soba.isTargetMethod(m) && m.hasMethodBody()) {
					System.out.println(m.toLongString());
					List<CallSite> callsites = m.getMethodBody().listCallSites();
					System.out.println("  Included " + Integer.toString(callsites.size()) + " callsites.");
					for (CallSite call: callsites) {
						System.out.print(Integer.toString(call.getInstructionIndex()) + ": ");
						System.out.println(call.getInvokedMethod().getClassName() + "." + call.getInvokedMethod().getMethodName());
						if (!call.getInvokedMethod().isStaticMethod()) {
							TypeSet ts = graph.getReceiverTypeAtCallsite(m, call.getInstructionIndex());
							if (ts != null) {
								for (int tIndex=0; tIndex<ts.getTypeCount(); ++tIndex) {
									System.out.println("    Type: " + ts.getType(tIndex));
								}
								for (int tIndex=0; tIndex<ts.getApproximatedTypeCount(); ++tIndex) {
									System.out.println("    Approx.: " + ts.getApproximatedType(tIndex));
								}
							} else {
								System.out.println("    No type information.");
							}
						} else {
							System.out.println("    Static Method.");
						}
						IMethodInfo[] methods = graph.resolveMethodCall(m, call.getInstructionIndex());
						if (methods.length > 0) {
							for (int mIndex=0; mIndex<methods.length; ++mIndex) {
								System.out.println("    RESOLVED as: " + methods[mIndex].toLongString());
							}
						} else {
							System.out.println("    No binding information.");
						}
					}
				}
			}
		}
		long outputTime = System.currentTimeMillis() - t;
		graph.dumpPerformanceProperties();
		System.out.println("Load = " + loadTime + "ms");
		System.out.println("Construction = " + constructionTime + "ms");
		System.out.println("Output = " + outputTime + "ms");
	}

}
