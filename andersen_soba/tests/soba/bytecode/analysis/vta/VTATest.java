package soba.bytecode.analysis.vta;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;

import soba.bytecode.ClassInfo;
import soba.bytecode.JavaProgram;
import soba.bytecode.JavaProgramTest;
import soba.bytecode.MethodInfo;
import soba.model.IFieldInfo;
import soba.model.IMethodInfo;

public class VTATest {

	private JavaProgram program;
	private VTAResolver resolver;
	
	@Before
	public void setupResolver() {
		program = JavaProgramTest.readExampleProgram();
		resolver = new VTAResolver(program, new IAnalysisTarget(){
			@Override
			public boolean assumeExternalCallers(IMethodInfo m) {
				return false;
			}
			@Override
			public boolean isExcludedType(String className) {
				return false;
			}
			@Override
			public boolean isTargetMethod(IMethodInfo m) {
				return m.getClassName().startsWith("soba/testdata");
			}
			@Override
			public boolean isTargetField(IFieldInfo f) {
				return true;
			}
		});
	}
	
	@Test
	public void testBinidng1() {
		ClassInfo c = program.getClassInfo("soba/testdata/inheritance2/E");
		MethodInfo m = c.getMethodByName("testDynamicBinding1", "()V");
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		int counter = 0;
		for (int i=0; i<instructions.size(); ++i) {
			if (instructions.get(i).getOpcode() == Opcodes.INVOKEVIRTUAL) {
				if (counter == 0 || counter == 1) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(2, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/D", methods[0].getClassName());
					Assert.assertEquals("soba/testdata/inheritance1/G", methods[1].getClassName());
				}
				counter++;
			}
		}
		Assert.assertTrue(counter == 2);
		
	}

	@Test
	public void testBinidng2() {
		ClassInfo c = program.getClassInfo("soba/testdata/inheritance2/E");
		MethodInfo m = c.getMethodByName("testDynamicBinding2", "()V");
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		int counter = 0;
		for (int i=0; i<instructions.size(); ++i) {
			if (instructions.get(i).getOpcode() == Opcodes.INVOKEVIRTUAL) {
				if (counter == 0) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(1, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/D", methods[0].getClassName());
				} else if (counter == 1) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(1, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/G", methods[0].getClassName());
				}
				counter++;
			}
		}
		Assert.assertTrue(counter == 2);
		
	}

	/**
	 * Inherited but not overridden methods
	 */
	@Test
	public void testBinidng3() {
		ClassInfo c = program.getClassInfo("soba/testdata/inheritance2/E");
		MethodInfo m = c.getMethodByName("testDynamicBinding3", "()V");
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		int counter = 0;
		for (int i=0; i<instructions.size(); ++i) {
			if (instructions.get(i).getOpcode() == Opcodes.INVOKEVIRTUAL) {
				if (counter == 0) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(1, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/C", methods[0].getClassName());
					Assert.assertEquals("y", methods[0].getMethodName());
				} else if (counter == 1) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(1, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/C", methods[0].getClassName());
					Assert.assertEquals("y", methods[0].getMethodName());
				}
				counter++;
			}
		}
	}
	
	
	private void checkLoopBinding(MethodInfo m) {
		InsnList instructions = m.getMethodBody().getMethodNode().instructions;
		for (int i=0; i<instructions.size(); ++i) {
			if (instructions.get(i).getOpcode() == Opcodes.INVOKEVIRTUAL) {
				MethodInsnNode call = (MethodInsnNode)instructions.get(i);
				if (call.name.equals("x")) {
					IMethodInfo[] methods = resolver.resolveMethodCall(m, i);
					Assert.assertEquals(2, methods.length);
					Assert.assertEquals("soba/testdata/inheritance1/D", methods[0].getClassName());
					Assert.assertEquals("soba/testdata/inheritance1/G", methods[1].getClassName());
				}
			}
		}
	}
	
	private void checkParamBinding(MethodInfo m) {
		TypeSet typeset = resolver.getMethodParamType(m, 1);
		Assert.assertEquals(2, typeset.getTypeCount());
	}
	
	@Test
	public void testLoopBinding() {
		ClassInfo c = program.getClassInfo("soba/testdata/inheritance2/E");
		checkLoopBinding(c.getMethodByName("testDynamicBinding4", "(Lsoba/testdata/inheritance1/C;)V"));
		checkLoopBinding(c.getMethodByName("testDynamicBinding5", "(Lsoba/testdata/inheritance1/C;)V"));
	}
	
	@Test
	public void testMethodParamType() {
		ClassInfo c = program.getClassInfo("soba/testdata/inheritance2/E");
		checkParamBinding(c.getMethodByName("testDynamicBinding4", "(Lsoba/testdata/inheritance1/C;)V"));
		checkParamBinding(c.getMethodByName("testDynamicBinding5", "(Lsoba/testdata/inheritance1/C;)V"));
		
	}
}
