package soba.bytecode;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import soba.bytecode.method.MethodBody;

public class MethodInfoTest implements ExampleProgram {

	private JavaProgram program; 
	
	@Before
	public void readExampleProgram() {
		program = JavaProgramTest.readExampleProgram();
	}
	
	@Test
	public void testMethodInfo() {
		ClassInfo d = program.getClassInfo(CLASS_D);
		MethodInfo notExist = d.getMethodByName("example", "(IJDLjava/lang/String;)V");
		Assert.assertTrue(notExist == null);
		MethodInfo example = d.getMethodByName("example", "(IJDLjava/lang/String;)I");
		Assert.assertFalse(example.isStatic());
		Assert.assertEquals(5, example.getParamCount());
		Assert.assertEquals("int", example.getReturnType());
		Assert.assertEquals("this", example.getParamName(0));
		Assert.assertEquals("i", example.getParamName(1));
		Assert.assertEquals("l", example.getParamName(2));
		Assert.assertEquals("d", example.getParamName(3));
		Assert.assertEquals("s", example.getParamName(4));
		Assert.assertNull(example.getParamName(5));
		Assert.assertEquals(CLASS_D, example.getParamType(0));
		Assert.assertEquals("int", example.getParamType(1));
		Assert.assertEquals("long", example.getParamType(2));
		Assert.assertEquals("double", example.getParamType(3));
		Assert.assertEquals("java/lang/String", example.getParamType(4));
		
		// getMethodBody() returns the same instance
		MethodBody body = example.getMethodBody();
		Assert.assertTrue(body == example.getMethodBody());
		
		Assert.assertEquals(CLASS_D + "#example#(IJDLjava/lang/String;)I", example.getMethodKey());
	}		
	
	@Test
	public void testLongString() {
		ClassInfo d = program.getClassInfo(CLASS_D);
		MethodInfo example = d.getMethodByName("example", "(IJDLjava/lang/String;)I");
		Assert.assertEquals(CLASS_D + ".example(" + CLASS_D + ":this, int:i, long:l, double:d, java/lang/String:s): int", example.toLongString());
	}
	
	@Test
	public void testNoVariables() {
		ClassInfo d = program.getClassInfo(CLASS_C);
		MethodInfo novars = d.getMethodByName("novariables", "()V");
		Assert.assertEquals(0, novars.getParamCount());
		Assert.assertEquals(null, novars.getParamName(0));
		Assert.assertEquals(null, novars.getParamType(0));
		Assert.assertTrue(novars.isStatic());
	}
}
