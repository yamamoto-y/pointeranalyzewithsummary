package soba.bytecode.method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import soba.bytecode.MethodInfo;

public class DataFlowInfoTest {

	@Test
	public void testDataFlow() throws IOException {
		FileInputStream input = new FileInputStream("bin/soba/testdata/DefUseTestData.class");
		try {
			ClassReader cr = new ClassReader(input);
			ClassNode node = new ClassNode(); 
			cr.accept(node, 0);

			List<MethodNode> methods = (List<MethodNode>)node.methods;
			for (MethodNode m: methods) {
				MethodInfo methodInfo = new MethodInfo(m);
				if (methodInfo.getMethodName().equals("localDataDependence")) {
					analyzeLocalDataDependence(methodInfo);
				} else if (methodInfo.getMethodName().equals("overwriteParam")) {
					analyzeParam(methodInfo);
				}
			}
			
		} finally {
			input.close();
		}
		
	}
	
	/**
	 * @param body
	 * @param edges
	 * @param fromLineRelative specifies a line by a relative position from the start line of the method.
	 * @param toLineRelative
	 * @return true if edges include at least one edge between the specified edges.
	 */
	private boolean contains(MethodBody body, List<DataFlowEdge> edges, int fromLineRelative, int toLineRelative) {
		for (DataFlowEdge e: edges) {
			boolean matched;
			if (e.getSourceInstruction() == -1) {
				matched = (fromLineRelative == -1);
			} else {
				matched = (body.getLine(e.getSourceInstruction())-body.getMinLine() == fromLineRelative);
			}
			 
			matched = matched && (body.getLine(e.getDestinationInstruction())-body.getMinLine() == toLineRelative);
			
			if (matched) return true;
		}
		return false;
	}
	
	private int countLocalEdges(List<DataFlowEdge> edges) {
		int count = 0;
		for (DataFlowEdge e: edges) {
			if (e.isLocal()) count++;
		}
		return count;
	}
	
	private void analyzeLocalDataDependence(MethodInfo m) {
		Assert.assertTrue(m.hasMethodBody());
		MethodBody body = m.getMethodBody();
		DataFlowInfo info = body.getDataFlow();
		Assert.assertNotNull(info);
		
		List<DataFlowEdge> edges = info.getEdges();
		Assert.assertEquals(6, countLocalEdges(edges));
		contains(body, edges, +0, +1);
		contains(body, edges, +2, +8);
		contains(body, edges, +2, +9);
		contains(body, edges, +4, +5);
		contains(body, edges, +6, +8);
		contains(body, edges, +6, +9);

		Assert.assertEquals(11, edges.size() - countLocalEdges(edges));
		contains(body, edges, +0, +0); // 0 -> b
		contains(body, edges, +5, +5); // err -> println, iload -> println
		contains(body, edges, +8, +8); // err -> println, iload -> println
		contains(body, edges, +9, +9); // err -> println, iload -> println
		contains(body, edges, +1, +1); // b -> if 
		contains(body, edges, +2, +2); // 1 -> x
		contains(body, edges, +4, +4); // 2 -> x
		contains(body, edges, +6, +6); // 3 -> x
	}

	private void analyzeParam(MethodInfo m) {
		Assert.assertTrue(m.hasMethodBody());
		MethodBody body = m.getMethodBody();
		DataFlowInfo info = body.getDataFlow();
		Assert.assertNotNull(info);
		
		List<DataFlowEdge> edges = info.getEdges();
		Assert.assertEquals(3, countLocalEdges(edges));
		contains(body, edges, -1, +0);
		contains(body, edges, +0, +1);
		contains(body, edges, -1, +1);

		Assert.assertEquals(4, edges.size() - countLocalEdges(edges));
		contains(body, edges, +1, +1); // load System.out -> invoke, iload 2 -> invoke println
		contains(body, edges, 0, 0);   // iconst -> istore (y=0), iconst -> compare
	}
}
