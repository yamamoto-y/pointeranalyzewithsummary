package soba.bytecode.method;

import org.junit.Assert;
import org.junit.Test;

import soba.util.IntPairList;
import soba.util.graph.DirectedGraph;


public class ControlDependenceTest {

	public static DirectedGraph buildControlFlowGraph() { 
		IntPairList edges = new IntPairList();
		edges.add(0, 1); // cycle 0->1->2
		edges.add(1, 2);
		edges.add(2, 0);
		edges.add(1, 3);
		
		edges.add(3, 4);
		edges.add(3, 5);
		
		edges.add(5, 6); // another cycle: 5->6->8->5
		edges.add(6, 8);
		edges.add(8, 5);

		edges.add(6, 7); 
		edges.add(7, 9);
		edges.add(7, 10);
		edges.add(9, 11);
		edges.add(10, 11);
		edges.add(11, 12); // 13 is not connected to any other vertices
		
		return new DirectedGraph(14, edges);
	}


	@Test
	public void testControlDependence() {
		DirectedGraph g = buildControlFlowGraph();
		DirectedGraph cd = ControlDependence.getDependence(14, g);

		// Not a branch vertex
		Assert.assertArrayEquals(new int[0], cd.getEdges(0));
		Assert.assertArrayEquals(new int[0], cd.getEdges(2));
		Assert.assertArrayEquals(new int[0], cd.getEdges(4));
		Assert.assertArrayEquals(new int[0], cd.getEdges(5));
		Assert.assertArrayEquals(new int[0], cd.getEdges(8));
		Assert.assertArrayEquals(new int[0], cd.getEdges(9));
		Assert.assertArrayEquals(new int[0], cd.getEdges(10));
		Assert.assertArrayEquals(new int[0], cd.getEdges(11));
		Assert.assertArrayEquals(new int[0], cd.getEdges(12));
		Assert.assertArrayEquals(new int[0], cd.getEdges(13));
		
		// Conditional branches
		Assert.assertArrayEquals(new int[]{0, 2}, cd.getEdges(1));
		Assert.assertArrayEquals(new int[]{4, 6, 7, 11, 12}, cd.getEdges(3));
		Assert.assertArrayEquals(new int[]{5, 8}, cd.getEdges(6));
		Assert.assertArrayEquals(new int[]{9, 10}, cd.getEdges(7));
	}
	
}
