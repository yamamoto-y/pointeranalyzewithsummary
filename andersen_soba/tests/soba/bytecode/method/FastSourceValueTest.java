package soba.bytecode.method;


import org.junit.Assert;
import org.junit.Test;

public class FastSourceValueTest {

	@Test
	public void testConstructorWithoutInstruction() {
		FastSourceValue value1 = new FastSourceValue(1);
		Assert.assertEquals(1, value1.getSize());
		Assert.assertEquals(0, value1.getInstructions().length);

		FastSourceValue value2 = new FastSourceValue(2);
		Assert.assertEquals(2, value2.getSize());
		Assert.assertEquals(0, value2.getInstructions().length);
	}
	
	@Test
	public void testConstructorWithSingleInstruction() {
		FastSourceValue value1 = new FastSourceValue(1, 2);
		Assert.assertEquals(1, value1.getSize());
		Assert.assertEquals(1, value1.getInstructions().length);
		Assert.assertEquals(2, value1.getInstructions()[0]);
	}
	
	@Test
	public void testConstructorWithArray() {
		FastSourceValue value1 = new FastSourceValue(2, new int[] { 1, 4, 9 });
		Assert.assertEquals(2, value1.getSize());
		Assert.assertEquals(3, value1.getInstructions().length);
		Assert.assertEquals(1, value1.getInstructions()[0]);
		Assert.assertEquals(4, value1.getInstructions()[1]);
		Assert.assertEquals(9, value1.getInstructions()[2]);
	}
	
	@Test
	public void testMerge() {
		FastSourceValue value123 = new FastSourceValue(1, new int[] {1, 2, 3});
		FastSourceValue value456 = new FastSourceValue(1, new int[] {4, 5, 6});
		FastSourceValue value135 = new FastSourceValue(1, new int[] {1, 3, 5});
		FastSourceValue value246 = new FastSourceValue(1, new int[] {2, 4, 6});
		FastSourceValue valueNULL = new FastSourceValue(1, new int[0]);
		FastSourceValue value123Size2 = new FastSourceValue(2, new int[] {1, 2, 3});
		
		FastSourceValue value123456concat = new FastSourceValue(value123, value456);
		Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, value123456concat.getInstructions());

		FastSourceValue value123456anotherConcat = new FastSourceValue(value456, value123);
		Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, value123456anotherConcat.getInstructions());

		FastSourceValue value123456mix = new FastSourceValue(value135, value246);
		Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, value123456mix.getInstructions());

		FastSourceValue value123456anotherMix = new FastSourceValue(value246, value135);
		Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, value123456anotherMix.getInstructions());

		FastSourceValue value123unchanged = new FastSourceValue(value123, valueNULL);
		Assert.assertArrayEquals(new int[]{1, 2, 3}, value123unchanged.getInstructions());

		FastSourceValue value246unchanged = new FastSourceValue(valueNULL, value246);
		Assert.assertArrayEquals(new int[]{2, 4, 6}, value246unchanged.getInstructions());
		
		FastSourceValue valueConcatNull = new FastSourceValue(valueNULL, valueNULL);
		Assert.assertArrayEquals(new int[0], valueConcatNull.getInstructions());
		
		FastSourceValue value1235 = new FastSourceValue(value123, value135);
		Assert.assertArrayEquals(new int[] {1, 2, 3, 5} , value1235.getInstructions());

		FastSourceValue value1235another = new FastSourceValue(value135, value123);
		Assert.assertArrayEquals(new int[] {1, 2, 3, 5} , value1235another.getInstructions());

		FastSourceValue value2456 = new FastSourceValue(value246, value456);
		Assert.assertArrayEquals(new int[] {2, 4, 5, 6} , value2456.getInstructions());

		FastSourceValue value2456another = new FastSourceValue(value456, value246);
		Assert.assertArrayEquals(new int[] {2, 4, 5, 6} , value2456another.getInstructions());
		
		FastSourceValue value123differentSize = new FastSourceValue(value123, value123Size2);
		Assert.assertArrayEquals(new int[] {1, 2, 3} , value123differentSize.getInstructions());
		Assert.assertEquals(1, value123differentSize.getSize());

		FastSourceValue value123differentSizeAnother = new FastSourceValue(value123Size2, value123);
		Assert.assertArrayEquals(new int[] {1, 2, 3} , value123differentSizeAnother.getInstructions());
		Assert.assertEquals(1, value123differentSizeAnother.getSize());
	}

	@Test
	public void testContainsAll() {
		FastSourceValue value123 = new FastSourceValue(1, new int[] {1, 2, 3});
		FastSourceValue value246 = new FastSourceValue(1, new int[] {2, 4, 6});
		FastSourceValue valueNULL = new FastSourceValue(1, new int[0]);
		FastSourceValue value0 = new FastSourceValue(1, 0);
		FastSourceValue value1 = new FastSourceValue(1, 1);
		FastSourceValue value2 = new FastSourceValue(1, 2);
		FastSourceValue value3 = new FastSourceValue(1, 3);
		FastSourceValue value4 = new FastSourceValue(1, 4);
		FastSourceValue value12 = new FastSourceValue(1, new int[] {1, 2});
		FastSourceValue value23 = new FastSourceValue(1, new int[] {2, 3});
		FastSourceValue value45 = new FastSourceValue(1, new int[] {4, 5});
		FastSourceValue value46 = new FastSourceValue(1, new int[] {4, 6});
		FastSourceValue value1234 = new FastSourceValue(1, new int[] {1, 2, 3, 4});
		FastSourceValue value24 = new FastSourceValue(1, new int[] {2, 4});
		
		Assert.assertTrue(value123.containsAll(valueNULL));
		Assert.assertTrue(value123.containsAll(value123));
		Assert.assertTrue(value123.containsAll(value1));
		Assert.assertTrue(value123.containsAll(value2));
		Assert.assertTrue(value123.containsAll(value3));
		Assert.assertTrue(value123.containsAll(value12));
		Assert.assertTrue(value123.containsAll(value23));
		Assert.assertFalse(value123.containsAll(value0));
		Assert.assertFalse(value123.containsAll(value4));
		Assert.assertFalse(value123.containsAll(value246));
		Assert.assertFalse(value123.containsAll(value45));
		Assert.assertFalse(value123.containsAll(value46));
		Assert.assertFalse(value123.containsAll(value1234));
		Assert.assertFalse(value123.containsAll(value24));

		Assert.assertTrue(valueNULL.containsAll(valueNULL));
		Assert.assertFalse(valueNULL.containsAll(value1));
		Assert.assertFalse(valueNULL.containsAll(value123));

		Assert.assertTrue(value246.containsAll(value46));
		Assert.assertTrue(value246.containsAll(value4));
		Assert.assertTrue(value246.containsAll(value24));
		Assert.assertFalse(value246.containsAll(value123));

	}

	@Test
	public void testEquals() {
		int[] v123 = new int[] {1, 2, 3};
		FastSourceValue value123 = new FastSourceValue(1, v123);
		FastSourceValue value123another = new FastSourceValue(1, v123);
		FastSourceValue value123differentSize = new FastSourceValue(2, v123);
		FastSourceValue value123differentArray = new FastSourceValue(1, new int[] {1, 2, 3});
		FastSourceValue value123differentSizeAndArray = new FastSourceValue(2, new int[] {1, 2, 3});
		
		Assert.assertTrue(value123.equals(value123another));
		Assert.assertFalse(value123.equals(value123differentSize));
		Assert.assertFalse(value123.equals(value123differentArray));
		Assert.assertFalse(value123.equals(value123differentSizeAndArray));
	}
}
