package soba.bytecode.method;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;


import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import soba.model.FieldAccess;

import soba.bytecode.ClassInfo;
import soba.bytecode.MethodInfo;
import soba.model.Invocation;

public class ClassInfoTest {

	private ClassInfo c;
	
	@Before
	public void setup() throws IOException {
		File f = new File("bin/soba/testdata/DefUseTestData.class");
		FileInputStream in = new FileInputStream(f);
		c = new ClassInfo("DefUseTestData.class", in);
		in.close();
	}
	
	@Test
	public void testGetMethod() {
		MethodInfo m = c.getMethodByName("overwriteParam", "(II)V");
		Assert.assertTrue(m != null);
		Assert.assertTrue(m.hasMethodBody());

		MethodBody body = m.getMethodBody();
		List<Invocation> invokes = body.listMethodCalls();
		Assert.assertEquals(1, invokes.size());
		Assert.assertEquals("java/io/PrintStream", invokes.get(0).getClassName());
		Assert.assertEquals("println", invokes.get(0).getMethodName());
		Assert.assertEquals("(I)V", invokes.get(0).getDescriptor());
		Assert.assertFalse("println is an instance method.", invokes.get(0).isStaticOrSpecial());

		List<FieldAccess> access = body.listFieldAccesses();
		Assert.assertEquals(1, access.size());
		Assert.assertEquals("java/lang/System", access.get(0).getClassName());
		Assert.assertEquals("out", access.get(0).getFieldName());
		Assert.assertEquals("Ljava/io/PrintStream;", access.get(0).getDescriptor());
		Assert.assertTrue(access.get(0).isStatic());
		
	}
}
