package soba.bytecode;

import org.junit.Assert;
import org.junit.Test;


public class NameCacheTest {

	@Test
	public void testCache() { 
		String s1 = "xyz";
		String s2 = "x".concat("yz");
		String s3 = "xy".concat("z");
		String shared = NameCache.getSharedInstance(s1);
		String another = NameCache.getSharedInstance("abc");
		String nullString = NameCache.getSharedInstance(null);
		String shared2 = NameCache.getSharedInstance(s2);
		String shared3 = NameCache.getSharedInstance(s3);
		Assert.assertTrue(s1 == shared);
		Assert.assertTrue(shared2 == shared);
		Assert.assertTrue(shared3 == shared);
		Assert.assertTrue(another != shared);
		Assert.assertNull(nullString);
		
		NameCache.clearCache();
		Assert.assertTrue(s2 == NameCache.getSharedInstance(s2));
		Assert.assertTrue(s2 == NameCache.getSharedInstance(s3));

		String[] strings = NameCache.replaceWithSharedString(new String[] {s1, s2, "abc"});
		Assert.assertTrue(s2 == strings[0]);
		Assert.assertTrue(s2 == strings[1]);
		Assert.assertTrue("abc" == strings[2]);

		NameCache.disableCache();
		Assert.assertTrue(s2 == NameCache.getSharedInstance(s2));
		Assert.assertTrue(s3 == NameCache.getSharedInstance(s3));
		
	}
}
