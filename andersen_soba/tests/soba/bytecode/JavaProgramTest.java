package soba.bytecode;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import soba.util.files.Directory;
import soba.util.files.IFileEnumerator;


public class JavaProgramTest implements ExampleProgram {

	public static JavaProgram readExampleProgram() {
		Directory dir = new Directory(new File("bin/soba/testdata/"));
		JavaProgram program = new JavaProgram(new IFileEnumerator[] {dir});
		return program;
	}

	@Test
	public void testReadProgram() {
		JavaProgram p = readExampleProgram();
		Assert.assertTrue(p.getErrors().size() == 0);
		Assert.assertTrue(p.getDuplicated().size() == 0);
		Assert.assertTrue(p.getFiltered().size() == 0);
		Assert.assertNotNull(p.getClassInfo(CLASS_C));
		Assert.assertNotNull(p.getClassInfo(CLASS_D));
		Assert.assertNotNull(p.getClassInfo(CLASS_E));
		Assert.assertNotNull(p.getClassInfo(CLASS_F));
		Assert.assertNotNull(p.getClassInfo(CLASS_G));
		Assert.assertNotNull(p.getClassInfo(CLASS_H));
		Assert.assertNotNull(p.getClassInfo(CLASS_I));
		Assert.assertNotNull(p.getClassInfo(CLASS_J));
		Assert.assertNotNull(p.getClassInfo(CLASS_K));
	}
}
